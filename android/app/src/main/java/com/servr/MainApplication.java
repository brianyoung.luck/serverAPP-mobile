package com.servr;

import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.reactnativenavigation.NavigationApplication;
import com.reactnativenavigation.react.NavigationReactNativeHost;
import com.reactnativenavigation.react.ReactGateway;
import com.zyu.ReactNativeWheelPickerPackage;
import com.reactnativecommunity.asyncstorage.AsyncStoragePackage;
import com.lugg.ReactNativeConfig.ReactNativeConfigPackage;
import ca.jaysoo.extradimensions.ExtraDimensionsPackage;
import io.invertase.firebase.RNFirebasePackage;
import io.invertase.firebase.auth.RNFirebaseAuthPackage;
import io.invertase.firebase.messaging.RNFirebaseMessagingPackage;
import io.invertase.firebase.notifications.RNFirebaseNotificationsPackage;
import com.oblador.vectoricons.VectorIconsPackage;
import com.cardio.RNCardIOPackage;
import com.imagepicker.ImagePickerPackage;
import com.reactnativecommunity.slider.ReactSliderPackage;
import com.BV.LinearGradient.LinearGradientPackage;
import com.horcrux.svg.SvgPackage;
import com.swmansion.gesturehandler.react.RNGestureHandlerPackage;
import com.rssignaturecapture.RSSignatureCapturePackage;
import fr.greweb.reactnativeviewshot.RNViewShotPackage;

import java.util.Arrays;
import java.util.List;

public class MainApplication extends NavigationApplication {
    @Override
    protected ReactGateway createReactGateway() {
        ReactNativeHost host = new NavigationReactNativeHost(this, isDebug(), createAdditionalReactPackages()) {
            @Override
            protected String getJSMainModuleName() {
                return "index";
            }
        };
        return new ReactGateway(this, isDebug(), host);
    }

    @Override
    public boolean isDebug() {
        return BuildConfig.DEBUG;
    }

    protected List<ReactPackage> getPackages() {
        // Add additional packages you require here
        // No need to add RnnPackage and MainReactPackage
        return Arrays.<ReactPackage>asList(
          new AsyncStoragePackage(),
          new ReactNativeConfigPackage(),
          new ExtraDimensionsPackage(),
          new RNFirebasePackage(),
          new RNFirebaseAuthPackage(),
          new RNFirebaseMessagingPackage(),
          new RNFirebaseNotificationsPackage(),
          new VectorIconsPackage(),
          new RNCardIOPackage(),
          new ImagePickerPackage(),
          new ReactNativeWheelPickerPackage(),
          new ReactSliderPackage(),
          new LinearGradientPackage(),
          new SvgPackage(),
          new RNGestureHandlerPackage(),
          new RSSignatureCapturePackage(),
          new RNViewShotPackage()
        );
    }
  
    @Override
    public List<ReactPackage> createAdditionalReactPackages() {
        return getPackages();
    }
}
