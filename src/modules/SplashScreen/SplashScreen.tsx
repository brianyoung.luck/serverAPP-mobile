import React, { PureComponent } from 'react';
import { View, StyleSheet, Image } from 'react-native';
import { ISplashScreenReduxProps } from './SplashScreen.Container';
import { View as ViewAnimatable } from 'react-native-animatable';
import colors from '../../constants/colors';
import { scale } from '../../utils/dimensions';
import { H1, H3 } from '../_global/Text';
import { Navigation } from 'react-native-navigation';
import { mainmenu, pickHotel } from '../../utils/navigationControl';
import { GET_RESTAURANT_CATEGORY_DISH } from '../../types/action.restaurant';

interface ISplashScreenProps extends ISplashScreenReduxProps {
    componentId: string;
    primaryColor: string;
}

interface ISplashScreenState {}

class SplashScreen extends PureComponent<ISplashScreenProps, ISplashScreenState> {
    constructor(props: ISplashScreenProps) {
        super(props);

        Navigation.mergeOptions(props.componentId, {
            statusBar: {
                backgroundColor: props.primaryColor,
                style: 'light',
            },
        });
    }

    componentDidMount() {
        console.log('splash screen props', this.props);
        if (this.props.isHotelPicked) {
            this.props.getHotelDetail(this.props.codeHotel);
        }

        if (this.props.isCheckedIn) {
            this.props.getProfile();
        }

        setTimeout(() => {
            if (this.props.isHotelPicked && this.props.isCheckedIn) {
                return Navigation.setRoot({ root: mainmenu });
            }

            Navigation.setRoot({ root: pickHotel });
        }, 3000);
    }

    render() {
        // console.log("Printing props of spalsh ", this.props)
        return (
            <View style={{ flex: 1, backgroundColor: this.props.primaryColor }}>
                {this.props.category != 'Normal' ? (
                    <View style={{ flex: 1 }}>
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <ViewAnimatable animation="fadeIn">
                                {/* <Image style={{ height: scale.w(120), width: scale.w(120) }} source={require('../../images/sample-hotle-logo.jpg')}></Image> */}
                                <Image
                                    style={{ height: scale.w(120), width: scale.w(120) }}
                                    source={{ uri: this.props.logo.hotel_logo_md }}
                                ></Image>
                                {/* <H1 textAlign="center" color={colors.WHITE} fontSize={scale.w(54)}>
                            {'myhotel'}
                        </H1> */}
                            </ViewAnimatable>
                        </View>
                        <View style={{ justifyContent: 'center', alignItems: 'center', margin: 20 }}>
                            <ViewAnimatable animation="fadeIn">
                                <H1 textAlign="center" color={colors.WHITE} fontSize={scale.w(12)}>
                                    {'Powered By Servr Hotels'}
                                </H1>
                            </ViewAnimatable>
                        </View>
                    </View>
                ) : (
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <ViewAnimatable
                            animation="fadeIn"
                            style={{ justifyContent: 'center', alignItems: 'center' }}
                        >
                            <H1 textAlign="center" color={colors.WHITE} fontSize={scale.w(54)}>
                                {'Servr'}
                            </H1>
                        </ViewAnimatable>
                    </View>
                )}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    // basic_container_center: {
    //     flex: 1,
    //     // justifyContent: 'center',
    //     // alignItems: 'center',
    //     backgroundColor: colors.BLUE,
    // },
});

export default SplashScreen;
