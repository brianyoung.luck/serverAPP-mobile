import { connect } from 'react-redux';
import SplashScreen from './SplashScreen';
import { bindActionCreators, Dispatch } from 'redux';
import { IState } from '../../types/state';
import { getHotelDetail } from '../../actions/action.hotel';
import { getProfile } from '../../actions/action.account';

const mapStateToProps = (state: IState) => ({
    isHotelPicked: state.hotel.isHotelPicked,
    codeHotel: state.hotel.code,
    primaryColor: state.hotel.theme.primary_color,
    isCheckedIn: state.account.isCheckedIn,
    hotelName: state.hotel.name,
    category: state.hotel.category,
    logo: state.hotel.logo,
    selectedLanguage: state.language,
});

const mapDispatchToProps = (dispatch: Dispatch) => {
    return bindActionCreators(
        {
            getHotelDetail,
            getProfile,
        },
        dispatch,
    );
};
export interface ISplashScreenReduxProps
    extends ReturnType<typeof mapStateToProps>,
        ReturnType<typeof mapDispatchToProps> {}

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(SplashScreen);
