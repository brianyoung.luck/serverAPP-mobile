import React from 'react';
import {
    View,
    StyleSheet,
    TouchableOpacity,
    Keyboard,
    Alert,
    ScrollView,
    Platform,
    Image,
} from 'react-native';
import base from '../../utils/baseStyles';
import { scale } from '../../utils/dimensions';
import Navbar from '../_global/Navbar';
import { Navigation } from 'react-native-navigation';
import { IRestaurant } from '../../types/restaurant';
import FieldForm from './Components/FieldForm';
import { debounce } from 'lodash';
import * as Animatable from 'react-native-animatable';
import { ButtonPrimary } from '../_global/Button';
import colors from '../../constants/colors';
import { Calendar as CalendarWidget, DateObject } from 'react-native-calendars';
import { format } from 'date-fns';
import CustomModal from '../_global/CustomModal';
import ModalTimePicker from './Components/ModalTimePicker';
import { IBookATableReduxProps } from './BookATable.Container';
import { Text, View as ViewAnimatable } from 'react-native-animatable';
import ModalDatePicker from './Components/ModalDatePicker';
import DropDownPicker from 'react-native-dropdown-picker';
import { H4 } from '../_global/Text';

export interface IBookATableProps extends IBookATableReduxProps {
    componentId: string;
    restaurant: IRestaurant;
}

interface IBookATableState {
    time: string;
    date: string;
    numberPeople: string;
    loading: boolean;
    table_array: string[];
    table_no: string;
}

class BookATable extends React.Component<IBookATableProps, IBookATableState> {
    private _modalTimePicker = React.createRef<CustomModal>();
    private _modalDatePicker = React.createRef<CustomModal>();

    constructor(props: IBookATableProps) {
        super(props);

        this.state = {
            time: '',
            date: '',
            numberPeople: props.numberPeople.toString(),
            loading: false,
            table_no: '',
        };

        Navigation.mergeOptions(props.componentId, {
            statusBar: {
                backgroundColor: props.color,
                style: 'light',
            },
        });
        this._handleBack = this._handleBack.bind(this);
        this._handleBookATable = this._handleBookATable.bind(this);
        this._handleModalDatePicker = this._handleModalDatePicker.bind(this);
        this._onDayPress = this._onDayPress.bind(this);
        this._getMarkedDates = this._getMarkedDates.bind(this);
        this._handleModalDatePicker2 = this._handleModalDatePicker2.bind(this);
        this._onChangeDate = this._onChangeDate.bind(this);
    }

    _handleBack() {
        Navigation.pop(this.props.componentId);
    }

    _handleBookATable() {
        this.setState({ loading: true });
        const {
            restaurant: { id },
        } = this.props;
        const { numberPeople, date, time, table_no } = this.state;

        this.props.bookATable(
            id,
            Number(numberPeople) !== NaN ? Number(numberPeople) : 0,
            time,
            date,
            table_no,
            async () => {
                this.setState({ loading: false });
                await Navigation.popToRoot(this.props.componentId);
                Alert.alert('Success', 'Success, your reservation request has been sent.');
            },
            () => {
                this.setState({ loading: false });
            },
        );
    }

    _handleModalDatePicker(closeModal?: boolean) {
        return () => {
            Keyboard.dismiss();
            if (this._modalTimePicker.current) {
                if (closeModal) {
                    this._modalTimePicker.current.hide();
                } else {
                    this._modalTimePicker.current.show();
                    this.setState({ time: new Date().toString() });
                }
            }
        };
    }

    _onDayPress(date: DateObject) {
        this.setState({ date: date.dateString });
    }

    _getMarkedDates() {
        if (this.state.date !== '') {
            return {
                [this.state.date]: {
                    selected: true,
                },
            };
        }

        return {};
    }

    _handleModalDatePicker2(closeModal?: boolean) {
        return () => {
            Keyboard.dismiss();
            if (this._modalDatePicker.current) {
                if (closeModal) {
                    this._modalDatePicker.current.hide();
                } else {
                    this._modalDatePicker.current.show();
                    this.setState({ date: new Date().toString() });
                }
            }
        };
    }

    _onChangeDate(date: Date) {
        this.setState((prevState) => {
            if (prevState.date) {
                return {
                    ...prevState,
                    date: date.toString(),
                };
            }

            return {
                ...prevState,
                date: date.toString(),
            };
        });
    }

    render() {
        const { restaurant, color } = this.props;
        const { number_of_people, time_of_booking, confirm_booking } = this.props.selectedLanguage;
        var table_array = [];
        Array(Number(restaurant.res_table_numbers))
            .fill('')
            .map((item, index) => {
                table_array.push({ label: (index + 1).toString(), value: (index + 1).toString() });
            });

        return (
            <View style={base.container}>
                <Navbar
                    isBrown={color === ''}
                    color={color}
                    onClick={this._handleBack}
                    title={restaurant.name}
                />

                <ScrollView>
                    <View style={{ height: scale.h(100) }} />

                    <FieldForm
                        label={number_of_people}
                        placeholder="No."
                        value={this.state.numberPeople}
                        onChangeText={(numberPeople) => this.setState({ numberPeople: numberPeople })}
                        autoCapitalize="none"
                        keyboardType="number-pad"
                    />
                    <View style={{ height: scale.w(20) }} />
                    <TouchableOpacity
                        onPress={debounce(this._handleModalDatePicker(false), 1000, {
                            leading: true,
                            trailing: false,
                        })}
                        activeOpacity={0.8}
                    >
                        <FieldForm
                            label={time_of_booking}
                            value={
                                this.state.time !== ''
                                    ? format(this.state.time, 'hh:mma')
                                    : this.props.selectedLanguage.time
                            }
                            isEmpty={this.state.time === ''}
                            textOnly
                        />
                    </TouchableOpacity>
                    <View style={{ height: scale.w(20) }} />
                    <TouchableOpacity
                        onPress={debounce(this._handleModalDatePicker2(false), 1000, {
                            leading: true,
                            trailing: false,
                        })}
                        // disabled={this.props.isCheckedIn}
                        activeOpacity={0.8}
                    >
                        <FieldForm
                            label={'Date'}
                            value={
                                this.state.date === '' ? 'DD/MM/YY' : format(this.state.date, 'DD/MM/YYYY')
                            }
                            isEmpty={this.state.date === ''}
                            textOnly
                        />
                    </TouchableOpacity>
                    <View style={{ height: scale.w(20) }} />
                    <View
                        style={{
                            flex: 1,
                            paddingRight: scale.w(8),
                            paddingLeft: scale.w(20),
                            marginBottom: scale.h(5),
                        }}
                    >
                        <H4 fontSize={scale.w(18)}>Select table no</H4>
                    </View>

                    <DropDownPicker
                        zIndex={500}
                        items={table_array}
                        placeholder={'Select table'}
                        containerStyle={{
                            height: scale.h(50),
                            marginLeft: scale.w(20),
                        }}
                        style={{
                            backgroundColor: 'white',
                            borderWidth: 0,
                            width: scale.w(321),
                            borderTopRightRadius: scale.w(25),
                            borderTopLeftRadius: scale.w(25),
                            borderBottomLeftRadius: scale.w(25),
                            borderBottomRightRadius: scale.w(25),
                            ...Platform.select({
                                ios: {
                                    shadowColor: '#000',
                                    shadowOffset: { width: 0, height: 4 },
                                    shadowOpacity: 0.2,
                                    shadowRadius: 4,
                                },
                                android: {
                                    elevation: 4,
                                },
                            }),
                            paddingLeft: scale.w(26),
                        }}
                        labelStyle={{
                            fontFamily: 'Roboto-Light',
                            color: colors.BLACK,
                        }}
                        activeLabelStyle={{ fontFamily: 'Roboto-Bold', color: colors.BLACK }}
                        itemStyle={{
                            justifyContent: 'flex-start',
                        }}
                        placeholderStyle={{
                            color: '#888',
                            fontFamily: 'Roboto-Light',
                            fontSize: scale.w(18),
                        }}
                        arrowStyle={{
                            marginRight: scale.w(10),
                        }}
                        arrowColor={'black'}
                        dropDownStyle={{
                            backgroundColor: colors.WHITE,
                            width: scale.w(321),
                            borderTopLeftRadius: scale.w(25),
                            borderTopRightRadius: scale.w(25),
                            borderBottomRightRadius: scale.w(25),
                            borderBottomLeftRadius: scale.w(25),
                            marginTop: scale.h(5),
                        }}
                        onChangeItem={(item) => {
                            this.setState({
                                table_no: item.value,
                            });
                        }}
                    />
                    <View style={{ height: scale.w(20) }} />
                    <View
                        style={{
                            paddingHorizontal: scale.w(20),
                        }}
                    >
                        {restaurant.res_table_layout !== null ? (
                            <View
                                style={{
                                    width: scale.w(320),
                                    height: scale.h(200),
                                }}
                            >
                                <Image
                                    source={{ uri: restaurant.res_table_layout }}
                                    style={{
                                        width: '100%',
                                        height: '100%',
                                        marginBottom: scale.w(8),
                                    }}
                                    resizeMode="cover"
                                />
                            </View>
                        ) : (
                            <View />
                        )}
                    </View>
                    <Animatable.View
                        useNativeDriver
                        animation="fadeIn"
                        duration={300}
                        style={styles.submit_btn_container}
                    >
                        <ButtonPrimary
                            onPress={this._handleBookATable}
                            text={confirm_booking}
                            backgroundColor={color || colors.BROWN}
                            loading={this.state.loading}
                            disabled={this.state.loading}
                        />
                    </Animatable.View>
                </ScrollView>

                <CustomModal ref={this._modalTimePicker} animationIn="fadeInUp" animationOut="fadeOutDown">
                    <ModalTimePicker
                        time={this.state.time !== '' ? new Date(this.state.time) : new Date()}
                        onTimeChange={(time) => this.setState({ time: time.toString() })}
                        minimumDate={new Date()}
                        showModal={this._handleModalDatePicker(true)}
                        title={this.props.selectedLanguage.pick_your_time_booking}
                        color={color}
                        selectedLanguage={this.props.selectedLanguage}
                    />
                </CustomModal>

                <CustomModal ref={this._modalDatePicker} animationIn="fadeInUp" animationOut="fadeOutDown">
                    <ModalDatePicker
                        date={new Date(this.state.date)}
                        minimumDate={new Date()}
                        color={color}
                        onDateChange={this._onChangeDate}
                        showModal={this._handleModalDatePicker2(true)}
                        title={`Pick your booking date `}
                    />
                </CustomModal>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    calendar_container: {
        paddingHorizontal: scale.w(13),
        marginTop: scale.w(20),
    },
    submit_btn_container: {
        paddingHorizontal: scale.w(57),
        marginTop: scale.w(18),
        flex: 1,
        marginBottom: scale.h(150),
        zIndex: 100,
    },
});

export default BookATable;
