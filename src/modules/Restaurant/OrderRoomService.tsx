import React from 'react';
import {
    View,
    StyleSheet,
    TouchableOpacity,
    Keyboard,
    RefreshControl,
    Alert,
    SafeAreaView,
    SectionList,
    FlatList,
    Image,
    TextInput,
    ViewPagerAndroidComponent,
    Text,
    Platform,
    ActivityIndicator,
} from 'react-native';
import base from '../../utils/baseStyles';
import { scale, widthPercentageToDP as wp, heightPercentageToDP as hp } from '../../utils/dimensions';
import Navbar from '../_global/Navbar';
import { Navigation } from 'react-native-navigation';
import { IRestaurant, IDish, ICategoryDish } from '../../types/restaurant';
import { debounce } from 'lodash';
import * as Animatable from 'react-native-animatable';
import { ButtonPrimary } from '../_global/Button';
import colors from '../../constants/colors';
import CustomModal from '../_global/CustomModal';
import { H4, H2, H3 } from '../_global/Text';
import Ionicons from 'react-native-vector-icons/Ionicons';
import NoteOrderItem from './Components/NoteOrderItem';
import { IOrderRoomServiceReduxProps } from './OrderRoomService.Container';
import numeral from 'numeral';

import { find, findIndex } from 'lodash';
import { IOrderItem } from '../../types/action.restaurant';
import { trackingProgress } from '../../utils/navigationControl';
import Lightbox from 'react-native-lightbox';
import ImageZoom from 'react-native-image-pan-zoom';
import Modal from 'react-native-modal';

export interface IOrderRoomServiceProps extends IOrderRoomServiceReduxProps {
    componentId: string;
    restaurant: IRestaurant;
}

interface ISelectedItems extends IOrderItem {
    name: string;
}

interface IOrderRoomServiceState {
    items: ISelectedItems[];
    selectedItem: ISelectedItems;
    loadingGet: boolean;
    loading: boolean;
    selectedVal: any;
    dataToShow: any;
    search: boolean;
    newArray: any;
    check: boolean;
    modalVisible: boolean;
    modalVisible1: boolean;
    imageUrl: String;
    description: any;
    dishName: String;
    item: any;
}

class OrderRoomService extends React.Component<IOrderRoomServiceProps, IOrderRoomServiceState> {
    private _modalNoteOrderItem = React.createRef<CustomModal>();

    constructor(props: IOrderRoomServiceProps) {
        super(props);

        this.state = {
            items: [],
            selectedItem: {
                dish_id: 0,
                qty: 0,
                note: '',
                name: '',
            },
            loadingGet: false,
            loading: false,
            selectedVal: '',
            dataToShow: this.props.dishesCategories,
            search: false,
            newArray: [],
            check: true,
            modalVisible: false,
            modalVisible1: false,
            imageUrl: '',
            description: '',
            dishName: '',
            item: [],
        };

        Navigation.mergeOptions(props.componentId, {
            statusBar: {
                backgroundColor: props.color,
                style: 'light',
            },
        });
        this._fetch = this._fetch.bind(this);
        this._handleBack = this._handleBack.bind(this);
        this._handleOrderRoomService = this._handleOrderRoomService.bind(this);
        this._handleModalNoteOrderItem = this._handleModalNoteOrderItem.bind(this);
        this._onChangeText = this._onChangeText.bind(this);
        this._addTotalDish = this._addTotalDish.bind(this);
        this._substractTotalDish = this._substractTotalDish.bind(this);
        this._keyExtractor = this._keyExtractor.bind(this);
        this._renderListHeaderComponent = this._renderListHeaderComponent.bind(this);
        this._renderItemSeparatorComponent = this._renderItemSeparatorComponent.bind(this);
        this._renderItem = this._renderItem.bind(this);
        this._renderSectionHeader = this._renderSectionHeader.bind(this);
        this._renderSectionFooter = this._renderSectionFooter.bind(this);
    }
    setModalVisible(visible) {
        this.setState({ modalVisible: visible });
    }
    setModalVisible1(visible) {
        this.setState({ modalVisible1: visible });
    }
    componentDidMount() {
        this._fetch();
        // this.props.getRestaurantCategoryDish(this.props.restaurant.id);
        this.setState({ dataToShow: this.props.dishesCategories });
    }
    componentDidUpdate() {
        if (this.state.check) {
            this.props.dishesCategories.map((data: any) => {
                if (data.data) {
                    data.data.map((data1: any) => {
                        this.state.newArray.push(data1);
                    });
                }
            });
            if (this.props.dishesCategories.length != 0) {
                this.setState({
                    check: false,
                });
            }

            // console.log("newArr--> ", this.state.newArray)
        }
    }

    _fetch() {
        this.setState({ loadingGet: true });
        this.props.getRestaurantCategoryDish(
            this.props.restaurant.id,
            () => {
                this.setState({ loadingGet: false });
            },
            () => {
                this.setState({ loadingGet: false });
            },
        );
    }

    _handleBack() {
        Navigation.pop(this.props.componentId);
    }

    _handleOrderRoomService() {
        this.setState({ loading: true });
        const { id } = this.props.restaurant;
        const { items } = this.state;

        this.props.orderRoomService(
            id,
            items,
            async () => {
                this.setState({ loading: false });
                Navigation.popTo('restaurantList');
                await Navigation.push('restaurantList', trackingProgress);
                Alert.alert('Success', 'Success order room service');
            },
            () => {
                this.setState({ loading: false });
            },
        );
    }

    _handleModalNoteOrderItem(item: IDish | null, closeModal?: boolean) {
        return () => {
            Keyboard.dismiss();
            if (this._modalNoteOrderItem.current) {
                if (closeModal) {
                    this.setState(
                        (prevState) => ({
                            items: prevState.items.map((dish) => {
                                if (dish.dish_id === prevState.selectedItem.dish_id) {
                                    return {
                                        ...dish,
                                        note: prevState.selectedItem.note,
                                    };
                                }

                                return dish;
                            }),
                        }),
                        this._modalNoteOrderItem.current.hide,
                    );
                } else {
                    const selected = find(this.state.items, { dish_id: item ? item.id : 0 });
                    this.setState(
                        { selectedItem: selected ? selected : this.state.selectedItem },
                        this._modalNoteOrderItem.current.show,
                    );
                }
            }
        };
    }

    _onChangeText(text: string) {
        this.setState((prevState) => ({
            selectedItem: {
                ...prevState.selectedItem,
                note: text,
            },
        }));
    }

    _addTotalDish(item: IDish) {
        const index = findIndex(this.state.items, { dish_id: item.id });

        if (index < 0) {
            this.setState((prevState) => ({
                items: [
                    ...prevState.items,
                    {
                        dish_id: item.id,
                        qty: 1,
                        note: '',
                        name: item.name,
                    },
                ],
            }));
        } else {
            this.setState((prevState) => ({
                items: prevState.items.map((dish) => {
                    if (dish.dish_id === item.id) {
                        return {
                            ...dish,
                            qty: dish.qty + 1,
                        };
                    }

                    return dish;
                }),
            }));
        }
    }

    _substractTotalDish(item: IDish) {
        const selected = find(this.state.items, { dish_id: item.id });

        if (selected && selected.qty <= 1) {
            this.setState((prevState) => ({
                items: prevState.items.filter(({ dish_id }) => dish_id !== item.id),
            }));
        } else {
            this.setState((prevState) => ({
                items: prevState.items.map((dish) => {
                    if (dish.dish_id === item.id) {
                        return {
                            ...dish,
                            qty: dish.qty - 1,
                        };
                    }

                    return dish;
                }),
            }));
        }
    }

    _keyExtractor(item: ICategoryDish, index: number) {
        return `${item.name}_${index}`;
    }

    _renderListHeaderComponent() {
        return <View style={{ height: scale.w(30) }} />;
    }

    _renderItemSeparatorComponent() {
        return <View style={{ height: scale.w(0) }} />;
    }

    _renderItem({ item }: { item: IDish }) {
        const selected = find(this.state.items, { dish_id: item.id });
        const { color, currency } = this.props;

        return (
            <View
                style={{
                    width: scale.w(336),
                    height: scale.w(126),
                    marginHorizontal: scale.w(25),
                    borderRadius: scale.w(20),
                    backgroundColor: '#fff',
                    alignSelf: 'center',
                    paddingLeft: scale.w(10),
                    paddingRight: scale.w(10),
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginBottom: scale.h(20),
                    ...Platform.select({
                        ios: {
                            shadowColor: '#000',
                            shadowOffset: { width: 0, height: 4 },
                            shadowOpacity: 0.2,
                            shadowRadius: 3,
                        },
                        android: {
                            elevation: 8,
                        },
                    }),
                }}
            >
                <TouchableOpacity
                    onPress={() => {
                        this.setState({
                            imageUrl: item.image,
                            description: item.description,
                            dishName: item.name,
                            item: item,
                        });
                        this.setModalVisible(true);
                    }}
                    style={{
                        flexDirection: 'row',
                    }}
                >
                    <View
                        style={{
                            width: scale.w(76),
                            height: scale.w(70),
                            borderRadius: scale.w(25),
                        }}
                    >
                        <Image
                            resizeMode={'cover'}
                            source={{ uri: `${item.image}` }}
                            style={{
                                width: '100%',
                                height: '100%',
                                borderRadius: scale.w(20),
                            }}
                        />
                    </View>
                    <View
                        style={{
                            paddingLeft: scale.w(8),
                            width: scale.w(140),
                        }}
                    >
                        <H3 fontSize={scale.w(20)}>{`${item.name}`}</H3>
                        {`${item.description}` != 'null' &&
                            `${item.description}` != undefined &&
                            `${item.description}` != null && (
                                <View>
                                    <H4 fontSize={scale.w(12)}>
                                        {item.description
                                            ? item.description.length > 40
                                                ? item.description.substring(0, 40) + '...'
                                                : item.description
                                            : null}
                                    </H4>
                                </View>
                            )}
                        <H4 fontSize={scale.w(20)}>{`${currency}${numeral(item.price).format('0,0')}`}</H4>
                    </View>
                    <View></View>
                </TouchableOpacity>

                <View
                    style={{
                        width: scale.w(100),
                        flexDirection: 'row',
                    }}
                >
                    {selected && (
                        <TouchableOpacity
                            onPress={debounce(this._handleModalNoteOrderItem(item, false), 1000, {
                                leading: true,
                                trailing: false,
                            })}
                            activeOpacity={0.7}
                            style={{ flex: 0.4 }}
                        >
                            <View
                                style={{
                                    borderWidth: 1,
                                    borderRadius: scale.w(8),
                                    borderColor: color || colors.BROWN,
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    flex: 1,
                                }}
                            >
                                <Ionicons
                                    name={
                                        selected && selected.note !== ''
                                            ? 'md-checkmark-circle-outline'
                                            : 'md-create'
                                    }
                                    color={color || colors.BROWN}
                                    size={scale.w(18)}
                                />
                            </View>
                        </TouchableOpacity>
                    )}
                    <View style={{ width: 5 }} />

                    {selected ? (
                        <View
                            style={{
                                flexDirection: 'row',
                                borderWidth: 1,
                                borderRadius: scale.w(8),
                                borderColor: color || colors.BROWN,
                                paddingHorizontal: scale.w(10),
                                alignItems: 'center',
                                justifyContent: 'center',
                                flex: 1,
                                width: '80%',
                                paddingVertical: hp(0.2),
                            }}
                        >
                            <TouchableOpacity
                                onPress={() => this._substractTotalDish(item)}
                                activeOpacity={0.7}
                                style={{ flex: 0.3 }}
                            >
                                <Ionicons name="md-remove" color={color || colors.BROWN} size={scale.w(22)} />
                            </TouchableOpacity>
                            <View
                                style={{
                                    flex: 0.4,
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                }}
                            >
                                <H4>{selected ? selected.qty : 0}</H4>
                            </View>
                            <TouchableOpacity
                                onPress={() => this._addTotalDish(item)}
                                activeOpacity={0.7}
                                style={{ flex: 0.3 }}
                            >
                                <Ionicons name="md-add" color={color || colors.BROWN} size={scale.w(22)} />
                            </TouchableOpacity>
                        </View>
                    ) : (
                        <TouchableOpacity
                            onPress={() => this._addTotalDish(item)}
                            activeOpacity={0.7}
                            style={{
                                flex: 1,
                                width: '80%',
                                flexDirection: 'row',
                                paddingVertical: hp(0.2),
                            }}
                        >
                            <View style={{ flex: 0.3 }}></View>
                            <View
                                style={{
                                    flexDirection: 'row',
                                    borderWidth: 1,
                                    borderRadius: scale.w(8),
                                    borderColor: color || colors.BROWN,
                                    flex: 0.7,
                                    paddingHorizontal: scale.w(10),
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                }}
                            >
                                <View
                                    style={{
                                        flex: 1,
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                    }}
                                >
                                    <H4>{this.props.selectedLanguage.add}</H4>
                                </View>
                                <Ionicons name="md-add" color={color || colors.BROWN} size={scale.w(22)} />
                            </View>
                        </TouchableOpacity>
                    )}
                </View>
            </View>
        );
    }

    _renderSectionHeader({ section: { title } }: { section: any }) {
        return (
            <View
                style={{
                    paddingLeft: scale.w(20),
                    paddingBottom: scale.h(15),
                    backgroundColor: '#fff',
                }}
            >
                <H2 fontSize={scale.w(20)}>{title}</H2>
            </View>
        );
    }

    _renderSectionFooter() {
        return <View style={{ height: 25 }} />;
    }
    Search(val: any) {
        if (val === '') {
            this.setState({ search: false, dataToShow: this.props.dishesCategories });
        } else {
            let arr = this.state.newArray.filter((x: any) => {
                return x.name.toLowerCase().includes(val.toLowerCase());
            });
            // console.log(arr)
            this.setState({ dataToShow: arr, search: true });
        }
    }

    render() {
        const { color } = this.props;

        const { confirm_order } = this.props.selectedLanguage;

        if (!this.props.dishesCategories) {
            return null;
        } else {
            return (
                <View style={base.container}>
                    <Modal
                        onBackdropPress={() => {
                            this.setModalVisible(false);
                        }}
                        animationType="slide"
                        animationInTiming={500}
                        backdropOpacity={0.7}
                        onSwipeComplete={() => this.setState({ modalVisible: false })}
                        isVisible={this.state.modalVisible}
                        onBackButtonPress={() => {
                            this.setModalVisible(false);
                        }}
                    >
                        <View
                            style={{
                                flex: 1,
                                justifyContent: 'center',
                                paddingHorizontal: wp(2),
                                alignItems: 'center',
                            }}
                        >
                            <View
                                style={{
                                    width: '90%',
                                    backgroundColor: 'white',
                                    borderRadius: 50,
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    paddingHorizontal: wp(3),
                                }}
                            >
                                <View style={{ height: hp(4) }} />

                                <H3 fontSize={scale.w(20)}>{this.state.dishName}</H3>

                                {this.state.imageUrl && <View style={{ height: hp(1) }} />}
                                {this.state.imageUrl &&
                                    this.state.imageUrl != 'https://cms.servrhotels.com/images/default.jpg' &&
                                    this.state.imageUrl !=
                                        'http://cms.servrhotels.com/images/default.jpg' && (
                                        <TouchableOpacity
                                            onPress={() => {
                                                setTimeout(() => {
                                                    this.setModalVisible1(true);
                                                }, 400);
                                                this.setModalVisible(false);
                                            }}
                                            style={{
                                                height: hp(20),
                                                width: '100%',
                                                justifyContent: 'center',
                                                alignItems: 'center',
                                            }}
                                        >
                                            <Image
                                                resizeMode="contain"
                                                source={{ uri: this.state.imageUrl }}
                                                style={{ height: '80%', width: '80%' }}
                                            />
                                        </TouchableOpacity>
                                    )}
                                {this.state.imageUrl && <View style={{ height: hp(1) }} />}
                                {this.state.description && (
                                    <H4 fontSize={scale.w(12)}>{this.state.description}</H4>
                                )}

                                {this.state.description && <View style={{ height: hp(6) }} />}
                                <TouchableOpacity
                                    onPress={() => {
                                        this._addTotalDish(this.state.item);
                                        this.setModalVisible(false);
                                    }}
                                    style={{
                                        borderRadius: 100,
                                        height: hp(6),
                                        width: wp(40),
                                        backgroundColor: color,
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                    }}
                                >
                                    <Text
                                        style={{
                                            color: colors.WHITE,
                                            fontFamily: 'Roboto-Bold',
                                            fontSize: scale.w(14),
                                        }}
                                    >
                                        {this.props.selectedLanguage.add_to_order}
                                    </Text>
                                </TouchableOpacity>
                                <View style={{ height: hp(4) }} />
                            </View>
                        </View>
                        <View
                            style={{
                                position: 'absolute',
                                height: '100%',
                                alignSelf: 'flex-start',
                                paddingHorizontal: wp(1.2),
                            }}
                        >
                            <TouchableOpacity
                                onPress={() => {
                                    this.setModalVisible(false);
                                }}
                            >
                                <Image
                                    source={require('../../images/icon_back.png')}
                                    style={{ width: scale.w(30), height: scale.w(30) }}
                                    resizeMode={'contain'}
                                ></Image>
                            </TouchableOpacity>
                        </View>
                    </Modal>
                    {/* //////////////////////////// */}
                    <Modal
                        onBackdropPress={() => {
                            setTimeout(() => {
                                this.setModalVisible(true);
                            }, 400);
                            this.setModalVisible1(false);
                        }}
                        onBackButtonPress={() => {
                            setTimeout(() => {
                                this.setModalVisible(true);
                            }, 400);
                            this.setModalVisible1(false);
                        }}
                        isVisible={this.state.modalVisible1}
                        animationType="slide"
                        animationInTiming={500}
                        backdropOpacity={0.9}
                        style={styles.modal}
                    >
                        <View style={{ flex: 1 }}>
                            <View style={styles.modalContainer}>
                                <View
                                    style={{
                                        flexDirection: 'row',
                                        justifyContent: 'center',
                                        height: '100%',
                                    }}
                                >
                                    <View
                                        style={{
                                            justifyContent: 'center',
                                            alignSelf: 'center',
                                        }}
                                    >
                                        <ImageZoom
                                            cropWidth={wp(100)}
                                            cropHeight={hp(100)}
                                            imageWidth={wp(100)}
                                            imageHeight={hp(100)}
                                        >
                                            <Image
                                                resizeMode="contain"
                                                source={{ uri: this.state.imageUrl }}
                                                style={styles.image}
                                            ></Image>
                                        </ImageZoom>
                                    </View>
                                </View>
                            </View>
                            <View
                                style={{
                                    position: 'absolute',
                                    height: '100%',
                                    alignSelf: 'flex-start',
                                    paddingHorizontal: wp(1.2),
                                }}
                            >
                                <TouchableOpacity
                                    onPress={() => {
                                        setTimeout(() => {
                                            this.setModalVisible(true);
                                        }, 400);
                                        this.setModalVisible1(false);
                                    }}
                                >
                                    <Image
                                        source={require('../../images/icon_back.png')}
                                        style={{ width: scale.w(30), height: scale.w(30) }}
                                        resizeMode={'contain'}
                                    ></Image>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </Modal>
                    <Navbar
                        isBrown={color === ''}
                        color={color}
                        onClick={this._handleBack}
                        title={this.props.restaurant.name}
                    />

                    <View style={{ flex: 1 }}>
                        <View style={styles.searchview}>
                            <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: 13 }}>
                                <Image
                                    source={require('../../images/icon-search.png')}
                                    style={{ height: 15, width: 15, tintColor: color }}
                                />

                                <TextInput
                                    value={this.state.selectedVal}
                                    onChangeText={(val) => {
                                        this.setState({ selectedVal: val, search: true });
                                        this.Search(val);
                                    }}
                                    placeholder={this.props.selectedLanguage.search}
                                    style={{
                                        fontSize: 20,
                                        marginLeft: 10,
                                        width: wp(75),
                                        fontFamily: 'Roboto-Regular',
                                    }}
                                ></TextInput>
                            </View>
                        </View>
                        {this.state.search == false ? (
                            <SectionList
                                refreshControl={
                                    <RefreshControl
                                        onRefresh={this._fetch}
                                        refreshing={this.state.loadingGet}
                                    />
                                }
                                ListEmptyComponent={() => {
                                    return (
                                        <View>
                                            {this.state.loadingGet ? (
                                                <ActivityIndicator size="large" color={'#fff'} />
                                            ) : (
                                                <View style={{ marginTop: scale.h(250) }}>
                                                    <Text style={{ alignSelf: 'center' }}>
                                                        No items found
                                                    </Text>
                                                </View>
                                            )}
                                        </View>
                                    );
                                }}
                                sections={this.props.dishesCategories}
                                extraData={this.state}
                                keyExtractor={this._keyExtractor}
                                ListHeaderComponent={this._renderListHeaderComponent}
                                ItemSeparatorComponent={this._renderItemSeparatorComponent}
                                renderItem={this._renderItem}
                                renderSectionHeader={this._renderSectionHeader}
                                renderSectionFooter={this._renderSectionFooter}
                                initialNumToRender={10}
                            />
                        ) : (
                            <FlatList
                                refreshControl={
                                    <RefreshControl
                                        onRefresh={this._fetch}
                                        refreshing={this.state.loadingGet}
                                    />
                                }
                                ListEmptyComponent={() => {
                                    return (
                                        <View>
                                            {this.state.loadingGet ? (
                                                <ActivityIndicator size="large" color={'#fff'} />
                                            ) : (
                                                <View style={{ marginTop: scale.h(250) }}>
                                                    <Text style={{ alignSelf: 'center' }}>
                                                        No items found
                                                    </Text>
                                                </View>
                                            )}
                                        </View>
                                    );
                                }}
                                data={this.state.dataToShow}
                                extraData={this.state}
                                keyExtractor={this._keyExtractor}
                                ListHeaderComponent={this._renderListHeaderComponent}
                                ItemSeparatorComponent={this._renderItemSeparatorComponent}
                                renderItem={this._renderItem}
                                renderSectionHeader={this._renderSectionHeader}
                                renderSectionFooter={this._renderSectionFooter}
                                initialNumToRender={10}
                            />
                        )}
                        <Animatable.View
                            useNativeDriver
                            animation="fadeIn"
                            duration={300}
                            style={styles.submit_btn_container}
                        >
                            <ButtonPrimary
                                onPress={this._handleOrderRoomService}
                                loading={this.state.loading}
                                disabled={this.state.loading}
                                text={confirm_order}
                                backgroundColor={color || colors.BROWN}
                            />
                        </Animatable.View>
                        <SafeAreaView style={{ marginBottom: wp(4) }} />
                    </View>

                    <CustomModal
                        ref={this._modalNoteOrderItem}
                        animationIn="fadeInUp"
                        animationOut="fadeOutDown"
                    >
                        <NoteOrderItem
                            value={this.state.selectedItem.note}
                            onChangeText={this._onChangeText}
                            showModal={this._handleModalNoteOrderItem(null, true)}
                            title={`${this.state.selectedItem.name} Note`}
                            color={color}
                        />
                    </CustomModal>
                </View>
            );
        }
    }
}

const styles = StyleSheet.create({
    submit_btn_container: {
        paddingHorizontal: scale.w(57),
        marginTop: scale.w(18),
    },
    searchview: {
        marginHorizontal: scale.w(21),

        height: wp(12),
        // width:wp('100%'),
        justifyContent: 'center',
        marginTop: 10,
        borderRadius: 50,
        backgroundColor: '#ECECEC',
        // opacity:0.5
    },
    modalContainer: {
        height: '100%',
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        marginHorizontal: 10,
    },
    modal: {
        height: '100%',
        marginLeft: -1,
        paddingVertical: 20,
        marginBottom: -1,
    },
    image: {
        alignContent: 'center',
        width: '100%',
        height: hp('100%'),
        resizeMode: 'contain',
        alignSelf: 'center',
        // position: 'relative',
    },
});

export default OrderRoomService;
