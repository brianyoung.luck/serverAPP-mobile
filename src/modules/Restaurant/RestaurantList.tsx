import React from 'react';
import {
    View,
    StyleSheet,
    FlatList,
    RefreshControl,
    SafeAreaView,
    TouchableOpacity,
    Platform,
    Image,
    Text,
    ActivityIndicator,
} from 'react-native';
import base from '../../utils/baseStyles';
import { scale } from '../../utils/dimensions';
import { H4 } from '../_global/Text';
import Navbar from '../_global/Navbar';
import { Navigation } from 'react-native-navigation';
import { restaurantService, trackingProgress, mainmenu } from '../../utils/navigationControl';
import { IRestaurantListReduxProps } from './RestaurantList.Container';
import { IRestaurant } from '../../types/restaurant';
import * as Animatable from 'react-native-animatable';
import { ButtonPrimary } from '../_global/Button';
import colors from '../../constants/colors';
import { debounce } from 'lodash';

export interface IRestaurantListProps extends IRestaurantListReduxProps {
    componentId: string;
    backGround?: boolean;
}

interface IRestaurantListState {
    loadingGet: boolean;
}

class RestaurantList extends React.Component<IRestaurantListProps, IRestaurantListState> {
    constructor(props: IRestaurantListProps) {
        super(props);

        this.state = {
            loadingGet: false,
        };

        Navigation.mergeOptions(props.componentId, {
            statusBar: {
                backgroundColor: props.color,
                style: 'light',
            },
        });
        this._fetch = this._fetch.bind(this);
        this._handleBack = this._handleBack.bind(this);
        this._handleRestaurantList = this._handleRestaurantList.bind(this);
        this._keyExtractor = this._keyExtractor.bind(this);
        this._renderListHeaderComponent = this._renderListHeaderComponent.bind(this);
        this._renderItem = this._renderItem.bind(this);
        this._handleMyOrders = this._handleMyOrders.bind(this);
        this._listEmptyComponent = this._listEmptyComponent.bind(this);
    }

    componentDidMount() {
        // this.props.getRestaurantList();
        this._fetch();
    }

    _fetch() {
        console.log('hererereerere get callll');
        this.setState({ loadingGet: true });
        this.props.getRestaurantList(
            () => {
                this.setState({ loadingGet: false });
            },
            () => {
                this.setState({ loadingGet: false });
            },
        );
    }

    _handleBack() {
        if (this.props.backGround) {
            Navigation.push(this.props.componentId, mainmenu);
        } else {
            Navigation.pop(this.props.componentId);
        }
    }

    _handleRestaurantList(restaurant: IRestaurant) {
        Navigation.push(
            this.props.componentId,
            restaurantService({
                restaurant,
                color: this.props.color,
                selectedLanguage: this.props.selectedLanguage,
            }),
        );
    }

    _keyExtractor(item: IRestaurant) {
        return item.id.toString();
    }

    _renderListHeaderComponent() {
        return (
            <View style={styles.text_container}>
                <H4 fontSize={scale.w(18)} textAlign="center">
                    {this.props.selectedLanguage.please_select_a_restaurant_to_continue}
                </H4>
            </View>
        );
    }

    _renderItem({ item }: { item: IRestaurant }) {
        return (
            <View style={{ alignItems: 'center', marginRight: scale.w(8), paddingVertical: scale.w(8) }}>
                <TouchableOpacity
                    onPress={debounce(() => this._handleRestaurantList(item), 1000, {
                        leading: true,
                        trailing: false,
                    })}
                    activeOpacity={1}
                >
                    <Animatable.View
                        useNativeDriver
                        animation="fadeInLeft"
                        duration={400}
                        delay={Math.floor(Math.random() * 100)}
                        style={{
                            backgroundColor: colors.WHITE,
                            borderRadius: scale.w(20),
                            alignItems: 'center',
                            justifyContent: 'center',
                            marginHorizontal: scale.w(10),
                            marginVertical: scale.w(10),
                            paddingVertical: scale.w(12),
                            paddingHorizontal: scale.w(12),
                            width: scale.w(265),
                            ...Platform.select({
                                ios: {
                                    shadowColor: '#000',
                                    shadowOffset: { width: 0, height: 1 },
                                    shadowOpacity: 0.1,
                                    shadowRadius: 6,
                                },
                                android: {
                                    elevation: 8,
                                },
                            }),
                        }}
                    >
                        <Image
                            source={{ uri: item.logo_url }}
                            style={{
                                width: scale.w(230),
                                height: scale.w(90),
                                marginBottom: scale.w(8),
                            }}
                            resizeMode="contain"
                        />
                        <H4 textAlign="center" fontSize={scale.w(16)}>
                            {item.name}
                        </H4>
                    </Animatable.View>
                </TouchableOpacity>
            </View>
        );
    }

    _handleMyOrders() {
        Navigation.push(this.props.componentId, trackingProgress);
    }

    _listEmptyComponent() {
        return (
            <View>
                <Text>No restaurants</Text>
            </View>
        );
    }

    render() {
        const { color } = this.props;
        const { restaurant, my_orders } = this.props.selectedLanguage;
        console.log('Printing props,of restauant service == ', this.props);
        console.log('loader ', this.state.loadingGet);
        return (
            <View style={base.container}>
                <Navbar isBrown={color === ''} color={color} onClick={this._handleBack} title={restaurant} />

                <View style={{ flex: 1, justifyContent: 'center' }}>
                    <FlatList
                        refreshControl={
                            <RefreshControl onRefresh={this._fetch} refreshing={this.state.loadingGet} />
                        }
                        ListEmptyComponent={() => {
                            return (
                                <View>
                                    {this.state.loadingGet ? (
                                        <ActivityIndicator size="large" color={'#fff'} />
                                    ) : (
                                        <View style={{ marginTop: scale.h(250) }}>
                                            <Text style={{ alignSelf: 'center' }}>No restauant found</Text>
                                        </View>
                                    )}
                                </View>
                            );
                        }}
                        data={this.props.restaurants}
                        extraData={this.state}
                        keyExtractor={this._keyExtractor}
                        renderItem={this._renderItem}
                        initialNumToRender={10}
                    />
                </View>
                <Animatable.View
                    useNativeDriver
                    animation="fadeIn"
                    duration={300}
                    style={{ paddingHorizontal: scale.w(57), paddingBottom: scale.w(20) }}
                >
                    <ButtonPrimary
                        onPress={this._handleMyOrders}
                        text={my_orders}
                        backgroundColor={color || colors.BROWN}
                    />
                </Animatable.View>
                <SafeAreaView />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    text_container: {
        marginTop: scale.w(10),
        marginBottom: scale.w(20),
        marginHorizontal: scale.w(28),
    },
});

export default RestaurantList;
