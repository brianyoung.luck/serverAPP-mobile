import { connect } from 'react-redux';
import OrderRoomService from './OrderRoomService';
import { bindActionCreators, Dispatch } from 'redux';
import { IState } from '../../types/state';
import { getRestaurantCategoryDish, orderRoomService } from '../../actions/action.restaurant';
import { createSelector } from 'reselect';
import { startCase, upperFirst } from 'lodash';

const dishCategoriesSelector = createSelector(
    (state: IState) => state.restaurant.dishCategories,
    (dishes) => {
        return dishes.map(({ name, dishes }) => ({
            title: upperFirst(startCase(name)),
            data: Object.values(dishes),
        }));
    },
);

const mapStateToProps = (state: IState) => ({
    dishesCategories: dishCategoriesSelector(state),
    color: state.hotel.icon.restaurant_color,
    currency: state.hotel.currency,
    selectedLanguage: state.language,
});

const mapDispatchToProps = (dispatch: Dispatch) => {
    return bindActionCreators(
        {
            getRestaurantCategoryDish,
            orderRoomService,
        },
        dispatch,
    );
};

export interface IOrderRoomServiceReduxProps
    extends ReturnType<typeof mapStateToProps>,
        ReturnType<typeof mapDispatchToProps> {}

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(OrderRoomService);
