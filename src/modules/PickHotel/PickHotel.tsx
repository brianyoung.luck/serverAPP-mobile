import React from 'react';
import {
    View,
    StyleSheet,
    ScrollView,
    KeyboardAvoidingView,
    Platform,
    Keyboard,
    TouchableOpacity,
    Text,
} from 'react-native';
import { View as ViewAnimatable } from 'react-native-animatable';
import { IPickHotelReduxProps } from './PickHotel.Container';
import base from '../../utils/baseStyles';
import colors from '../../constants/colors';
import { H1, H3 } from '../_global/Text';
import { ButtonPrimary } from '../_global/Button';
import FieldForm from '../CheckIn/Components/FieldForm';
import { Navigation } from 'react-native-navigation';
import { mainmenu } from '../../utils/navigationControl';
import { scale } from '../../utils/dimensions';

interface IPickHotelProps extends IPickHotelReduxProps {
    componentId: string;
}

interface IPickHotelState {
    loading: boolean;
    code: string;
}

class PickHotel extends React.Component<IPickHotelProps, IPickHotelState> {
    constructor(props: IPickHotelProps) {
        super(props);

        this.state = {
            loading: false,
            code: '',
        };

        Navigation.mergeOptions(props.componentId, {
            statusBar: {
                backgroundColor: colors.LIGHT_BLUE,
                style: 'light',
            },
        });
        this._handleContinue = this._handleContinue.bind(this);
    }

    _handleContinue() {
        Keyboard.dismiss();
        this.setState({ loading: true });
        this.props.getHotelDetail(
            this.state.code,
            () => {
                Navigation.setRoot({ root: mainmenu });
                this.setState({ loading: false });
            },
            () => {
                this.setState({ loading: false });
            },
        );
    }

    render() {
        return (
            <View style={base.container}>
                <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding" enabled={Platform.OS === 'ios'}>
                    <ScrollView keyboardShouldPersistTaps="handled">
                        <ViewAnimatable
                            duration={400}
                            animation="fadeInUp"
                            style={styles.titleContainer}
                            useNativeDriver
                        >
                            <H1 textAlign="center" color={colors.BLUE} fontSize={scale.w(46)}>
                                {'Servr'}
                            </H1>
                            <H3 textAlign="center" color={colors.BLUE} fontSize={scale.w(18)}>
                                {'Welcome'}
                            </H3>
                        </ViewAnimatable>

                        <ViewAnimatable
                            animation="fadeInUp"
                            delay={100}
                            duration={400}
                            style={styles.formContainer}
                            useNativeDriver
                        >
                            <FieldForm
                                label="Hotel code:"
                                placeholder="i.e. ABC"
                                value={this.state.code}
                                onChangeText={(code) => this.setState({ code })}
                                autoCapitalize="none"
                                returnKeyType="done"
                                autoFocus
                                onSubmitEditing={this._handleContinue}
                            />

                            <View style={{ paddingHorizontal: scale.w(40) }}>
                                <H3 textAlign="center">
                                    {
                                        'Hotel code should be shown within your booking email confirmation, or you can ask your hotel'
                                    }
                                </H3>
                            </View>

                            <View style={{ height: scale.w(40) }} />

                            <View style={{ paddingHorizontal: scale.w(40) }}>
                                <ButtonPrimary
                                    loading={this.state.loading}
                                    onPress={this._handleContinue}
                                    text="Continue"
                                />
                            </View>
                        </ViewAnimatable>
                    </ScrollView>
                </KeyboardAvoidingView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    titleContainer: {
        flex: 1,
        justifyContent: 'center',
        marginTop: scale.w(100),
    },

    // form style
    formContainer: {
        marginVertical: scale.w(30),
    },
});

export default PickHotel;
