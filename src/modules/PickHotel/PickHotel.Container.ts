import { connect } from 'react-redux';
import PickHotel from './PickHotel';
import { bindActionCreators, Dispatch } from 'redux';
import { IState } from '../../types/state';
import { getHotelDetail } from '../../actions/action.hotel';
import { selectLanguage } from '../../actions/action.language';

const mapStateToProps = (state: IState) => ({
    selectedLanguage: state.language,
});

const mapDispatchToProps = (dispatch: Dispatch) => {
    return bindActionCreators(
        {
            getHotelDetail,
            selectLanguage,
        },
        dispatch,
    );
};

export interface IPickHotelReduxProps
    extends ReturnType<typeof mapStateToProps>,
        ReturnType<typeof mapDispatchToProps> {}

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(PickHotel);
