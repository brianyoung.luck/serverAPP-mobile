import React from 'react';
import { Separator } from '../../_global/Container';
import { StyleSheet } from 'react-native';
import { widthPercentageToDP, scale, heightPercentageToDP } from '../../../utils/dimensions';
import colors from '../../../constants/colors';
import { H1, H4 } from '../../_global/Text';
import * as Animatable from 'react-native-animatable';
import { TLateCheckoutStatus } from '../../../types/account';

interface IMessageCheckOut {
    pending: string;
    accepted: string;
    rejected: string;
}

interface ILateCheckOutProps {
    checkoutTime: string;
    lateCheckoutTime: string;
    lateCheckoutStatus: TLateCheckoutStatus;
    color?: string;
    selectedLanguage?: any;
}

export default class LateCheckOut extends React.PureComponent<ILateCheckOutProps> {
    render() {
        const { lateCheckoutStatus, lateCheckoutTime, checkoutTime } = this.props;

        const message: IMessageCheckOut = {
            pending: this.props.selectedLanguage.your_late_check_out_request_has_been_sent,
            accepted: this.props.selectedLanguage.please_check_back_to_see_if_your_request_was_accepted,
            rejected: 'Your late check out request has been rejected.',
        };

        const messageSecondary: IMessageCheckOut = {
            pending: this.props.selectedLanguage.please_check_back_to_see_if_your_request_was_accepted,
            accepted: this.props.selectedLanguage.your_new_check_out_time_is,
            rejected: this.props.selectedLanguage.your_check_out_time_is,
        };

        return (
            <>
                {(lateCheckoutStatus === 'pending' || lateCheckoutStatus === 'accepted') && (
                    <Animatable.Image
                        useNativeDriver
                        animation="bounceIn"
                        duration={400}
                        source={require('../../../images/icon_smiley.png')}
                        style={[styles.image, { tintColor: this.props.color }]}
                    />
                )}

                <Animatable.View
                    useNativeDriver
                    animation="fadeInUp"
                    duration={400}
                    delay={100}
                    style={styles.input_container}
                >
                    <H1 fontSize={scale.w(24)} textAlign="center" color={this.props.color}>
                        {message[lateCheckoutStatus ? lateCheckoutStatus : 'pending']}
                    </H1>
                    <Separator height={10} />
                    <H4 textAlign="center" fontSize={scale.w(20)} color={colors.DARK_GREY}>
                        {messageSecondary[lateCheckoutStatus ? lateCheckoutStatus : 'pending']}
                    </H4>
                    <Separator height={10} />
                    {lateCheckoutStatus && lateCheckoutStatus !== 'pending' && (
                        <H1 fontSize={scale.w(50)} textAlign="center" color={this.props.color}>
                            {lateCheckoutStatus === 'accepted' ? lateCheckoutTime : checkoutTime}
                        </H1>
                    )}
                </Animatable.View>
            </>
        );
    }
}

const styles = StyleSheet.create({
    input_container: {
        width: widthPercentageToDP(92),
        alignSelf: 'center',
        marginBottom: heightPercentageToDP(2),
        alignItems: 'center',
        marginTop: scale.w(62),
    },
    image: {
        width: scale.w(120),
        height: scale.w(120),
        alignSelf: 'center',
    },
});
