import React from 'react';
import { View, StyleSheet, ActivityIndicator } from 'react-native';
import base from '../../utils/baseStyles';
import Navbar from '../_global/Navbar';
import { Navigation } from 'react-native-navigation';
import RequestCheckOut from './Components/RequestCheckOut';
import LateCheckOut from './Components/LateCheckOut';
import { ICheckOutReduxProps } from './CheckOut.Container';
import colors from '../../constants/colors';
import { H4 } from '../_global/Text';
import { Separator } from '../_global/Container';
import { scale } from '../../utils/dimensions';
import { Text } from 'react-native-animatable';

interface ICheckOutProps extends ICheckOutReduxProps {
    componentId: string;
}

interface ICheckOutState {
    loading: boolean;
    loadingGet: boolean;
    request: boolean;
    loadingCheckOut: boolean;
}

class CheckOut extends React.PureComponent<ICheckOutProps, ICheckOutState> {
    constructor(props: ICheckOutProps) {
        super(props);

        this.state = {
            loading: false,
            loadingGet: true,
            request: !!props.late_checkout_status,
            loadingCheckOut: false,
        };

        Navigation.mergeOptions(props.componentId, {
            statusBar: {
                backgroundColor: props.color,
                style: 'light',
            },
        });
        this._handleBack = this._handleBack.bind(this);
        this._handleRequest = this._handleRequest.bind(this);
        this._handleCheckOut = this._handleCheckOut.bind(this);
    }

    componentDidMount() {
        this.props.getProfile(
            () => {
                this.setState({ loadingGet: false });
            },
            () => {
                this.setState({ loadingGet: false });
            },
        );
    }

    UNSAFE_componentWillReceiveProps(nextProps: ICheckOutProps) {
        if (this.props.late_checkout_status !== nextProps.late_checkout_status) {
            this.setState({ request: !!nextProps.late_checkout_status });
        }
    }

    _handleBack() {
        Navigation.pop(this.props.componentId);
    }

    _handleRequest() {
        this.setState({ loading: true });

        this.props.lateCheckOut(
            () => {
                this.setState({ loading: false, request: true });
            },
            () => {
                this.setState({ loading: false });
            },
        );
    }

    _handleCheckOut() {
        this.setState({ loadingCheckOut: true });

        this.props.userCheckOut(
            () => {
                this.setState({ loadingCheckOut: false, request: true });
            },
            () => {
                this.setState({ loadingCheckOut: false });
            },
        );

        // console.log('herer api willl calll')
        // setTimeout(()=>{
        //     this.setState({ loadingCheckOut: false, });
        //     console.log('api call successfully')

        // }, 4000)
    }

    render() {
        const { checkout_time, late_checkout_time, late_checkout_status, color } = this.props;
        const { check_out } = this.props.selectedLanguage;
        return (
            <View style={base.container}>
                <Navbar color={color} onClick={this._handleBack} title={check_out} />

                <View style={StyleSheet.flatten([base.center_mid, { flex: 1, paddingBottom: scale.w(120) }])}>
                    {this.state.loadingGet ? (
                        <>
                            <ActivityIndicator size="large" color={color || colors.LIGHT_BLUE} />
                            <Separator height={10} />
                            <H4 textAlign="center" fontSize={scale.w(16)}>
                                {'Please wait\nChecking your check out time'}
                            </H4>
                        </>
                    ) : (
                        <>
                            {this.state.request ? (
                                <LateCheckOut
                                    checkoutTime={checkout_time}
                                    lateCheckoutTime={late_checkout_time}
                                    lateCheckoutStatus={late_checkout_status}
                                    color={color}
                                    selectedLanguage={this.props.selectedLanguage}
                                />
                            ) : (
                                <RequestCheckOut
                                    time={checkout_time}
                                    onPress={this._handleRequest}
                                    loading={this.state.loading}
                                    disabled={this.state.loading}
                                    onPressCheckOut={this._handleCheckOut}
                                    loadingCheckOut={this.state.loadingCheckOut}
                                    disabledCheckOut={this.state.loadingCheckOut}
                                    color={color}
                                    selectedLanguage={this.props.selectedLanguage}
                                />
                            )}
                        </>
                    )}
                </View>
            </View>
        );
    }
}

export default CheckOut;
