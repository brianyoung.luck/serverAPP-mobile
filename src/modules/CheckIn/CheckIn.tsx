import React from 'react';
import { View } from 'react-native';
import base from '../../utils/baseStyles';
import Navbar from '../_global/Navbar';
import CheckInForm from './Components/CheckInForm.Container';
import VerifyPhoneNumber from './Components/VerifyPhoneNumber.Container';
import VerifyPin from './Components/VerifyPin.Container';
import { Navigation } from 'react-native-navigation';
import { ICheckInReduxProps } from './CheckIn.Container';
import { mainmenu } from '../../utils/navigationControl';

export interface ICheckInProps extends ICheckInReduxProps {
    componentId: string;
    backGround?: boolean;
}

export type TDisplay = 'form' | 'verify_phone' | 'verify_pin';

interface ICheckInState {
    display: TDisplay;
    phoneNumber: string;
}

class CheckIn extends React.Component<ICheckInProps, ICheckInState> {
    constructor(props: ICheckInProps) {
        super(props);

        this.state = {
            display: 'form',
            phoneNumber: '+62',
        };

        Navigation.mergeOptions(props.componentId, {
            statusBar: {
                backgroundColor: props.icon,
                style: 'light',
            },
        });
        this._handleBack = this._handleBack.bind(this);
        this._renderContent = this._renderContent.bind(this);
        this._handleChangeDisplay = this._handleChangeDisplay.bind(this);
    }

    _handleBack() {
        switch (this.state.display) {
            case 'verify_pin':
                return this.setState({ display: 'verify_phone' });

            case 'verify_phone':
                return this.setState({ display: 'form' });

            default:
                if (this.props.backGround) {
                    return Navigation.push(this.props.componentId, mainmenu);
                } else {
                    return Navigation.pop(this.props.componentId);
                }
        }
    }

    _renderContent() {
        console.log('displayyyy====', this.state.display);
        switch (this.state.display) {
            case 'form':
                return (
                    <CheckInForm
                        componentId={this.props.componentId}
                        handleChangeDisplay={this._handleChangeDisplay}
                    />
                );

            case 'verify_phone':
                return (
                    <VerifyPhoneNumber
                        componentId={this.props.componentId}
                        handleChangeDisplay={this._handleChangeDisplay}
                        phoneNumber={this.state.phoneNumber}
                        onChangePhoneNumber={(phoneNumber) => this.setState({ phoneNumber })}
                    />
                );

            default:
                return (
                    <VerifyPin componentId={this.props.componentId} phoneNumber={this.state.phoneNumber} />
                );
        }
    }

    _handleChangeDisplay(display: TDisplay) {
        this.setState({ display });
    }

    render() {
        return (
            <View style={base.container}>
                <Navbar
                    color={this.props.icon}
                    onClick={this._handleBack}
                    title={this.props.selectedLanguage.check_in}
                />

                {this._renderContent()}
            </View>
        );
    }
}

export default CheckIn;
