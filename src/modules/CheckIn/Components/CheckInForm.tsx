import React, { createRef } from 'react';
import {
    View,
    ScrollView,
    Image,
    TouchableOpacity,
    KeyboardAvoidingView,
    FlatList,
    Alert,
    Keyboard,
    StyleSheet,
    NativeSyntheticEvent,
    NativeScrollEvent,
    Platform,
} from 'react-native';
import colors from '../../../constants/colors';
import { scale, widthPercentageToDP as wp, heightPercentageToDP as hp } from '../../../utils/dimensions';
import { ButtonPrimary, ButtonSecondary } from '../../_global/Button';
import MenuButton from '../../_global/MenuButton';
import FieldForm from './FieldForm';
import ImagePicker, { ImagePickerOptions, ImagePickerResponse } from 'react-native-image-picker';
import { Text, View as ViewAnimatable } from 'react-native-animatable';
import { CardIOModule } from 'react-native-awesome-card-io';
import { H2, H3, H4 } from '../../_global/Text';
import { Separator } from '../../_global/Container';
import { format } from 'date-fns';
import { TDisplay } from '../CheckIn';
import { ICheckInFormReduxProps } from './CheckInForm.Container';
import { repeat } from '../../../utils/formating';
import { IPhoto } from '../../../types/action.account';
import { Navigation } from 'react-native-navigation';
import uuid from 'uuid/v4';
import CustomModal from '../../_global/CustomModal';
import ModalDatePicker from './ModalDatePicker';
import FieldFormWithMask from './FieldFormWithMask';
import { debounce, isEqual, padStart, stubString } from 'lodash';
import Carousel from 'react-native-snap-carousel';
import base from '../../../utils/baseStyles';
import ModalSelectPhoto from './ModalSelectPhoto';
import ModalMessageScanCC from './ModalMessageScanCC';
import DropDownPicker from 'react-native-dropdown-picker';
import { languages } from '../../_global/languages';
import SignatureCapture from 'react-native-signature-capture';
import Modal from 'react-native-modal';
import ViewShot from 'react-native-view-shot';
import { Icon } from 'react-native-vector-icons/Icon';

const IMAGE_PICKER_OPTIONS: ImagePickerOptions = {
    title: 'Select Passport Photo',
    storageOptions: {
        skipBackup: true,
        path: 'images',
    },
    cameraType: 'front',
    allowsEditing: false,
    maxHeight: 800,
    maxWidth: 600,
    quality: 0.6,
    mediaType: 'photo',
    takePhotoButtonTitle: 'Take Photo',
    chooseFromLibraryButtonTitle: 'Choose from Library',
};

interface ICheckInFormProps extends ICheckInFormReduxProps {
    componentId: string;
    handleChangeDisplay: (display: TDisplay) => void;
}

interface ICheckInFormState {
    isArrivalDate: boolean;
    indexOfPassportToBeAdded: number | null;
    loading: boolean;
    listNumberGuest: number[];
    numberGuest: number;
    listavatar: IPhoto[];
    arrivalDate: string;
    departureDate: string;
    cardNumber: string;
    cardName: string;
    cardExpiryDate: string;
    cardAddress: string;
    phoneNumber: string;
    reference: string;
    room_Number: string | number;
    country: string;
    note_request: string;
    signature_photo: Object;
    modalVisible: boolean;
    terms_and_condition: boolean;
}

interface ICard {
    cardNumber: string;
    cardType: string;
    cardholderName: null | string;
    cvv: string;
    expiryMonth: number;
    expiryYear: number;
    postalCode: null | string;
    redactedCardNumber: string;
}

// var sign = createRef();
// var viewRef = createRef();

class CheckInForm extends React.Component<ICheckInFormProps, ICheckInFormState> {
    private _modalDatePicker = React.createRef<CustomModal>();
    private _modalImagePicker = React.createRef<CustomModal>();
    private _modalMessageScanCC = React.createRef<CustomModal>();
    private sign = React.createRef();
    private viewRef = React.createRef();

    constructor(props: ICheckInFormProps) {
        super(props);
        this.state = {
            indexOfPassportToBeAdded: null,
            isArrivalDate: false,
            loading: false,
            listNumberGuest: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
            numberGuest: props.profile.passport_photos.length,
            listavatar: props.profile.passport_photos,
            arrivalDate: props.profile.arrival_date,
            departureDate: props.profile.departure_date,
            cardNumber: props.profile.card_number,
            cardName: props.profile.cardholder_name,
            cardExpiryDate: props.profile.card_expiry_date,
            cardAddress: props.profile.card_address,
            phoneNumber: props.profile.phone_number,
            reference: props.profile.reference,
            room_Number: props.profile.room_number,
            country: props.selectedLanguage.lang,
            note_request: props.profile.note_request,
            signature_photo: props.profile.signature_photo,
            modalVisible: false,
            terms_and_condition: false,
        };
        this._handleSubmit = this._handleSubmit.bind(this);
        this._handleSelectNumberOfGuest = this._handleSelectNumberOfGuest.bind(this);
        this._handleProcessImage = this._handleProcessImage.bind(this);
        this._showModalImagePicker = this._showModalImagePicker.bind(this);
        this._openCamera = this._openCamera.bind(this);
        this._openLibrary = this._openLibrary.bind(this);
        this._handleMessageScanCC = this._handleMessageScanCC.bind(this);
        this._handleScanCC = this._handleScanCC.bind(this);
        this._onChangeDate = this._onChangeDate.bind(this);
        this._handleModalDatePicker = this._handleModalDatePicker.bind(this);
        this._renderPhotoPicker = this._renderPhotoPicker.bind(this);
        this._onScroll = this._onScroll.bind(this);
        this._renderScrollNumber = this._renderScrollNumber.bind(this);
        this._onDragEvent = this._onDragEvent.bind(this);
        this.resetSign = this.resetSign.bind(this);
        this.setModalVisible = this.setModalVisible.bind(this);
        this.toggleTerms = this.toggleTerms.bind(this);
        this.onCapture = this.onCapture.bind(this);
    }

    componentDidMount() {
        if (this.props.isCheckedIn) {
            this.props.getProfile();
        }
    }

    UNSAFE_componentWillReceiveProps(nextProps: ICheckInFormProps) {
        console.log('next props', nextProps);
        if (!isEqual(this.props.profile, nextProps.profile)) {
            this.setState({
                numberGuest: nextProps.profile.passport_photos.length,
                listavatar: nextProps.profile.passport_photos,
                arrivalDate: nextProps.profile.arrival_date,
                departureDate: nextProps.profile.departure_date,
                cardName: nextProps.profile.cardholder_name,
                cardExpiryDate: nextProps.profile.card_expiry_date,
                cardAddress: nextProps.profile.card_address,
                phoneNumber: nextProps.profile.phone_number,
                cardNumber: nextProps.profile.card_number,
                reference: nextProps.profile.reference,
                // signature_photo: nextProps.profile.signature_photo
            });
        }
    }

    _handleSubmit() {
        Keyboard.dismiss();

        if (this.state.listavatar.map((avatar) => avatar.uri).includes('')) {
            Alert.alert('Attention', 'Please upload all passport photos!');
            return false;
        }

        this.setState({ loading: true });
        const {
            listavatar,
            arrivalDate,
            departureDate,
            cardName,
            cardExpiryDate,
            cardAddress,
            phoneNumber,
            cardNumber,
            reference,
            note_request,
            signature_photo,
            terms_and_condition,
        } = this.state;
        const { idHotel } = this.props;

        this.props.checkIn(
            {
                hotel_id: idHotel,
                passport_photos: listavatar,
                arrival_date: format(arrivalDate, 'YYYY-MM-DD 00:00'),
                departure_date: format(departureDate, 'YYYY-MM-DD 23:59'),
                cardholder_name: cardName,
                card_expiry_date: cardExpiryDate,
                card_address: cardAddress,
                phone_number: phoneNumber.replace(/( )/gi, ''),
                card_number: cardNumber,
                reference,
                note_request,
                signature_photo,
                terms_and_condition,
            },
            async () => {
                await Navigation.pop(this.props.componentId);
                Alert.alert(
                    'Success',
                    'Check in request was successful! Please wait for your hotel to accept your check in.',
                );
                this.setState({ loading: false });
            },
            () => {
                this.setState({ loading: false });
            },
        );
    }

    _handleSelectNumberOfGuest(num: number) {
        this.setState((prevState) => ({
            ...prevState,
            numberGuest: num,
            listavatar:
                num > prevState.listavatar.length - 1
                    ? [
                          ...prevState.listavatar,
                          ...repeat<IPhoto>(num - prevState.listavatar.length, {
                              uri: '',
                              name: '',
                              type: 'image/jpeg',
                          }),
                      ]
                    : prevState.listavatar.slice(0, num),
        }));
    }

    _handleProcessImage(response: ImagePickerResponse) {
        if (response.didCancel || response.error || response.customButton) {
            if (__DEV__) {
                console.log('PICKER_IMAGE: ', { response });
            }
            return false;
        }

        this.setState((prevState) => ({
            ...prevState,
            listavatar: prevState.listavatar.map((avatar, idx) => {
                if (this.state.indexOfPassportToBeAdded === idx) {
                    return {
                        uri: response.uri,
                        name: response.fileName ? response.fileName : uuid(),
                        type: 'image/jpeg',
                    };
                }

                return avatar;
            }),
        }));

        if (this._modalImagePicker.current) {
            this._modalImagePicker.current.hide();
        }
    }

    _showModalImagePicker(index: number, isShow?: boolean) {
        return () => {
            this.setState({ indexOfPassportToBeAdded: index });
            if (isShow) {
                this._modalImagePicker.current!.show();
            } else {
                this._modalImagePicker.current!.hide();
            }
        };
    }

    _openCamera() {
        // Launch Camera:
        ImagePicker.launchCamera(IMAGE_PICKER_OPTIONS, this._handleProcessImage);
    }

    _openLibrary() {
        // Open Image Library:
        ImagePicker.launchImageLibrary(IMAGE_PICKER_OPTIONS, this._handleProcessImage);
    }

    _handleMessageScanCC(isShow?: boolean) {
        if (this._modalMessageScanCC.current) {
            // open modal
            if (isShow) {
                return this._modalMessageScanCC.current.show();
            }

            // close modal and open scan cc
            this._modalMessageScanCC.current.hide();

            return setTimeout(this._handleScanCC, 800);
        }

        // open scan cc, in case modal is not rendered
        return this._handleScanCC();
    }

    async _handleScanCC() {
        console.log('herer in card scannerrrrrr');
        try {
            Keyboard.dismiss();
            const {
                cardholderName,
                cardNumber,
                expiryMonth,
                expiryYear,
            }: ICard = await CardIOModule.scanCard({
                hideCardIOLogo: true,
                requireCVV: false,
                requireCardholderName: true,
            });

            this.setState({
                cardName: cardholderName ? cardholderName : this.state.cardName,
                cardNumber: cardNumber ? cardNumber : '',
                cardExpiryDate: `${padStart(expiryMonth.toString(), 2, '0')}/${expiryYear
                    .toString()
                    .substr(2)}`,
            });
        } catch (error) {
            if (__DEV__) {
                console.log('SCAN_CC: ', { error });
            }
        }
    }

    _onChangeDate(date: Date) {
        this.setState((prevState) => {
            if (prevState.isArrivalDate) {
                return {
                    ...prevState,
                    arrivalDate: date.toString(),
                };
            }

            return {
                ...prevState,
                departureDate: date.toString(),
            };
        });
    }

    _handleModalDatePicker(isArrivalDate: boolean, closeModal?: boolean) {
        return () => {
            Keyboard.dismiss();
            if (!closeModal) {
                this.setState({ isArrivalDate });
            }

            if (this._modalDatePicker.current) {
                if (closeModal) {
                    this._modalDatePicker.current.hide();
                } else {
                    this._modalDatePicker.current.show();

                    // just mod -> in ios, must be change the date, then setstate is execute
                    if (isArrivalDate && this.state.arrivalDate === '') {
                        this._onChangeDate(new Date());
                    } else if (this.state.departureDate === '') {
                        this._onChangeDate(new Date());
                    }
                }
            }
        };
    }

    _renderPhotoPicker({ item, index }: { item: IPhoto; index: number }) {
        if (item.uri !== '') {
            console.log('photo uriiii', item);
            return (
                <View style={base.center_mid}>
                    <TouchableOpacity
                        onPress={debounce(this._showModalImagePicker(index, true), 1000, {
                            leading: true,
                            trailing: false,
                        })}
                        activeOpacity={0.6}
                        disabled={this.props.isCheckedIn}
                    >
                        <View style={styles.photo_picker_container}>
                            <Image
                                source={{ uri: item.uri }}
                                style={styles.passport_photo}
                                resizeMode="cover"
                            />
                        </View>
                    </TouchableOpacity>
                </View>
            );
        }

        return (
            <View style={{ alignItems: 'center', marginBottom: scale.h(8) }}>
                <MenuButton
                    source={require('../../../images/icon_passport_photo.png')}
                    onPress={debounce(this._showModalImagePicker(index, true), 1000, {
                        leading: true,
                        trailing: false,
                    })}
                    text={`${this.props.selectedLanguage.passport_photo} #${index + 1}`}
                    width={scale.w(120)}
                    height={scale.w(120)}
                    iconSize={scale.w(40)}
                    fontSize={scale.w(14)}
                    disabled={this.props.isCheckedIn}
                    styleImage={{ tintColor: this.props.color }}
                />
            </View>
        );
    }

    _onScroll(offsetX: number) {
        const rawIndex = offsetX / scale.w(70);

        const index = Math.round(rawIndex);
        const currNumOfGuest = this.state.numberGuest - 1;
        if (currNumOfGuest !== index) {
            this._handleSelectNumberOfGuest(index + 1);
        }
    }

    resetSign() {
        this.sign.current.resetImage();
    }

    _renderScrollNumber({ item }: { item: number }) {
        return (
            <View style={styles.scroll_number_item}>
                <H2>{item}</H2>
            </View>
        );
    }

    _onDragEvent() {
        console.log('dragged');
    }

    setModalVisible(params: boolean) {
        this.setState({
            modalVisible: params,
        });
    }

    toggleTerms() {
        this.setState({
            terms_and_condition: !this.state.terms_and_condition,
        });
    }

    onCapture() {
        this.viewRef.capture().then((uri) => {
            this.setState({
                signature_photo: {
                    name: uri.split('/')[uri.split('/').length - 1],
                    type: 'image/png',
                    uri: uri,
                },
            });
            this.setModalVisible(false);
        });
    }

    render() {
        const {
            isArrivalDate,
            loading,
            listNumberGuest,
            listavatar,
            arrivalDate,
            departureDate,
            cardName,
            cardExpiryDate,
            cardAddress,
            phoneNumber,
            reference,
            room_Number,
            note_request,
            signature_photo,
        } = this.state;
        const {
            check_in,
            guest_checking_in,
            arrival_date,
            departured_date,
            scan_credit_card,
            cardholder_name,
            card_expiry_date,
            card_address,
            booking_referance,
            phone_number,
            confirm_check_in,
            room_number,
            okay,
            scan_credit_card_or_input_manually_in_the_bottom_right_corner,
            note,
            signature,
            tab_to_add_signature,
            clear,
            i_agree_with_all,
            terms_and_condition,
            clear_signature,
            save_signature,
        } = this.props.selectedLanguage;
        console.log('propss', this.props);
        return (
            <KeyboardAvoidingView behavior={Platform.OS === 'ios' ? 'padding' : null} style={{ flex: 1 }}>
                <ScrollView keyboardShouldPersistTaps="handled">
                    <ViewAnimatable
                        animation="fadeInLeft"
                        useNativeDriver
                        duration={300}
                        delay={150}
                        style={styles.number_guest_check_in_container}
                    >
                        <DropDownPicker
                            items={[
                                { label: 'English', value: 'english' },
                                { label: 'Spanish', value: 'spanish' },
                                { label: 'French', value: 'french' },
                                { label: 'Russian', value: 'russian' },
                                { label: 'Portugese', value: 'portugese' },
                                { label: 'Chinese', value: 'chinese' },
                                { label: 'Italian', value: 'italian' },
                                { label: 'Hebrew', value: 'hebrew' },
                                { label: 'Arabic', value: 'arabic' },
                                { label: 'Indonesian', value: 'indonesian' },
                                { label: 'Dutch', value: 'dutch' },
                                { label: 'German', value: 'german' },
                                { label: 'Japanese', value: 'japanese' },
                            ]}
                            // containerStyle={{width: wp(150), height: hp(70)}}
                            // // dropDownStyle={{backgroundColor: '#fafafa', height: hp(30), width: wp(50)}}
                            defaultValue={this.state.country}
                            labelStyle={{ fontFamily: 'Roboto-Light', color: colors.BLACK }}
                            activeLabelStyle={{ fontFamily: 'Roboto-Bold', color: colors.BLACK }}
                            containerStyle={{ height: scale.h(40), width: scale.w(120) }}
                            style={{
                                alignItems: 'center',
                                justifyContent: 'center',
                                width: scale.w(120),
                                height: scale.w(50),
                                borderWidth: 1,
                                borderColor: colors.GREY,
                                borderRadius: scale.w(8),
                                backgroundColor: colors.WHITE,
                                alignSelf: 'center',
                                marginHorizontal: scale.w(10),
                            }}
                            itemStyle={{
                                justifyContent: 'flex-start',
                            }}
                            dropDownStyle={{ backgroundColor: colors.WHITE }}
                            onChangeItem={async (item) => {
                                await this.setState({
                                    country: item.value,
                                });
                                if (this.state.country == 'english') {
                                    await this.props.selectLanguage(languages.english);
                                } else if (this.state.country == 'spanish') {
                                    await this.props.selectLanguage(languages.spanish);
                                } else if (this.state.country == 'french') {
                                    await this.props.selectLanguage(languages.french);
                                } else if (this.state.country == 'russian') {
                                    await this.props.selectLanguage(languages.russian);
                                } else if (this.state.country == 'chinese') {
                                    await this.props.selectLanguage(languages.chinese);
                                } else if (this.state.country == 'italian') {
                                    await this.props.selectLanguage(languages.italian);
                                } else if (this.state.country == 'hebrew') {
                                    await this.props.selectLanguage(languages.hebrew);
                                } else if (this.state.country == 'arabic') {
                                    await this.props.selectLanguage(languages.arabic);
                                } else if (this.state.country == 'indonesian') {
                                    await this.props.selectLanguage(languages.indonesian);
                                } else if (this.state.country == 'dutch') {
                                    await this.props.selectLanguage(languages.dutch);
                                } else if (this.state.country == 'german') {
                                    await this.props.selectLanguage(languages.german);
                                } else if (this.state.country == 'japanese') {
                                    await this.props.selectLanguage(languages.japanese);
                                } else if (this.state.country == 'portugese') {
                                    await this.props.selectLanguage(languages.portugese);
                                }
                            }}
                        />
                        <Separator height={15} />

                        {this.props.isCheckedIn == true ? (
                            <View style={{ height: scale.h(50) }}>
                                {/* <Separator height={8} /> */}

                                <H3 fontSize={scale.w(18)} textAlign="center">
                                    {room_number}
                                    {':'}
                                </H3>
                                <Separator height={8} />
                                <View style={styles.room_number_view}>
                                    <H2>{room_Number}</H2>
                                </View>
                            </View>
                        ) : null}

                        <Separator height={40} />

                        <H3 fontSize={scale.w(18)} textAlign="center">
                            {guest_checking_in}
                            {':'}
                        </H3>
                        <Separator height={8} />

                        {!this.props.isCheckedIn && (
                            <View style={{ height: scale.h(80) }}>
                                <Carousel
                                    data={listNumberGuest}
                                    renderItem={this._renderScrollNumber}
                                    sliderWidth={scale.w(250)}
                                    itemWidth={scale.w(70)}
                                    onScroll={(e: NativeSyntheticEvent<NativeScrollEvent>) => {
                                        this._onScroll(e.nativeEvent.contentOffset.x);
                                    }}
                                />
                            </View>
                        )}

                        <Separator height={24} />

                        <FlatList
                            data={listavatar}
                            extraData={this.state}
                            keyExtractor={(item, index) => index.toString()}
                            numColumns={2}
                            showsHorizontalScrollIndicator={false}
                            ListHeaderComponent={() => <View style={{ width: scale.w(8) }} />}
                            ListFooterComponent={() => <View style={{ width: scale.w(20) }} />}
                            renderItem={this._renderPhotoPicker}
                        />
                    </ViewAnimatable>

                    <View style={{ height: scale.w(20) }} />

                    <ViewAnimatable animation="fadeInUp" useNativeDriver duration={300} delay={150}>
                        <TouchableOpacity
                            onPress={debounce(this._handleModalDatePicker(true), 1000, {
                                leading: true,
                                trailing: false,
                            })}
                            disabled={this.props.isCheckedIn}
                            activeOpacity={0.7}
                        >
                            <FieldForm
                                label={arrival_date + ':'}
                                value={arrivalDate === '' ? 'DD/MM/YY' : format(arrivalDate, 'DD/MM/YYYY')}
                                isEmpty={arrivalDate === ''}
                                textOnly
                            />
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={debounce(this._handleModalDatePicker(false), 1000, {
                                leading: true,
                                trailing: false,
                            })}
                            disabled={this.props.isCheckedIn}
                            activeOpacity={0.7}
                        >
                            <FieldForm
                                label={departured_date + ':'}
                                value={
                                    departureDate === '' ? 'DD/MM/YY' : format(departureDate, 'DD/MM/YYYY')
                                }
                                isEmpty={departureDate === ''}
                                textOnly
                            />
                        </TouchableOpacity>

                        {!this.props.isCheckedIn && (
                            <View style={{ paddingHorizontal: scale.w(40), marginVertical: scale.w(20) }}>
                                <ButtonSecondary
                                    fontSize={scale.w(16)}
                                    height={scale.w(42)}
                                    text={scan_credit_card}
                                    onPress={() => this._handleMessageScanCC(true)}
                                />
                            </View>
                        )}

                        <FieldForm
                            label={cardholder_name + ':'}
                            placeholder="Name"
                            value={cardName}
                            onChangeText={(cardName) => this.setState({ cardName })}
                            editable={!this.props.isCheckedIn}
                        />

                        <FieldFormWithMask
                            type="datetime"
                            options={{ format: 'MM/YY' }}
                            label={card_expiry_date + ':'}
                            placeholder="MM/YY"
                            value={cardExpiryDate}
                            maxLength={5}
                            onChangeText={(cardExpiryDate) => this.setState({ cardExpiryDate })}
                            editable={!this.props.isCheckedIn}
                        />

                        <FieldForm
                            label={card_address + ':'}
                            placeholder="Address"
                            value={cardAddress}
                            onChangeText={(cardAddress) => this.setState({ cardAddress })}
                            editable={!this.props.isCheckedIn}
                        />

                        <FieldForm
                            label={booking_referance + ':'}
                            placeholder="No."
                            value={reference}
                            onChangeText={(reference) => this.setState({ reference })}
                            editable={!this.props.isCheckedIn}
                            autoCapitalize="none"
                        />

                        <FieldForm
                            label={note + ':'}
                            placeholder="Note"
                            value={note_request}
                            onChangeText={(note_request) => this.setState({ note_request })}
                            editable={!this.props.isCheckedIn}
                            autoCapitalize="none"
                        />

                        <FieldFormWithMask
                            type="custom"
                            options={{ mask: '+9999 999 999 999' }}
                            label={phone_number + ':'}
                            placeholder="No."
                            keyboardType="phone-pad"
                            maxLength={18}
                            value={phoneNumber}
                            onChangeText={(phoneNumber) => this.setState({ phoneNumber })}
                            editable={!this.props.isCheckedIn}
                        />

                        <View
                            style={[
                                { paddingHorizontal: scale.w(40) },
                                this.props.isCheckedIn ? { marginBottom: scale.h(30) } : {},
                            ]}
                        >
                            <H4 fontSize={scale.w(18)}>{signature}</H4>
                            <TouchableOpacity
                                disabled={this.props.isCheckedIn}
                                onPress={() => {
                                    this.setModalVisible(true);
                                }}
                                activeOpacity={1}
                                style={{
                                    width: scale.w(285),
                                    height: scale.w(150),
                                    marginHorizontal: scale.w(25),
                                    borderRadius: scale.w(20),
                                    backgroundColor: '#fff',
                                    alignSelf: 'center',
                                    marginTop: scale.w(10),
                                    flexDirection: 'row',
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    ...Platform.select({
                                        ios: {
                                            shadowColor: '#000',
                                            shadowOffset: { width: 0, height: 4 },
                                            shadowOpacity: 0.2,
                                            shadowRadius: 3,
                                        },
                                        android: {
                                            elevation: 8,
                                        },
                                    }),
                                }}
                            >
                                {this.state.signature_photo.name ? (
                                    <Image
                                        resizeMode="contain"
                                        style={{
                                            height: '100%',
                                            width: '100%',
                                        }}
                                        source={{ uri: this.state.signature_photo.uri }}
                                    />
                                ) : (
                                    <Text>{tab_to_add_signature}</Text>
                                )}
                            </TouchableOpacity>
                            {!this.props.isCheckedIn &&
                                (this.state.signature_photo ? (
                                    <TouchableOpacity
                                        onPress={() => {
                                            this.setState({
                                                signature_photo: '',
                                            });
                                        }}
                                    >
                                        <Text
                                            style={{
                                                fontFamily: 'Roboto-Light',
                                                fontSize: 14,
                                                color: '#2E91EE',
                                                textDecorationLine: 'underline',
                                                alignSelf: 'flex-end',
                                                marginTop: scale.w(10),
                                            }}
                                        >
                                            {clear}
                                        </Text>
                                    </TouchableOpacity>
                                ) : (
                                    <View />
                                ))}
                        </View>
                        {!this.props.isCheckedIn && (
                            <View
                                style={{
                                    paddingHorizontal: scale.w(40),
                                    flexDirection: 'row',
                                    marginTop: scale.w(40),
                                    marginBottom: scale.w(30),
                                    justifyContent: 'space-between',
                                }}
                            >
                                <View style={{ flexDirection: 'row' }}>
                                    <Text
                                        style={{
                                            fontFamily: 'Roboto-Light',
                                            fontSize: 14,
                                            marginTop: scale.w(10),
                                        }}
                                    >
                                        {`${i_agree_with_all} `}
                                    </Text>
                                    <TouchableOpacity>
                                        <Text
                                            style={{
                                                fontFamily: 'Roboto-Light',
                                                fontSize: 14,
                                                color: '#2E91EE',
                                                textDecorationLine: 'underline',
                                                alignSelf: 'flex-end',
                                                marginTop: scale.w(10),
                                            }}
                                        >
                                            {terms_and_condition}
                                        </Text>
                                    </TouchableOpacity>
                                </View>
                                <TouchableOpacity
                                    onPress={this.toggleTerms}
                                    style={[
                                        {
                                            width: scale.w(20),
                                            height: scale.w(20),
                                            borderColor: '#C1C8E4',
                                            borderWidth: 1,
                                            alignSelf: 'flex-end',
                                            marginLeft: scale.w(50),
                                            padding: scale.w(2),
                                            borderRadius: scale.w(5),
                                        },
                                        this.state.terms_and_condition
                                            ? { backgroundColor: this.props.color }
                                            : { backgroundColor: 'white' },
                                    ]}
                                ></TouchableOpacity>
                            </View>
                        )}
                    </ViewAnimatable>

                    {!this.props.isCheckedIn && (
                        <View style={styles.btn_primary_container}>
                            <ButtonPrimary
                                backgroundColor={this.props.color !== '' ? this.props.color : undefined}
                                onPress={this._handleSubmit}
                                loading={loading}
                                disabled={loading}
                                fontSize={scale.w(16.5)}
                                text={confirm_check_in}
                            />
                        </View>
                    )}

                    {/* <CustomModal ref={this._modalDatePicker}>
                        <ModalDatePicker
                            date={isArrivalDate ? new Date(arrivalDate) : new Date(departureDate)}
                            minimumDate={new Date()}
                            onDateChange={this._onChangeDate}
                            showModal={this._handleModalDatePicker(isArrivalDate, true)}
                            title={`Pick your ${isArrivalDate ? 'arrival' : 'departure'} date`}
                            color={this.props.color !== '' ? this.props.color : undefined}
                        />
                    </CustomModal> */}

                    <CustomModal ref={this._modalImagePicker}>
                        <ModalSelectPhoto
                            openCamera={this._openCamera}
                            openLibrary={this._openLibrary}
                            selectedLanguage={this.props.selectedLanguage}
                        />
                    </CustomModal>

                    <CustomModal ref={this._modalMessageScanCC}>
                        <ModalMessageScanCC
                            color={this.props.color !== '' ? this.props.color : undefined}
                            handleMessageScanCC={() => this._handleMessageScanCC(false)}
                            selectedLanguage={this.props.selectedLanguage}
                        />
                    </CustomModal>
                </ScrollView>
                <CustomModal ref={this._modalDatePicker}>
                    <ModalDatePicker
                        date={isArrivalDate ? new Date(arrivalDate) : new Date(departureDate)}
                        minimumDate={new Date()}
                        onDateChange={this._onChangeDate}
                        showModal={this._handleModalDatePicker(isArrivalDate, true)}
                        title={`Pick your ${isArrivalDate ? 'arrival' : 'departure'} date`}
                        color={this.props.color !== '' ? this.props.color : undefined}
                    />
                </CustomModal>
                <Modal
                    backdropOpacity={0.7}
                    isVisible={this.state.modalVisible}
                    onBackdropPress={() => {
                        this.setModalVisible(false);
                    }}
                >
                    <View
                        style={{
                            flex: 1,
                            justifyContent: 'center',
                            paddingHorizontal: wp(2),
                            alignItems: 'center',
                        }}
                    >
                        <View
                            style={{
                                width: scale.w(350),
                                height: scale.w(320),
                                backgroundColor: '#fff',
                                borderRadius: scale.w(20),
                                alignItems: 'center',
                                justifyContent: 'center',
                            }}
                        >
                            <ViewShot
                                ref={(ref) => {
                                    this.viewRef = ref;
                                }}
                                options={{ format: 'png', quality: 0.9 }}
                                captureMode="none"
                            >
                                <SignatureCapture
                                    style={{
                                        height: scale.w(240),
                                        width: scale.w(340),
                                    }}
                                    ref={this.sign}
                                    onDragEvent={this._onDragEvent}
                                    showNativeButtons={false}
                                    showTitleLabel={false}
                                    viewMode={'portrait'}
                                />
                            </ViewShot>
                            <View
                                style={{
                                    flexDirection: 'row',
                                    marginTop: scale.w(10),
                                }}
                            >
                                <View
                                    style={{
                                        width: '50%',
                                        paddingHorizontal: scale.w(10),
                                    }}
                                >
                                    <ButtonPrimary
                                        backgroundColor={
                                            this.props.color !== '' ? this.props.color : undefined
                                        }
                                        onPress={this.resetSign}
                                        loading={false}
                                        disabled={false}
                                        fontSize={scale.w(16.5)}
                                        text={clear_signature}
                                    />
                                </View>
                                <View
                                    style={{
                                        width: '50%',
                                        paddingHorizontal: scale.w(10),
                                    }}
                                >
                                    <ButtonPrimary
                                        backgroundColor={
                                            this.props.color !== '' ? this.props.color : undefined
                                        }
                                        onPress={this.onCapture}
                                        loading={false}
                                        disabled={false}
                                        fontSize={scale.w(16.5)}
                                        text={save_signature}
                                    />
                                </View>
                            </View>
                        </View>
                    </View>
                </Modal>
            </KeyboardAvoidingView>
        );
    }
}

const styles = StyleSheet.create({
    photo_picker_container: {
        width: scale.w(120),
        height: scale.w(120),
        borderRadius: 18,
        backgroundColor: colors.GREY,
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: scale.w(10),
    },
    passport_photo: {
        width: scale.w(120),
        height: scale.w(120),
        borderRadius: 18,
    },
    number_guest_check_in_container: {
        marginVertical: scale.w(10),
        alignItems: 'center',
        marginTop: scale.w(20),
    },
    scroll_number_item: {
        alignItems: 'center',
        justifyContent: 'center',
        width: scale.w(50),
        height: scale.w(50),
        borderWidth: 1,
        borderColor: colors.GREY,
        borderRadius: scale.w(8),
        backgroundColor: colors.WHITE,
        alignSelf: 'center',
        marginHorizontal: scale.w(10),
    },
    room_number_view: {
        alignItems: 'center',
        justifyContent: 'center',
        width: scale.w(120),
        height: scale.w(50),
        borderWidth: 1,
        borderColor: colors.GREY,
        borderRadius: scale.w(8),
        backgroundColor: colors.WHITE,
        alignSelf: 'center',
        marginHorizontal: scale.w(10),
    },
    btn_primary_container: {
        paddingHorizontal: scale.w(57),
        paddingTop: scale.w(8),
        paddingBottom: scale.w(38),
    },
});

export default CheckInForm;
