import React from 'react';
import { View, StyleSheet } from 'react-native';
import { scale } from '../../../utils/dimensions';
import { H3 } from '../../_global/Text';
import colors from '../../../constants/colors';
import { ButtonPrimary } from '../../_global/Button';

interface IModalMessageScanCCProps {
    handleMessageScanCC: () => void;
    color?: string;
    selectedLanguage?: any;
}

const ModalMessageScanCC = (props: IModalMessageScanCCProps) => {
    return (
        <View style={styles.container}>
            <H3 fontSize={scale.w(16)} textAlign="center">
                {props.selectedLanguage.scan_credit_card_or_input_manually_in_the_bottom_right_corner}
            </H3>
            <View style={{ height: 20 }} />
            <ButtonPrimary
                backgroundColor={props.color}
                onPress={props.handleMessageScanCC}
                fontSize={scale.w(16.5)}
                text="Okay"
            />
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.WHITE,
        padding: scale.w(24),
        marginHorizontal: scale.w(30),
        borderRadius: scale.w(30),
    },
});

export default ModalMessageScanCC;
