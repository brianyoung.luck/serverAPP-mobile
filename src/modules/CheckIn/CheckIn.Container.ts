import { connect } from 'react-redux';
import CheckIn from './CheckIn';
import { bindActionCreators, Dispatch } from 'redux';
import { IState } from '../../types/state';

const mapStateToProps = (state: IState) => ({
    icon: state.hotel.icon.check_in_color,
    selectedLanguage: state.language,
});

const mapDispatchToProps = (dispatch: Dispatch) => {
    return bindActionCreators({}, dispatch);
};

export interface ICheckInReduxProps
    extends ReturnType<typeof mapStateToProps>,
        ReturnType<typeof mapDispatchToProps> {}

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(CheckIn);
