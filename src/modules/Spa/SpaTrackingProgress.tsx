import React from 'react';
import {
    View,
    FlatList,
    TouchableOpacity,
    Platform,
    RefreshControl,
    SafeAreaView,
    StyleSheet,
    ActivityIndicator,
} from 'react-native';
import base from '../../utils/baseStyles';
import { scale, screenWidth } from '../../utils/dimensions';
import { H3 } from '../_global/Text';
import Navbar from '../_global/Navbar';
import { Navigation } from 'react-native-navigation';
import { ISpaTrackingProgressOrderRoomService } from '../../types/spa';
import { ISpaTrackingProgressReduxProps } from './spaTrackingProgres.Container';
import colors from '../../constants/colors';
import { upperFirst, startCase } from 'lodash';
import * as Animatable from 'react-native-animatable';
import { ButtonPrimary } from '../_global/Button';
import { chat } from '../../utils/navigationControl';
import { BoxShadow } from 'react-native-shadow';
import Icon from 'react-native-vector-icons/FontAwesome5';

interface ISpaTrackingProgressProps extends ISpaTrackingProgressReduxProps {
    componentId: string;
}

interface ITrackingProgressState {
    display: 'current' | 'history';
    loadingGet: boolean;
}

class TrackingProgress extends React.Component<ISpaTrackingProgressProps, ITrackingProgressState> {
    constructor(props: ISpaTrackingProgressProps) {
        super(props);

        this.state = {
            display: 'current',
            loadingGet: false,
        };

        Navigation.mergeOptions(props.componentId, {
            statusBar: {
                backgroundColor: props.color,
                style: 'light',
            },
        });
        this._fetch = this._fetch.bind(this);
        this._handleBack = this._handleBack.bind(this);
        this._keyExtractor = this._keyExtractor.bind(this);
        this._renderItem = this._renderItem.bind(this);
        this._handleChat = this._handleChat.bind(this);
        this._renderListHeaderComponent = this._renderListHeaderComponent.bind(this);
        this._renderListFooterComponent = this._renderListFooterComponent.bind(this);
        this._renderListEmptyComponent = this._renderListEmptyComponent.bind(this);
    }

    componentDidMount() {
        // this.props.spaTrackingProgressOrderRoomService();
        this._fetch();
    }

    _fetch() {
        this.setState({ loadingGet: true });
        this.props.spaTrackingProgressOrderRoomService(
            () => {
                this.setState({ loadingGet: false });
            },
            () => {
                this.setState({ loadingGet: false });
            },
        );
    }

    _handleBack() {
        Navigation.pop(this.props.componentId);
    }

    _keyExtractor(item: ISpaTrackingProgressOrderRoomService) {
        return item.booking_date.toString();
    }

    handleDelete(id: number) {
        this.props.deleteSpaOrder(
            id,
            () => {
                this._fetch();
            },
            () => {},
        );
    }

    _renderItem({ item }: { item: ISpaTrackingProgressOrderRoomService }) {
        const { color, currency } = this.props;
        console.log('item', item);
        return (
            <View style={styles.item_container}>
                <View style={[styles.secondary_container, { backgroundColor: '#fff' }]}>
                    <View
                        style={{
                            alignItems: 'flex-end',
                        }}
                    >
                        {item.status == 'rejected' ? (
                            <TouchableOpacity
                                onPress={() => {
                                    this.handleDelete(item.id);
                                }}
                            >
                                <Icon color="red" size={18} name={'times-circle'} />
                            </TouchableOpacity>
                        ) : (
                            <View />
                        )}
                    </View>
                    <H3 textAlign="center" color={'#6D6D6D'} fontSize={scale.w(27)}>
                        {item.booking_type == 'room_service' ? 'Room Service' : 'Normal Reservation'}
                    </H3>
                    <View style={{ marginTop: scale.w(10) }}>
                        {item.treatments.map((spa, index) => {
                            return (
                                <View key={index} style={styles.menu_container}>
                                    <H3 fontSize={scale.w(19)} color={'#4B4B4B'}>{`${index + 1}.`}</H3>
                                    <View style={{ flex: 1, marginHorizontal: scale.w(10) }}>
                                        <H3 fontSize={scale.w(19)} color={'#4B4B4B'}>
                                            {spa.name}
                                        </H3>
                                    </View>
                                    <H3 fontSize={scale.w(19)} color={'#4B4B4B'}>
                                        {`${currency}${Number(spa.price)}`}
                                    </H3>
                                </View>
                            );
                        })}
                    </View>
                    <View style={{ alignItems: 'flex-end' }}>
                        <H3 fontSize={scale.w(22)} color={'#4B4B4B'}>
                            {`Total price: ${currency}${item.total_price}`}
                        </H3>
                    </View>
                </View>

                <View
                    style={{
                        borderBottomLeftRadius: scale.w(20),
                        borderBottomRightRadius: scale.w(20),
                        paddingVertical: scale.w(18),
                        backgroundColor: color || colors.LIGHT_BLUE,
                    }}
                >
                    <H3 fontSize={scale.w(22)} textAlign="center" color="#fff">
                        {`Status: ${upperFirst(startCase(item.status))}`}
                    </H3>
                </View>
            </View>
        );
    }

    _handleChat() {
        Navigation.push(this.props.componentId, chat({ from: 'restaurant' }));
    }

    _renderListHeaderComponent() {
        return <View style={{ height: 40 }} />;
    }

    _renderListFooterComponent() {
        return <View style={{ height: 20 }} />;
    }

    _renderListEmptyComponent() {
        return (
            <View style={{ marginTop: scale.h(220) }}>
                {!this.state.loadingGet ? (
                    <H3 fontSize={scale.w(18)} textAlign="center">
                        {this.state.display == 'current'
                            ? this.props.selectedLanguage.no_current_orders
                            : this.props.selectedLanguage.no_previous_orders}
                    </H3>
                ) : (
                    <View></View>
                )}
            </View>
        );
    }

    render() {
        const { display, loadingGet } = this.state;
        const { current_orders, previous_orders, live_chat } = this.props.selectedLanguage;
        const { color } = this.props;

        console.log('propssss', this.props);

        return (
            <View style={base.container}>
                <Navbar color={color} onClick={this._handleBack} title={'Orders'} />

                <View style={{ flex: 1 }}>
                    <View
                        style={{
                            height: scale.w(54),
                            flexDirection: 'row',
                            width: scale.w(375),
                            paddingLeft: scale.w(25),
                            marginTop: scale.h(20),
                        }}
                    >
                        <TouchableOpacity
                            style={[
                                {
                                    width: scale.w(154),
                                    height: scale.w(50),
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    borderWidth: 1,
                                    borderColor: color || colors.LIGHT_BLUE,
                                    borderRightWidth: 0,
                                    borderTopLeftRadius: scale.w(15),
                                    borderBottomLeftRadius: scale.w(15),
                                },
                                display == 'current'
                                    ? { backgroundColor: color || colors.LIGHT_BLUE }
                                    : { backgroundColor: '#fff' },
                            ]}
                            onPress={() => {
                                this.setState({ display: 'current' });
                            }}
                        >
                            <H3
                                textAlign="center"
                                fontSize={scale.w(18)}
                                color={display == 'current' ? '#fff' : '#8e8e8e'}
                            >
                                {current_orders}
                            </H3>
                        </TouchableOpacity>

                        <TouchableOpacity
                            style={[
                                {
                                    width: scale.w(154),
                                    height: scale.w(50),
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    borderWidth: 1,
                                    borderColor: color || colors.LIGHT_BLUE,
                                    borderTopRightRadius: scale.w(15),
                                    borderBottomRightRadius: scale.w(15),
                                },
                                display == 'history'
                                    ? { backgroundColor: color || colors.LIGHT_BLUE }
                                    : { backgroundColor: '#fff' },
                            ]}
                            onPress={() => {
                                this.setState({ display: 'history' });
                            }}
                        >
                            <H3
                                textAlign="center"
                                fontSize={scale.w(18)}
                                color={display == 'history' ? '#fff' : '#8e8e8e'}
                            >
                                {previous_orders}
                            </H3>
                        </TouchableOpacity>
                    </View>

                    <FlatList
                        refreshControl={<RefreshControl refreshing={loadingGet} onRefresh={this._fetch} />}
                        data={display == 'current' ? this.props.currentOrder : this.props.previousOrder}
                        extraData={this.state}
                        keyExtractor={this._keyExtractor}
                        renderItem={this._renderItem}
                        ListEmptyComponent={this._renderListEmptyComponent}
                        ListHeaderComponent={this._renderListHeaderComponent}
                        ListFooterComponent={this._renderListFooterComponent}
                        ItemSeparatorComponent={this._renderListFooterComponent}
                        initialNumToRender={10}
                    />

                    <Animatable.View
                        useNativeDriver
                        animation="fadeIn"
                        duration={300}
                        style={styles.btn_chat_container}
                    >
                        <ButtonPrimary
                            onPress={this._handleChat}
                            text={live_chat}
                            backgroundColor={color || colors.LIGHT_BLUE}
                        />
                    </Animatable.View>
                    <SafeAreaView />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    item_container: {
        marginHorizontal: scale.w(25),
        width: scale.w(265),
        borderRadius: scale.w(20),
        backgroundColor: '#fff',
        alignSelf: 'center',
        ...Platform.select({
            ios: {
                shadowColor: '#000',
                shadowOffset: { width: 0, height: 4 },
                shadowOpacity: 0.2,
                shadowRadius: 3,
            },
            android: {
                elevation: 8,
            },
        }),
    },
    secondary_container: {
        borderRadius: 50,
        paddingTop: scale.h(10),
        paddingBottom: scale.h(30),
        paddingHorizontal: scale.w(24),
    },
    menu_container: {
        flexDirection: 'row',
        marginBottom: scale.w(18),
    },
    btn_chat_container: {
        paddingHorizontal: scale.w(57),
        paddingVertical: scale.w(12),
    },
});

const shadowOpt = {
    width: screenWidth,
    height: scale.w(50),
    color: '#000',
    border: 6,
    radius: 1,
    opacity: 0.2,
    x: 0,
    y: 6,
    style: { marginBottom: scale.w(9), marginTop: scale.w(25) },
};

export default TrackingProgress;
