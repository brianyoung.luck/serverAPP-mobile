import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import { IState } from '../../types/state';
import SpaService from './SpaService';
import { getSpa } from '../../actions/action.spa';

const mapStateToProps = (state: IState) => ({
    spa: state.spa.spa,
    color: state.hotel.icon.spa_color,
    selectedLanguage: state.language,
});

const mapDispatchToProps = (dispatch: Dispatch) => {
    return bindActionCreators(
        {
            getSpa,
        },
        dispatch,
    );
};

export interface ISpaServiceReduxProps
    extends ReturnType<typeof mapStateToProps>,
        ReturnType<typeof mapDispatchToProps> {}

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(SpaService);
