import React from 'react';
import { View, StyleSheet, Image, ScrollView, Alert, TouchableOpacity } from 'react-native';
import base from '../../utils/baseStyles';
import { scale, heightPercentageToDP } from '../../utils/dimensions';
import Navbar from '../_global/Navbar';
import { Navigation } from 'react-native-navigation';
import { spaBookingTime, mainmenu, spaTrackingProgress } from '../../utils/navigationControl';
import MenuButton from '../_global/MenuButton';
import { ISpaServiceReduxProps } from './SpaService.Container';
import Carousel from 'react-native-snap-carousel';
import { screenWidth } from '../../utils/dimensions';
import { IFeatureHotel } from '../../types/hotel';
import colors from '../../constants/colors';
import { ButtonPrimary } from '../_global/Button';
import * as Animatable from 'react-native-animatable';

export interface ISpaServiceProps extends ISpaServiceReduxProps {
    componentId: string;
    backGround?: boolean;
}

interface ISpaServiceState {}

class SpaService extends React.Component<ISpaServiceProps, ISpaServiceState> {
    constructor(props: ISpaServiceProps) {
        super(props);

        this.state = {};

        Navigation.mergeOptions(props.componentId, {
            statusBar: {
                backgroundColor: props.color,
                style: 'light',
            },
        });
        this.props.getSpa();
        this._handleBack = this._handleBack.bind(this);
        this._handleReserveSpa = this._handleReserveSpa.bind(this);
        this._handleOrderRoomService = this._handleOrderRoomService.bind(this);
        this._handleMyOrders = this._handleMyOrders.bind(this);
        // this._isLockFeature = this._isLockFeature.bind(this);
    }

    // componentDidMount() {
    //     this.props.getSpa();
    // }

    _handleBack() {
        if (this.props.backGround) {
            Navigation.push(this.props.componentId, mainmenu);
        } else {
            Navigation.pop(this.props.componentId);
        }
    }

    _handleReserveSpa() {
        // if (this._isLockFeature('is_spa_treatment')) {
        //     return false;
        // }
        Navigation.push(
            this.props.componentId,
            spaBookingTime({
                isReserveSpaType: true,
                spa: this.props.spa,
            }),
        );
    }

    _handleOrderRoomService() {
        // if (this._isLockFeature('is_spa_room_service')) {
        //     return false;
        // }
        // console.log("printing spa props = ",this.props)

        Navigation.push(
            this.props.componentId,
            spaBookingTime({
                isReserveSpaType: false,
                spa: this.props.spa,
            }),
        );
    }
    // _isLockFeature(feature?: keyof IFeatureHotel) {
    //     if (feature !== 'spa_room_service' && !this.props.isCheckedIn) {
    //         Alert.alert('Attention', 'Please check in first, to use this service');

    //         return true;
    //     }
    // }

    _handleMyOrders() {
        Navigation.push(this.props.componentId, spaTrackingProgress);
    }

    render() {
        const { spa, color } = this.props;
        // console.log("Printing props of spa == ",this.props.spa)
        const { reserve_a_spa_treatment, order_spa_room_service, my_orders } = this.props.selectedLanguage;
        return (
            <View style={base.container}>
                <Navbar isViolet={color === ''} color={color} onClick={this._handleBack} title={spa.name} />
                <View style={{ flex: 1, justifyContent: 'center' }}>
                    {spa.images ? (
                        <View style={{ justifyContent: 'center', marginTop: scale.h(15) }}>
                            <Carousel
                                ref={'carousel'}
                                data={spa.images}
                                renderItem={({ item }) => (
                                    <View
                                        style={{
                                            width: scale.w(330),
                                            height: scale.h(220),
                                            alignSelf: 'center',
                                        }}
                                    >
                                        <Image
                                            source={{ uri: item.image_url }}
                                            style={{
                                                width: '100%',
                                                height: '100%',
                                                borderRadius: scale.w(10),
                                            }}
                                        />
                                    </View>
                                )}
                                sliderWidth={screenWidth}
                                itemWidth={screenWidth}
                                autoplay
                                loop
                            />
                            <TouchableOpacity
                                onPress={() => {
                                    this.refs.carousel.snapToPrev();
                                }}
                                style={{
                                    justifyContent: 'center',
                                    position: 'absolute',
                                    marginTop: heightPercentageToDP(17),
                                    opacity: 0.4,
                                }}
                            >
                                <Image
                                    source={require('../../images/icon_previous.png')}
                                    style={{ width: scale.w(20), height: scale.w(20) }}
                                    resizeMode="contain"
                                />
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => {
                                    this.refs.carousel.snapToNext();
                                }}
                                style={{
                                    justifyContent: 'center',
                                    position: 'absolute',
                                    marginTop: heightPercentageToDP(17),
                                    alignSelf: 'flex-end',
                                    opacity: 0.4,
                                }}
                            >
                                <Image
                                    source={require('../../images/icon_next.png')}
                                    style={{ width: scale.w(20), height: scale.w(20) }}
                                    resizeMode="contain"
                                />
                            </TouchableOpacity>
                        </View>
                    ) : (
                        <View>
                            <Carousel
                                data={spa.logo_url}
                                renderItem={({ item }) => (
                                    <Image source={{ uri: item }} style={styles.logo} />
                                )}
                                sliderWidth={screenWidth}
                                itemWidth={screenWidth}
                                autoplay
                                loop
                            />
                            {/* <TouchableOpacity onPress={()=>{this.refs.carousel.snapToPrev();}} style={{ justifyContent: 'center',position:"absolute",marginTop:heightPercentageToDP(17),opacity:0.4}} >
                        <Image
                                source={require('../../images/icon_previous.png')}
                                style={{ width: scale.w(20), height: scale.w(20) }}
                                resizeMode="contain"
                            />  
                        </TouchableOpacity>
                        <TouchableOpacity onPress={()=>{this.refs.carousel.snapToNext();}} style={{ justifyContent: 'center',position:"absolute",marginTop:heightPercentageToDP(17),alignSelf:'flex-end',opacity:0.4}} >
                        <Image
                                source={require('../../images/icon_next.png')}
                                style={{ width: scale.w(20), height: scale.w(20) }}
                                resizeMode="contain"
                            /> 
                        </TouchableOpacity> */}
                        </View>
                    )}

                    <View
                        style={{
                            flexDirection: 'row',
                            width: scale.w(375),
                            paddingLeft: scale.w(8),
                            marginTop: scale.w(20),
                        }}
                    >
                        {this.props.spa.spa_room_service == true ? (
                            <View style={styles.menu_btn_container}>
                                <MenuButton
                                    onPress={this._handleOrderRoomService}
                                    source={require('../../images/icon_order_spa_room_service.png')}
                                    text={order_spa_room_service}
                                    width={scale.w(155)}
                                    height={scale.h(300)}
                                    iconSize={scale.w(85)}
                                    fontSize={scale.w(20)}
                                    styleImage={{ tintColor: color }}
                                />
                            </View>
                        ) : null}
                        {this.props.spa.spa_treatment == true ? (
                            <View style={styles.menu_btn_container}>
                                <MenuButton
                                    onPress={this._handleReserveSpa}
                                    source={require('../../images/icon_reserve_spa_treatment.png')}
                                    text={reserve_a_spa_treatment}
                                    width={scale.w(155)}
                                    height={scale.h(300)}
                                    iconSize={scale.w(70)}
                                    fontSize={scale.w(20)}
                                    styleImage={{ tintColor: color }}
                                />
                            </View>
                        ) : null}
                    </View>
                </View>
                <Animatable.View
                    useNativeDriver
                    animation="fadeIn"
                    duration={300}
                    style={{ paddingHorizontal: scale.w(57), paddingBottom: scale.w(20) }}
                >
                    <ButtonPrimary
                        onPress={this._handleMyOrders}
                        text={my_orders}
                        backgroundColor={color || colors.BROWN}
                    />
                </Animatable.View>

                {/* <ScrollView>
                    <View style={{ height: scale.w(20) }} />
                    {this.props.spa.spa_treatment == true ? (
                        <View style={styles.menu_btn_container}>
                            <MenuButton
                                onPress={this._handleReserveSpa}
                                source={require('../../images/icon_reserve_spa_treatment.png')}
                                text={reserve_a_spa_treatment}
                                width={scale.w(265)}
                                height={scale.w(150)}
                                iconSize={scale.w(70)}
                                fontSize={scale.w(20)}
                                styleImage={{ tintColor: color }}
                            />
                        </View>
                    ) : null}

                    {this.props.spa.spa_room_service == true ? (
                        <View style={styles.menu_btn_container}>
                            <MenuButton
                                onPress={this._handleOrderRoomService}
                                source={require('../../images/icon_order_spa_room_service.png')}
                                text={order_spa_room_service}
                                width={scale.w(265)}
                                height={scale.w(150)}
                                iconSize={scale.w(85)}
                                fontSize={scale.w(20)}
                                styleImage={{ tintColor: color }}
                            />
                        </View>
                    ) : null}
                </ScrollView> */}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    text_container: {
        marginTop: scale.w(10),
        marginBottom: scale.w(20),
        marginHorizontal: scale.w(28),
    },
    image_container: {
        alignItems: 'center',
        marginTop: scale.w(60),
        marginBottom: scale.w(68),
    },
    menu_btn_container: {
        marginBottom: scale.w(10),
        alignItems: 'center',
    },
    logo: {
        width: '100%',
        height: scale.w(200),
        resizeMode: 'contain',
        // marginTop: scale.w(20),
    },
});

export default SpaService;
