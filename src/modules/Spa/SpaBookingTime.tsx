import React from 'react';
import { View, StyleSheet, TouchableOpacity, Keyboard, ScrollView, Alert } from 'react-native';
import base from '../../utils/baseStyles';
import { scale } from '../../utils/dimensions';
import Navbar from '../_global/Navbar';
import { Navigation } from 'react-native-navigation';
import FieldForm from '../Restaurant/Components/FieldForm';
import { debounce } from 'lodash';
import * as Animatable from 'react-native-animatable';
import { ButtonPrimary, ButtonSecondary } from '../_global/Button';
import colors from '../../constants/colors';
import { Calendar as CalendarWidget, DateObject } from 'react-native-calendars';
import { format } from 'date-fns';
import CustomModal from '../_global/CustomModal';
import ModalTimePicker from '../Restaurant/Components/ModalTimePicker';
import { ISpaBookingTimeReduxProps } from './SpaBookingTime.Container';
import { H4 } from '../_global/Text';
import { spaTreatmentList, spaTrackingProgress } from '../../utils/navigationControl';
import { ISpa, ISpaTreatment } from '../../types/spa';
import ModalDatePicker from './ModalDatePicker';

export interface ISpaBookingTimeProps extends ISpaBookingTimeReduxProps {
    componentId: string;
    isReserveSpaType?: boolean;
    spa: ISpa;
}

interface ISpaBookingTimeState {
    selectedTreatments: ISpaTreatment[];
    time: string;
    date: string;
    numberPeople: string;
    loading: boolean;
}

class SpaBookingTime extends React.Component<ISpaBookingTimeProps, ISpaBookingTimeState> {
    private _modalTimePicker = React.createRef<CustomModal>();
    private _modalDatePicker = React.createRef<CustomModal>();

    constructor(props: ISpaBookingTimeProps) {
        super(props);

        this.state = {
            selectedTreatments: [],
            time: '',
            date: '',
            numberPeople: props.numberPeople.toString(),
            loading: false,
        };

        Navigation.mergeOptions(props.componentId, {
            statusBar: {
                backgroundColor: props.color,
                style: 'light',
            },
        });
        this._handleBack = this._handleBack.bind(this);
        this._handleSpaBookingTime = this._handleSpaBookingTime.bind(this);
        this._handleModalDatePicker = this._handleModalDatePicker.bind(this);
        this._onDayPress = this._onDayPress.bind(this);
        this._getMarkedDates = this._getMarkedDates.bind(this);
        this._handleChooseTreatment = this._handleChooseTreatment.bind(this);
        this._handleModalDatePicker2 = this._handleModalDatePicker2.bind(this);
        this._onChangeDate = this._onChangeDate.bind(this);
    }

    _handleBack() {
        Navigation.pop(this.props.componentId);
    }

    _handleSpaBookingTime() {
        const { numberPeople, date, time, selectedTreatments } = this.state;
        const { componentId, isReserveSpaType, spa } = this.props;

        this.setState({ loading: true });
        const body = {
            spa_id: spa.id,
            number_people: Number(numberPeople) !== NaN ? Number(numberPeople) : 0,
            date,
            time,
            treatments: selectedTreatments.map((treatment) => treatment.id),
        };

        // is reserved spa
        if (isReserveSpaType) {
            return this.props.reserveSpa(
                body,
                async () => {
                    this.setState({ loading: false });
                    await Navigation.popToRoot(componentId);
                    Alert.alert(
                        this.props.selectedLanguage.success,
                        this.props.selectedLanguage.success_your_reservation_request_has_been_sent,
                    );
                },
                () => {
                    this.setState({ loading: false });
                },
            );
        }

        // is order room spa
        this.props.orderRoomSpa(
            body,
            async () => {
                this.setState({ loading: false });
                Navigation.popTo('spaService');
                await Navigation.push('spaService', spaTrackingProgress);
                Alert.alert(
                    this.props.selectedLanguage.success,
                    this.props.selectedLanguage.success_your_reservation_request_has_been_sent,
                );
            },
            () => {
                this.setState({ loading: false });
            },
        );
    }

    _handleModalDatePicker(closeModal?: boolean) {
        return () => {
            Keyboard.dismiss();
            if (this._modalTimePicker.current) {
                if (closeModal) {
                    this._modalTimePicker.current.hide();
                } else {
                    this._modalTimePicker.current.show();
                    this.setState({ time: new Date().toString() });
                }
            }
        };
    }

    _onDayPress(date: DateObject) {
        this.setState({ date: date.dateString });
    }

    _getMarkedDates() {
        if (this.state.date !== '') {
            return { [this.state.date]: { selected: true } };
        }

        return {};
    }

    _handleChooseTreatment() {
        Navigation.push(
            this.props.componentId,
            spaTreatmentList({
                treatmentSelectedList: this.state.selectedTreatments,
                spa: this.props.spa,
                onSubmitSelectedTreatments: (selectedTreatments) => this.setState({ selectedTreatments }),
            }),
        );
    }

    _handleModalDatePicker2(closeModal?: boolean) {
        return () => {
            Keyboard.dismiss();
            if (this._modalDatePicker.current) {
                if (closeModal) {
                    this._modalDatePicker.current.hide();
                } else {
                    this._modalDatePicker.current.show();
                    this.setState({ date: new Date().toString() });
                }
            }
        };
    }

    _onChangeDate(date: Date) {
        this.setState((prevState) => {
            if (prevState.date) {
                return {
                    ...prevState,
                    date: date.toString(),
                };
            }

            return {
                ...prevState,
                date: date.toString(),
            };
        });
    }

    render() {
        console.log('propssssss', this.props);
        const { spa, isReserveSpaType, color } = this.props;
        const { numberPeople, time, selectedTreatments, loading } = this.state;
        const {
            reserve_a_spa_treatment,
            order_spa_room_service,
            choose_treatment,
            number_of_people,
            time_of_booking,
            confirm_booking,
        } = this.props.selectedLanguage;
        return (
            <View style={base.container}>
                <Navbar isViolet={color === ''} color={color} onClick={this._handleBack} title={spa.name} />

                <ScrollView>
                    <View style={{ height: scale.w(5) }} />
                    <H4 textAlign="center" fontSize={scale.w(16)}>
                        {isReserveSpaType ? reserve_a_spa_treatment : order_spa_room_service}
                    </H4>
                    <View style={{ height: scale.w(20) }} />
                    <View style={{ marginHorizontal: scale.w(55) }}>
                        <ButtonSecondary
                            onPress={this._handleChooseTreatment}
                            text={
                                selectedTreatments.length > 0
                                    ? `${selectedTreatments.length} ${this.props.selectedLanguage.treatments_selected}`
                                    : choose_treatment
                            }
                            fontSize={scale.w(16)}
                            height={scale.w(48)}
                        />
                    </View>
                    <View style={{ height: scale.w(60) }} />
                    <FieldForm
                        label={number_of_people}
                        placeholder="No."
                        value={numberPeople}
                        onChangeText={(numberPeople) => this.setState({ numberPeople: numberPeople })}
                        autoCapitalize="none"
                        keyboardType="number-pad"
                    />
                    <View style={{ height: scale.w(20) }} />
                    <TouchableOpacity
                        onPress={debounce(this._handleModalDatePicker(false), 1000, {
                            leading: true,
                            trailing: false,
                        })}
                        activeOpacity={0.7}
                    >
                        <FieldForm
                            label={time_of_booking + ':'}
                            value={time !== '' ? format(time, 'hh:mma') : this.props.selectedLanguage.time}
                            isEmpty={time === ''}
                            textOnly
                        />
                    </TouchableOpacity>
                    <View style={{ height: scale.w(20) }} />
                    <TouchableOpacity
                        onPress={debounce(this._handleModalDatePicker2(false), 1000, {
                            leading: true,
                            trailing: false,
                        })}
                        activeOpacity={0.7}
                    >
                        <FieldForm
                            label={'Date'}
                            value={
                                this.state.date === '' ? 'DD/MM/YY' : format(this.state.date, 'DD/MM/YYYY')
                            }
                            isEmpty={this.state.date === ''}
                            textOnly
                        />
                    </TouchableOpacity>
                    {/* <View style={styles.calendar_container}>
                        <CalendarWidget
                            markedDates={this._getMarkedDates()}
                            minDate={format(new Date(), 'YYYY-MM-DD')}
                            theme={{
                                todayTextColor: colors.BROWN,
                                selectedDayTextColor: '#fff',
                                selectedDayBackgroundColor: color || colors.VIOLET,
                                arrowColor: colors.BLACK,
                                textDayFontSize: 16,
                                textMonthFontSize: 16,
                                textDayHeaderFontSize: 16,
                                textDayFontFamily: 'Roboto-Regular',
                                textDayHeaderFontFamily: 'Roboto-Regular',
                                textMonthFontFamily: 'Roboto-Bold',
                                calendarBackground: '#F2F2F2',
                                backgroundColor: '#E6E6E6',
                                dayTextColor: colors.BLACK,
                                monthTextColor: colors.BLACK,
                                textDisabledColor: '#AAA',
                                textSectionTitleColor: colors.BLACK,
                                'stylesheet.calendar.header': {
                                    week: {
                                        paddingVertical: scale.w(12),
                                        flexDirection: 'row',
                                        justifyContent: 'space-between',
                                        backgroundColor: '#EBEBEB',
                                    },
                                },
                            }}
                            onDayPress={this._onDayPress}
                        />
                    </View> */}

                    <Animatable.View
                        useNativeDriver
                        animation="fadeIn"
                        duration={300}
                        style={styles.submit_btn_container}
                    >
                        <ButtonPrimary
                            onPress={this._handleSpaBookingTime}
                            text={confirm_booking}
                            backgroundColor={color || colors.VIOLET}
                            loading={loading}
                            disabled={loading}
                        />
                    </Animatable.View>
                </ScrollView>

                <CustomModal ref={this._modalTimePicker} animationIn="fadeInUp" animationOut="fadeOutDown">
                    <ModalTimePicker
                        time={time !== '' ? new Date(time) : new Date()}
                        onTimeChange={(time) => this.setState({ time: time.toString() })}
                        minimumDate={new Date()}
                        showModal={this._handleModalDatePicker(true)}
                        title={this.props.selectedLanguage.pick_your_time_booking}
                        isViolet={color === ''}
                        color={color}
                        selectedLanguage={this.props.selectedLanguage}
                    />
                </CustomModal>
                <CustomModal ref={this._modalDatePicker} animationIn="fadeInUp" animationOut="fadeOutDown">
                    <ModalDatePicker
                        date={new Date(this.state.date)}
                        minimumDate={new Date()}
                        color={color}
                        onDateChange={this._onChangeDate}
                        showModal={this._handleModalDatePicker2(true)}
                        title={`Pick your booking date `}
                    />
                </CustomModal>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    calendar_container: {
        paddingHorizontal: scale.w(13),
        marginTop: scale.w(20),
    },
    submit_btn_container: {
        paddingHorizontal: scale.w(57),
        marginTop: scale.h(100),
        flex: 1,
        paddingBottom: scale.h(40),
    },
});

export default SpaBookingTime;
