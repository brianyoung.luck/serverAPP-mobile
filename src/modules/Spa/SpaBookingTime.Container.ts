import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import { IState } from '../../types/state';
import SpaBookingTime from './SpaBookingTime';
import { reserveSpa, orderRoomSpa } from '../../actions/action.spa';

const mapStateToProps = (state: IState) => ({
    numberPeople: state.account.profile.passport_photos ? state.account.profile.passport_photos.length : 1,
    color: state.hotel.icon.spa_color,
    selectedLanguage: state.language,
});

const mapDispatchToProps = (dispatch: Dispatch) => {
    return bindActionCreators(
        {
            reserveSpa,
            orderRoomSpa,
        },
        dispatch,
    );
};

export interface ISpaBookingTimeReduxProps
    extends ReturnType<typeof mapStateToProps>,
        ReturnType<typeof mapDispatchToProps> {}

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(SpaBookingTime);
