import React from 'react';
import {
    View,
    StyleSheet,
    TouchableOpacity,
    RefreshControl,
    SafeAreaView,
    FlatList,
    Image,
    TextInput,
    Text,
    Platform,
    ActivityIndicator,
} from 'react-native';
import base from '../../utils/baseStyles';
import { scale, widthPercentageToDP as wp, heightPercentageToDP as hp } from '../../utils/dimensions';
import Navbar from '../_global/Navbar';
import { Navigation } from 'react-native-navigation';
import * as Animatable from 'react-native-animatable';
import { ButtonPrimary } from '../_global/Button';
import colors from '../../constants/colors';
import { H4, H3 } from '../_global/Text';
import { ISpaTreatmentListReduxProps } from './SpaTreatmentList.Container';
import CheckSquare from '../_global/CheckSquare';
import { ISpa, ISpaTreatment } from '../../types/spa';
import ImageZoom from 'react-native-image-pan-zoom';
import Modal from 'react-native-modal';
import numeral from 'numeral';

export interface ISpaTreatmentListProps extends ISpaTreatmentListReduxProps {
    componentId: string;
    treatmentSelectedList: ISpaTreatment[];
    spa: ISpa;
    onSubmitSelectedTreatments: (treatments: ISpaTreatment[]) => void;
}

interface ISpaTreatmentListState {
    selectedItem: ISpaTreatment[];
    loadingGet: boolean;
    selectedVal: any;
    search: boolean;
    dataToShow: {};
    modalVisible: boolean;
    modalVisible1: boolean;
    imageUrl: String;
    description: any;
    title: String;
    item: any;
}

class SpaTreatmentList extends React.Component<ISpaTreatmentListProps, ISpaTreatmentListState> {
    constructor(props: ISpaTreatmentListProps) {
        super(props);

        this.state = {
            selectedItem: props.treatmentSelectedList,
            loadingGet: false,
            selectedVal: '',
            search: false,
            dataToShow: this.props.treatments,
            modalVisible: false,
            modalVisible1: false,
            imageUrl: '',
            description: '',
            title: '',
            item: [],
        };

        Navigation.mergeOptions(props.componentId, {
            statusBar: {
                backgroundColor: props.color,
                style: 'light',
            },
        });
        this._fetch = this._fetch.bind(this);
        this._handleBack = this._handleBack.bind(this);
        this._handleSpaTreatmentList = this._handleSpaTreatmentList.bind(this);
        this._handleSelectItem = this._handleSelectItem.bind(this);
        this._keyExtractor = this._keyExtractor.bind(this);
        this._renderListHeaderComponent = this._renderListHeaderComponent.bind(this);
        this._renderItemSeparatorComponent = this._renderItemSeparatorComponent.bind(this);
        this._renderItem = this._renderItem.bind(this);
    }

    componentDidMount() {
        // this.props.getSpaTreatment(this.props.spa.id);
        this._fetch();
    }

    _fetch() {
        this.setState({ loadingGet: true });
        this.props.getSpaTreatment(
            this.props.spa.id,
            () => {
                this.setState({ loadingGet: false });
            },
            () => {
                this.setState({ loadingGet: false });
            },
        );
    }

    _handleBack() {
        Navigation.pop(this.props.componentId);
    }

    _handleSpaTreatmentList() {
        const { selectedItem } = this.state;
        this.props.onSubmitSelectedTreatments(selectedItem);
        Navigation.pop(this.props.componentId);
    }

    _handleSelectItem(item: ISpaTreatment) {
        const { selectedItem } = this.state;
        this.setState({
            selectedItem: selectedItem.map((treatment) => treatment.id).includes(item.id)
                ? selectedItem.filter((treatment) => treatment.id !== item.id)
                : [...selectedItem, item],
        });
    }

    _keyExtractor(item: ISpaTreatment) {
        return item.id.toString();
    }

    _renderListHeaderComponent() {
        return <View style={{ height: scale.w(30) }} />;
    }

    _renderItemSeparatorComponent() {
        return <View style={{ height: scale.w(0) }} />;
    }
    setModalVisible(visible) {
        this.setState({ modalVisible: visible });
    }
    setModalVisible1(visible) {
        this.setState({ modalVisible1: visible });
    }
    _renderItem({ item }: { item: ISpaTreatment }) {
        const { selectedItem } = this.state;
        const { currency } = this.props;

        return (
            <View
                style={{
                    width: scale.w(336),
                    height: scale.w(126),
                    marginHorizontal: scale.w(25),
                    borderRadius: scale.w(20),
                    backgroundColor: '#fff',
                    alignSelf: 'center',
                    paddingLeft: scale.w(10),
                    paddingRight: scale.w(10),
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginBottom: scale.h(25),
                    ...Platform.select({
                        ios: {
                            shadowColor: '#000',
                            shadowOffset: { width: 0, height: 4 },
                            shadowOpacity: 0.2,
                            shadowRadius: 3,
                        },
                        android: {
                            elevation: 8,
                        },
                    }),
                }}
            >
                <TouchableOpacity
                    onPress={() => {
                        this.setState({
                            imageUrl: `${item.image}`,
                            description: item.description,
                            title: item.name,
                            item: item,
                        });
                        this.setModalVisible(true);
                    }}
                    style={{
                        flexDirection: 'row',
                        width: scale.w(270),
                    }}
                >
                    <View
                        style={{
                            width: scale.w(76),
                            height: scale.w(70),
                            borderRadius: scale.w(25),
                        }}
                    >
                        <Image
                            resizeMode={'cover'}
                            source={{ uri: `${item.image}` }}
                            style={{
                                width: '100%',
                                height: '100%',
                                borderRadius: scale.w(20),
                            }}
                        />
                    </View>
                    <View
                        style={{
                            paddingLeft: scale.w(8),
                            width: scale.w(140),
                        }}
                    >
                        <H3 fontSize={scale.w(20)}>{`${item.name}`}</H3>
                        {`${item.description}` != 'null' &&
                            `${item.description}` != undefined &&
                            `${item.description}` != null && (
                                <View>
                                    <H4 fontSize={scale.w(12)}>
                                        {item.description
                                            ? item.description.length > 40
                                                ? item.description.substring(0, 40) + '...'
                                                : item.description
                                            : null}
                                    </H4>
                                </View>
                            )}
                        <H4 fontSize={scale.w(20)}>{`${currency}${numeral(item.price).format('0,0')}`}</H4>
                    </View>
                    <View></View>
                </TouchableOpacity>
                <View
                    style={{
                        width: scale.w(50),
                        alignItems: 'center',
                    }}
                >
                    <TouchableOpacity
                        onPress={() => this._handleSelectItem(item)}
                        activeOpacity={0.7}
                        style={{ flexDirection: 'row', alignItems: 'center' }}
                    >
                        <CheckSquare
                            isChecked={selectedItem.map((treatment) => treatment.id).includes(item.id)}
                            borderRadius={scale.w(10)}
                        />
                    </TouchableOpacity>
                </View>
            </View>

            // <View>
            //     <View style={styles.item_container}>
            //         <TouchableOpacity
            //             style={{ flex: 0.3 }}
            //             onPress={() => {
            //                 this.setState({
            //                     imageUrl: `${item.image}`,
            //                     description: item.description,
            //                     title: item.name,
            //                     item: item,
            //                 });
            //                 this.setModalVisible(true);
            //             }}
            //         >
            //             {selectedItem.includes(item) ? (
            //                 <Image
            //                     resizeMode="contain"
            //                     resizeMethod={'auto'}
            //                     source={{ uri: `${item.image}` }}
            //                     style={styles.image}
            //                 ></Image>
            //             ) : (
            //                 <Image
            //                     resizeMode="contain"
            //                     resizeMethod={'auto'}
            //                     source={{ uri: `${item.image}` }}
            //                     style={styles.image}
            //                 ></Image>
            //             )}
            //         </TouchableOpacity>
            //         <TouchableOpacity
            //             onPress={() => {
            //                 this.setState({
            //                     imageUrl: `${item.image}`,
            //                     description: item.description,
            //                     title: item.name,
            //                     item: item,
            //                 });
            //                 this.setModalVisible(true);
            //                 //  this._handleSelectItem(item)
            //             }}
            //             activeOpacity={0.7}
            //             style={{ flexDirection: 'column', flex: 1, justifyContent: 'center' }}
            //         >
            //             <View style={{}}>
            //                 {selectedItem.includes(item) ? (
            //                     <H3 fontSize={scale.w(20)}>{item.name}</H3>
            //                 ) : (
            //                     <H4 fontSize={scale.w(20)}>{item.name}</H4>
            //                 )}
            //             </View>
            //             {item.description && (
            //                 <View style={{ justifyContent: 'center' }}>
            //                     {selectedItem.includes(item) ? (
            //                         <H3 fontSize={scale.w(13)}>
            //                             {item.description
            //                                 ? item.description.length > 30
            //                                     ? item.description.substring(0, 35) + '...'
            //                                     : item.description
            //                                 : null}
            //                         </H3>
            //                     ) : (
            //                         <H4 fontSize={scale.w(13)}>
            //                             {item.description
            //                                 ? item.description.length > 40
            //                                     ? item.description.substring(0, 40) + '...'
            //                                     : item.description
            //                                 : null}
            //                         </H4>
            //                     )}
            //                 </View>
            //             )}
            //         </TouchableOpacity>
            //         <TouchableOpacity
            //             onPress={() => this._handleSelectItem(item)}
            //             activeOpacity={0.7}
            //             style={{ flex: 0.3, justifyContent: 'center' }}
            //         >
            //             {selectedItem.includes(item) ? (
            //                 <H3 fontSize={scale.w(18)}>
            //                     {currency}
            //                     {item.price}
            //                 </H3>
            //             ) : (
            //                 <H4 fontSize={scale.w(18)}>
            //                     {currency}
            //                     {item.price}
            //                 </H4>
            //             )}
            //         </TouchableOpacity>

            //         <TouchableOpacity
            //             onPress={() => this._handleSelectItem(item)}
            //             activeOpacity={0.7}
            //             style={{ flexDirection: 'row', alignItems: 'center' }}
            //         >
            //             <CheckSquare
            //                 isChecked={selectedItem.map((treatment) => treatment.id).includes(item.id)}
            //                 borderRadius={scale.w(10)}
            //             />
            //         </TouchableOpacity>
            //     </View>
            // </View>
        );
    }
    Search(val: any) {
        if (val === '') {
            this.setState({ search: false, dataToShow: this.props.treatments });
        } else {
            let arr = Object.values(this.props.treatments).filter((x: any) => {
                return x.name.toLowerCase().includes(val.toLowerCase());
            });
            this.setState({ dataToShow: arr });
        }
    }
    render() {
        const { spa, treatments, color } = this.props;
        const { loadingGet } = this.state;
        const { choose_treatment } = this.props.selectedLanguage;

        return (
            <View style={base.container}>
                <Modal
                    onBackdropPress={() => {
                        this.setModalVisible(false);
                    }}
                    onBackButtonPress={() => {
                        this.setModalVisible(false);
                    }}
                    animationType="slide"
                    animationInTiming={500}
                    backdropOpacity={0.7}
                    onSwipeComplete={() => this.setState({ modalVisible: false })}
                    isVisible={this.state.modalVisible}
                >
                    <View
                        style={{
                            flex: 1,
                            justifyContent: 'center',
                            paddingHorizontal: wp(2),
                            alignItems: 'center',
                        }}
                    >
                        <View
                            style={{
                                width: '90%',
                                backgroundColor: 'white',
                                borderRadius: 50,
                                alignItems: 'center',
                                justifyContent: 'center',
                                paddingHorizontal: wp(3),
                            }}
                        >
                            <View style={{ height: hp(4) }} />

                            <H3 fontSize={scale.w(20)}>{this.state.title}</H3>

                            {this.state.imageUrl && <View style={{ height: hp(1) }} />}
                            {this.state.imageUrl != 'null' && (
                                <TouchableOpacity
                                    onPress={() => {
                                        setTimeout(() => {
                                            this.setModalVisible1(true);
                                        }, 400);
                                        this.setModalVisible(false);
                                    }}
                                    style={{
                                        height: hp(20),
                                        width: '100%',
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                    }}
                                >
                                    <Image
                                        resizeMode="contain"
                                        source={{ uri: this.state.imageUrl }}
                                        style={{ height: '80%', width: '80%' }}
                                    />
                                </TouchableOpacity>
                            )}
                            {this.state.imageUrl && <View style={{ height: hp(1) }} />}
                            {this.state.description && (
                                <H4 fontSize={scale.w(12)}>{this.state.description}</H4>
                            )}

                            {this.state.description && <View style={{ height: hp(6) }} />}
                            <TouchableOpacity
                                onPress={() => {
                                    this._handleSelectItem(this.state.item);
                                    this.setModalVisible(false);
                                }}
                                style={{
                                    borderRadius: 100,
                                    height: hp(6),
                                    width: wp(40),
                                    backgroundColor: color,
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                }}
                            >
                                <Text
                                    style={{
                                        color: colors.WHITE,
                                        fontFamily: 'Roboto-Bold',
                                        fontSize: scale.w(14),
                                    }}
                                >
                                    {this.props.selectedLanguage.add_to_order}
                                </Text>
                            </TouchableOpacity>
                            <View style={{ height: hp(4) }} />
                        </View>
                    </View>
                    <View
                        style={{
                            position: 'absolute',
                            height: '100%',
                            alignSelf: 'flex-start',
                            paddingHorizontal: wp(1.2),
                        }}
                    >
                        <TouchableOpacity
                            onPress={() => {
                                this.setModalVisible(false);
                            }}
                        >
                            <Image
                                source={require('../../images/icon_back.png')}
                                style={{ width: scale.w(30), height: scale.w(30) }}
                                resizeMode={'contain'}
                            ></Image>
                        </TouchableOpacity>
                    </View>
                </Modal>
                {/* //////////////////////////// */}

                <Modal
                    onBackdropPress={() => {
                        setTimeout(() => {
                            this.setModalVisible(true);
                        }, 400);
                        this.setModalVisible1(false);
                    }}
                    onBackButtonPress={() => {
                        setTimeout(() => {
                            this.setModalVisible(true);
                        }, 400);
                        this.setModalVisible1(false);
                    }}
                    isVisible={this.state.modalVisible1}
                    animationType="slide"
                    animationInTiming={500}
                    backdropOpacity={0.9}
                    style={styles.modal}
                >
                    <View style={{ flex: 1 }}>
                        <View style={styles.modalContainer}>
                            <View
                                style={{
                                    flexDirection: 'row',
                                    justifyContent: 'center',
                                    height: '100%',
                                }}
                            >
                                <View
                                    style={{
                                        justifyContent: 'center',
                                        alignSelf: 'center',
                                    }}
                                >
                                    <ImageZoom
                                        cropWidth={wp(100)}
                                        cropHeight={hp(100)}
                                        imageWidth={wp(100)}
                                        imageHeight={hp(100)}
                                    >
                                        <Image
                                            resizeMode="contain"
                                            source={{ uri: this.state.imageUrl }}
                                            style={styles.image1}
                                        ></Image>
                                    </ImageZoom>
                                </View>
                            </View>
                        </View>
                        <View
                            style={{
                                position: 'absolute',
                                height: '100%',
                                alignSelf: 'flex-start',
                                paddingHorizontal: wp(1.2),
                            }}
                        >
                            <TouchableOpacity
                                onPress={() => {
                                    setTimeout(() => {
                                        this.setModalVisible(true);
                                    }, 400);
                                    this.setModalVisible1(false);
                                }}
                            >
                                <Image
                                    source={require('../../images/icon_back.png')}
                                    style={{ width: scale.w(30), height: scale.w(30) }}
                                    resizeMode={'contain'}
                                ></Image>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>
                <Navbar isViolet={color === ''} color={color} onClick={this._handleBack} title={spa.name} />

                <View style={{ flex: 1 }}>
                    <View style={styles.searchview}>
                        <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: 13 }}>
                            <Image
                                source={require('../../images/icon-search.png')}
                                style={{ height: 15, width: 15, tintColor: color }}
                            />

                            <TextInput
                                value={this.state.selectedVal}
                                onChangeText={(val) => {
                                    this.setState({ selectedVal: val, search: true });
                                    this.Search(val);
                                }}
                                placeholder={this.props.selectedLanguage.search}
                                style={{
                                    fontSize: 20,
                                    marginLeft: 10,
                                    width: wp(75),
                                    fontFamily: 'Roboto-Regular',
                                }}
                            ></TextInput>
                        </View>
                    </View>
                    {this.state.dataToShow.length == 0 && this.state.search == false ? (
                        <FlatList
                            refreshControl={
                                <RefreshControl onRefresh={this._fetch} refreshing={loadingGet} />
                            }
                            ListEmptyComponent={() => {
                                return (
                                    <View>
                                        {this.state.loadingGet ? (
                                            <ActivityIndicator size="large" color={'#fff'} />
                                        ) : (
                                            <View style={{ marginTop: scale.h(250) }}>
                                                <Text style={{ alignSelf: 'center' }}>
                                                    No spa treatment found
                                                </Text>
                                            </View>
                                        )}
                                    </View>
                                );
                            }}
                            data={treatments}
                            extraData={this.state}
                            keyExtractor={this._keyExtractor}
                            ListHeaderComponent={this._renderListHeaderComponent}
                            ItemSeparatorComponent={this._renderItemSeparatorComponent}
                            renderItem={this._renderItem}
                            initialNumToRender={10}
                        />
                    ) : (
                        <FlatList
                            refreshControl={
                                <RefreshControl onRefresh={this._fetch} refreshing={loadingGet} />
                            }
                            data={this.state.dataToShow}
                            extraData={this.state}
                            keyExtractor={this._keyExtractor}
                            ListHeaderComponent={this._renderListHeaderComponent}
                            ItemSeparatorComponent={this._renderItemSeparatorComponent}
                            renderItem={this._renderItem}
                            initialNumToRender={10}
                        />
                    )}

                    <Animatable.View
                        useNativeDriver
                        animation="fadeIn"
                        duration={300}
                        style={styles.submit_btn_container}
                    >
                        <ButtonPrimary
                            onPress={this._handleSpaTreatmentList}
                            text={choose_treatment}
                            backgroundColor={color || colors.VIOLET}
                        />
                    </Animatable.View>
                    <SafeAreaView style={{ marginBottom: wp(4) }} />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    submit_btn_container: {
        paddingHorizontal: scale.w(57),
        marginTop: scale.w(18),
    },
    item_container: {
        flexDirection: 'row',
        paddingLeft: scale.w(5),
        paddingRight: scale.w(5),
    },
    searchview: {
        marginHorizontal: scale.w(21),

        height: wp(12),
        justifyContent: 'center',
        marginTop: 10,
        borderRadius: 50,
        backgroundColor: '#ECECEC',
    },
    image: {
        alignContent: 'center',
        width: wp(14),
        height: hp(8),
        alignSelf: 'center',
        // position: 'relative',
    },
    modalContainer: {
        height: '100%',
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        marginHorizontal: 10,
    },
    modal: {
        height: '100%',
        marginLeft: -1,
        paddingVertical: 20,
        marginBottom: -1,
    },
    image1: {
        alignContent: 'center',
        width: '100%',
        height: hp('100%'),
        resizeMode: 'contain',
        alignSelf: 'center',
        // position: 'relative',
    },
});

export default SpaTreatmentList;
