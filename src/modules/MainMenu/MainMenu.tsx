import React from 'react';
import { View, StyleSheet, Alert, ScrollView, Platform, Image } from 'react-native';
import { IMainMenuReduxProps } from './MainMenu.Container';
import base from '../../utils/baseStyles';
import { scale, widthPercentageToDP as wp, heightPercentageToDP as hp } from '../../utils/dimensions';
import { H1, H2 } from '../_global/Text';
import { RootContainer } from '../_global/Container';
import { ButtonPrimary } from '../_global/Button';
import MenuButton from '../_global/MenuButton';
import { View as ViewAnimatable } from 'react-native-animatable';
import { Navigation } from 'react-native-navigation';
import firebase from 'react-native-firebase';

import {
    checkin,
    checkout,
    chat,
    restaurantList,
    conciergeService,
    spaService,
    //cleaningService,
    experienceService,
    trackingProgress,
} from '../../utils/navigationControl';
import { IFeatureHotel } from '../../types/hotel';
import colors from '../../constants/colors';

interface IMainMenuProps extends IMainMenuReduxProps {
    componentId: string;
}

interface IMainMenuState {
    loading: boolean;
    showPassword: boolean;
}

class MainMenu extends React.Component<IMainMenuProps, IMainMenuState> {
    constructor(props: IMainMenuProps) {
        super(props);

        Navigation.events().bindComponent(this);
        Navigation.mergeOptions(props.componentId, {
            statusBar: {
                backgroundColor: props.color.primary_color,
                style: 'light',
            },
        });
        this._isLockFeature = this._isLockFeature.bind(this);
        this._handleCheckIn = this._handleCheckIn.bind(this);
        this._handleCheckOut = this._handleCheckOut.bind(this);
        this._handleRestaurant = this._handleRestaurant.bind(this);
        this._handleSpa = this._handleSpa.bind(this);

        this._handleExperience = this._handleExperience.bind(this);

        this._handleConciergeService = this._handleConciergeService.bind(this);
        // this._handleCleaningService = this._handleCleaningService.bind(this);
        this._handleChat = this._handleChat.bind(this);
    }

    componentDidMount() {
        /////////////////////////////////////////////////////////////////////////////////////////////////////////
        firebase.notifications().onNotification((notification) => {
            if (Platform.OS === 'android') {
                const localNotification = new firebase.notifications.Notification().android

                    .setChannelId('servr')
                    .android.setSmallIcon('ic_launcher_round')
                    .android.setPriority(firebase.notifications.Android.Priority.High)
                    .setSound(firebase.notifications.Android.Defaults.Sound.toString())
                    .setNotificationId(notification.notificationId)
                    .setTitle('New message')
                    .setSubtitle(
                        `Unread message: ${JSON.parse(notification.data.sendbird).unread_message_count}`,
                    )
                    .setBody(notification.data.message)
                    .setData(JSON.parse(notification.data.sendbird));

                firebase
                    .notifications()
                    .displayNotification(localNotification)
                    .catch((err) => console.error(err));
            } else if (Platform.OS === 'ios') {
                const localNotification = new firebase.notifications.Notification()
                    .setNotificationId(notification.messageId)
                    .setTitle('New message')
                    .setSubtitle(
                        `Unread message: ${JSON.parse(notification.data.sendbird).unread_message_count}`,
                    )
                    .setBody(notification.data.message)
                    .setData(JSON.parse(notification.data.sendbird))
                    .ios.setBadge(notification.ios.badge);

                firebase
                    .notifications()
                    .displayNotification(localNotification)
                    .catch((err) => console.error(err));
            }
        });
        ////////////////////////////////////////////////////////////////////////////////////////////////////////
        firebase
            .notifications()
            .getInitialNotification()
            .then((notificationOpen) => {
                if (notificationOpen) {
                    console.log(notificationOpen);

                    console.log('1---OPEND');
                    firebase.notifications().removeAllDeliveredNotifications();
                    console.log('1---OPEND-After');
                }
            });
        firebase.notifications().onNotificationOpened((notificationOpen) => {
            console.log('notification', notificationOpen);
            if (notificationOpen && notificationOpen.notification.data.message) {
                console.log('1234 == ', notificationOpen);
                if (
                    notificationOpen.notification.data.message.includes('Check In request has been accepted')
                ) {
                    Navigation.push(this.props.componentId, checkin({ backGround: true }));
                }
                //laundry order
                else if (
                    notificationOpen.notification.data.message.includes('Your laundry order is Confirmed')
                ) {
                    Navigation.push(this.props.componentId, conciergeService({ backGround: true }));
                } else if (
                    notificationOpen.notification.data.message.includes('Your laundry order is Completed')
                ) {
                    // Navigation.setRoot({ root: mainmenu });

                    // Navigation.push('conciergeService',conciergeService);
                    console.log('Laundry order is completed ---> ', notificationOpen.notification.data.text);
                    Navigation.push(this.props.componentId, conciergeService({ backGround: true }));
                } else if (
                    notificationOpen.notification.data.message.includes('Your laundry order is Rejected')
                ) {
                    Navigation.push(
                        notificationOpen.notification.data.message,
                        conciergeService({ backGround: true }),
                    );
                }
                // else if(notificationOpen.notification.data.text.includes('Laundry service has been requested')){
                //     Navigation.push(this.props.componentId, conciergeService);
                // }
                //room cleaning
                else if (
                    notificationOpen.notification.data.message.includes(
                        'Your room cleaning order is Confirmed',
                    )
                ) {
                    Navigation.push(this.props.componentId, conciergeService({ backGround: true }));
                } else if (
                    notificationOpen.notification.data.message.includes('Your room cleaning order is Done')
                ) {
                    Navigation.push(this.props.componentId, conciergeService({ backGround: true }));
                } else if (
                    notificationOpen.notification.data.message.includes(
                        'Your room cleaning order is Rejected',
                    )
                ) {
                    Navigation.push(this.props.componentId, conciergeService({ backGround: true }));
                } else if (
                    notificationOpen.notification.data.message.includes(
                        'Your room cleaning order is Completed',
                    )
                ) {
                    Navigation.push(this.props.componentId, conciergeService({ backGround: true }));
                }
                //conciergeService
                else if (
                    notificationOpen.notification.data.message.includes(
                        'Your Concierge Service Request is confirmed',
                    )
                ) {
                    Navigation.push(this.props.componentId, conciergeService({ backGround: true }));
                } else if (
                    notificationOpen.notification.data.message.includes(
                        'Your Concierge Service Request is Done',
                    )
                ) {
                    Navigation.push(this.props.componentId, conciergeService({ backGround: true }));
                } else if (
                    notificationOpen.notification.data.message.includes(
                        'Your Concierge Service Request is Rejected',
                    )
                ) {
                    Navigation.push(this.props.componentId, conciergeService({ backGround: true }));
                }
                //restaurantList
                else if (
                    notificationOpen.notification.data.message.includes('Your restaurant order is Confirmed')
                ) {
                    // Navigation.push(this.props.componentId, restaurantList({backGround:true}));
                    Navigation.push(this.props.componentId, trackingProgress);
                } else if (
                    notificationOpen.notification.data.message.includes('Your restaurant order is Preparing')
                ) {
                    // Navigation.push(this.props.componentId, restaurantList({backGround:true}));
                    Navigation.push(this.props.componentId, trackingProgress);
                } else if (
                    notificationOpen.notification.data.message.includes('Your restaurant order is On The Way')
                ) {
                    // Navigation.push(this.props.componentId, restaurantList({backGround:true}));
                    Navigation.push(this.props.componentId, trackingProgress);
                } else if (
                    notificationOpen.notification.data.message.includes('Your restaurant order is Done')
                ) {
                    // Navigation.push(this.props.componentId, restaurantList({backGround:true}));
                    Navigation.push(this.props.componentId, trackingProgress);
                } else if (
                    notificationOpen.notification.data.message.includes('Your restaurant order is Rejected')
                ) {
                    // Navigation.push(this.props.componentId, restaurantList({backGround:true}));
                    Navigation.push(this.props.componentId, trackingProgress);
                }
                //Restaurant Book Table
                else if (
                    notificationOpen.notification.data.message.includes(
                        'Your restaurant reservation has been accepted',
                    )
                ) {
                    Navigation.push(this.props.componentId, restaurantList({ backGround: true }));
                } else if (
                    notificationOpen.notification.data.message.includes(
                        'Unfortunatly your restaurant reservation has been declined, please try to book again or contact your hotel',
                    )
                ) {
                    Navigation.push(this.props.componentId, restaurantList({ backGround: true }));
                }
                //Spa Booking
                else if (
                    notificationOpen.notification.data.message.includes(
                        'Your spa reservation has been accepted',
                    )
                ) {
                    Navigation.push(this.props.componentId, spaService({ backGround: true }));
                } else {
                    // this._handleChat;
                    Navigation.push(this.props.componentId, chat());
                }
            } else if (notificationOpen && notificationOpen.notification.data.text) {
                console.log('1234 == ', notificationOpen);

                if (notificationOpen.notification.data.text.includes('Check In request has been accepted')) {
                    Navigation.push(this.props.componentId, checkin({ backGround: true }));
                }
                //laundry order
                else if (
                    notificationOpen.notification.data.text.includes('Your laundry order is Confirmed')
                ) {
                    Navigation.push(this.props.componentId, conciergeService({ backGround: true }));
                } else if (
                    notificationOpen.notification.data.text.includes('Your laundry order is Completed')
                ) {
                    // Navigation.setRoot({ root: mainmenu });

                    // Navigation.push('conciergeService',conciergeService);
                    console.log('Laundry order is completed ---> ', notificationOpen.notification.data.text);
                    Navigation.push(this.props.componentId, conciergeService({ backGround: true }));
                } else if (
                    notificationOpen.notification.data.text.includes('Your laundry order is Rejected')
                ) {
                    Navigation.push(this.props.componentId, conciergeService({ backGround: true }));
                }
                // else if(notificationOpen.notification.data.text.includes('Laundry service has been requested')){
                //     Navigation.push(this.props.componentId, conciergeService);
                // }
                //room cleaning
                else if (
                    notificationOpen.notification.data.text.includes('Your room cleaning order is Confirmed')
                ) {
                    Navigation.push(this.props.componentId, conciergeService({ backGround: true }));
                } else if (
                    notificationOpen.notification.data.text.includes('Your room cleaning order is Done')
                ) {
                    Navigation.push(this.props.componentId, conciergeService({ backGround: true }));
                } else if (
                    notificationOpen.notification.data.text.includes('Your room cleaning order is Rejected')
                ) {
                    Navigation.push(this.props.componentId, conciergeService({ backGround: true }));
                } else if (
                    notificationOpen.notification.data.text.includes('Your room cleaning order is Completed')
                ) {
                    Navigation.push(this.props.componentId, conciergeService({ backGround: true }));
                }
                //conciergeService
                else if (
                    notificationOpen.notification.data.text.includes(
                        'Your Concierge Service Request is confirmed',
                    )
                ) {
                    Navigation.push(this.props.componentId, conciergeService({ backGround: true }));
                } else if (
                    notificationOpen.notification.data.text.includes('Your Concierge Service Request is Done')
                ) {
                    Navigation.push(this.props.componentId, conciergeService({ backGround: true }));
                } else if (
                    notificationOpen.notification.data.text.includes(
                        'Your Concierge Service Request is Rejected',
                    )
                ) {
                    Navigation.push(this.props.componentId, conciergeService({ backGround: true }));
                }
                //restaurantList
                else if (
                    notificationOpen.notification.data.text.includes('Your restaurant order is Confirmed')
                ) {
                    // Navigation.push(this.props.componentId, restaurantList({backGround:true}));
                    Navigation.push(this.props.componentId, trackingProgress);
                } else if (
                    notificationOpen.notification.data.text.includes('Your restaurant order is Preparing')
                ) {
                    // Navigation.push(this.props.componentId, restaurantList({backGround:true}));
                    Navigation.push(this.props.componentId, trackingProgress);
                } else if (
                    notificationOpen.notification.data.text.includes('Your restaurant order is On The Way')
                ) {
                    // Navigation.push(this.props.componentId, restaurantList({backGround:true}));
                    Navigation.push(this.props.componentId, trackingProgress);
                } else if (
                    notificationOpen.notification.data.text.includes('Your restaurant order is Done')
                ) {
                    // Navigation.push(this.props.componentId, restaurantList({backGround:true}));
                    Navigation.push(this.props.componentId, trackingProgress);
                } else if (
                    notificationOpen.notification.data.text.includes('Your restaurant order is Rejected')
                ) {
                    // Navigation.push(this.props.componentId, restaurantList({backGround:true}));
                    Navigation.push(this.props.componentId, trackingProgress);
                }
                //Restaurant Book Table
                else if (
                    notificationOpen.notification.data.text.includes(
                        'Your restaurant reservation has been accepted',
                    )
                ) {
                    Navigation.push(this.props.componentId, restaurantList({ backGround: true }));
                } else if (
                    notificationOpen.notification.data.text.includes(
                        'Unfortunatly your restaurant reservation has been declined, please try to book again or contact your hotel',
                    )
                ) {
                    Navigation.push(this.props.componentId, restaurantList({ backGround: true }));
                }
                //Spa Booking
                else if (
                    notificationOpen.notification.data.text.includes('Your spa reservation has been accepted')
                ) {
                    Navigation.push(this.props.componentId, spaService({ backGround: true }));
                } else {
                    // this._handleChat;
                    Navigation.push(this.props.componentId, chat());
                }
            }

            console.log('OPEND');
            firebase.notifications().removeAllDeliveredNotifications();
            console.log('OPEND-After');
        });

        ////////////////////////////////////////////////////////////////////////////////////////
        if (this.props.isCheckedIn) {
            // connect sendbird if user already checked in
            let execOnce = false;
            this.props.connectSendBird();
            this.props.getTotalUnreadMessage();
            this.props.onMessageReceived(() => {
                if (!execOnce) {
                    this.props.getProfile();
                    execOnce = true;
                }
            });
            this.props.onTokenNotifRefresh();
            this.props.handleAppStateChange();
        }
    }

    componentDidAppear() {
        if (this.props.isCheckedIn) {
            this.props.getProfile();
        }
    }

    UNSAFE_componentWillReceiveProps(nextProps: IMainMenuProps) {
        // if user just checked in, then connect sendbird
        if (this.props.isCheckedIn !== nextProps.isCheckedIn && nextProps.isCheckedIn) {
            // for checked in, listen all incoming data
            let execOnce = false;
            this.props.getTotalUnreadMessage();
            this.props.onMessageReceived(() => {
                if (!execOnce) {
                    this.props.getProfile();
                    execOnce = true;
                }
            });
            this.props.onTokenNotifRefresh();
            this.props.handleAppStateChange();
        }
    }

    componentWillUnmount() {
        if (this.props.isCheckedIn) {
            this.props.removeOnTokenNotifRefresh();
            this.props.removeOnMessageReceived();
            this.props.removeAppStateChange();
            this.props.disconnectSendBird();
        }
    }

    _isLockFeature(feature?: keyof IFeatureHotel) {
        if (feature !== 'is_check_in_enabled' && !this.props.isCheckedIn) {
            Alert.alert('Attention', 'Please check in first, to use this service');

            return true;
        }

        if (feature !== 'is_check_in_enabled' && this.props.status === 'pending') {
            Alert.alert('Attention', 'To use this feature, your check in must be accepted by hotel admin');

            return true;
        }

        // if (feature && !this.props.feature[feature]) {
        //     Alert.alert('Attention', 'This service is unavailable for this hotel');

        //     return true;
        // }

        return false;
    }

    _handleCheckIn() {
        console.log('handle check in');
        if (this._isLockFeature('is_check_in_enabled')) {
            return false;
        }

        Navigation.push(this.props.componentId, checkin({ backGround: false }));
    }

    _handleCheckOut() {
        if (this._isLockFeature('is_check_out_enabled')) {
            return false;
        }
        Navigation.push(this.props.componentId, checkout);
    }

    _handleRestaurant() {
        if (this._isLockFeature('is_restaurant_enabled')) {
            return false;
        }

        Navigation.push(this.props.componentId, restaurantList({ backGround: false }));
    }

    _handleSpa() {
        if (this._isLockFeature('is_spa_enabled')) {
            return false;
        }

        Navigation.push(this.props.componentId, spaService({ backGround: false }));
    }
    ///////////////////////////////////////////////////////////////////////////
    _handleExperience() {
        if (this._isLockFeature('is_experience_enabled')) {
            return false;
        }

        Navigation.push(this.props.componentId, experienceService);
    }

    _handleConciergeService() {
        if (this._isLockFeature('is_concierge_enabled')) {
            return false;
        }

        Navigation.push(this.props.componentId, conciergeService({ backGround: false }));
    }

    // _handleCleaningService() {
    //     if (this._isLockFeature('is_cleaning_enabled')) {
    //         return false;
    //     }

    //     Navigation.push(this.props.componentId, cleaningService);
    // }

    _handleChat() {
        if (this._isLockFeature()) {
            return false;
        }

        Navigation.push(this.props.componentId, chat());
    }

    render() {
        console.log('propssss', this.props);
        const {
            icon,
            color,
            title,
            countUnreadMessage,
            mobile_hotel_layout_id,
            mobile_hotel_layouts,
            hotel_logo,
        } = this.props;
        const {
            check_in,
            check_out,
            restaurant,
            spa,
            conceirge_service,
            experience,
            live_chat,
            parking_and_valet,
        } = this.props.selectedLanguage;
        if (mobile_hotel_layout_id != 0 && mobile_hotel_layouts.layout_name != 'default') {
            return (
                <View style={[base.container, { backgroundColor: '#FDFDFD' }]}>
                    <RootContainer>
                        <View style={styles.container}>
                            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                <ViewAnimatable
                                    useNativeDriver
                                    animation="fadeIn"
                                    duration={400}
                                    style={{
                                        paddingHorizontal: scale.w(10),
                                        // height:wp(30),
                                        //     width:wp(100)
                                    }}
                                >
                                    {/* <Image source = {{uri:hotel_logo}} resizeMode={'contain'} style={{height:hp(20),width :wp(20)}} /> */}
                                    <H1
                                        fontSize={scale.w(36)}
                                        textAlign="center"
                                        color={color.primary_color || colors.BLUE}
                                    >
                                        {title !== '' ? title : 'Hotel Name'}
                                    </H1>
                                </ViewAnimatable>
                            </View>
                            <ScrollView showsVerticalScrollIndicator={false}>
                                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                    {this.props.feature.is_check_in_enabled && (
                                        <MenuButton
                                            height={wp(30)}
                                            width={wp(100)}
                                            source={require('../../images/icon_check_in.png')}
                                            text={check_in}
                                            onPress={this._handleCheckIn}
                                            iconSize={wp(13)}
                                            styleImage={{ marginRight: scale.w(20), tintColor: colors.WHITE }}
                                            showImageBackground={true}
                                            borderRadius={0}
                                            marginVertical={scale.w(1)}
                                            paddingVertical={scale.w(1)}
                                            white={true}
                                            fontSize={scale.w(18)}
                                            bold={true}
                                            elevation={false}
                                            source2={{ uri: mobile_hotel_layouts.checkin_image }}
                                        />
                                    )}

                                    {this.props.feature.is_check_out_enabled && (
                                        <MenuButton
                                            height={wp(30)}
                                            width={wp(100)}
                                            source={require('../../images/icon_check_out.png')}
                                            text={check_out}
                                            iconSize={wp(13)}
                                            onPress={this._handleCheckOut}
                                            styleImage={{ tintColor: colors.WHITE }}
                                            showImageBackground={true}
                                            borderRadius={0}
                                            marginVertical={scale.w(1)}
                                            paddingVertical={scale.w(1)}
                                            white={true}
                                            bold={true}
                                            elevation={false}
                                            source2={{ uri: mobile_hotel_layouts.checkout_image }}
                                        />
                                    )}

                                    {this.props.feature.is_restaurant_enabled && (
                                        <MenuButton
                                            height={wp(30)}
                                            width={wp(100)}
                                            source={require('../../images/icon_restaurant.png')}
                                            text={restaurant}
                                            iconSize={wp(13)}
                                            onPress={this._handleRestaurant}
                                            styleImage={{ tintColor: colors.WHITE }}
                                            showImageBackground={true}
                                            borderRadius={0}
                                            marginVertical={scale.w(1)}
                                            paddingVertical={scale.w(1)}
                                            white={true}
                                            bold={true}
                                            elevation={false}
                                            source2={{ uri: mobile_hotel_layouts.restaurant_image }}
                                        />
                                    )}

                                    {this.props.feature.is_spa_enabled && (
                                        <MenuButton
                                            height={wp(30)}
                                            width={wp(100)}
                                            source={require('../../images/icon_spa.png')}
                                            text={spa}
                                            iconSize={wp(13)}
                                            onPress={this._handleSpa}
                                            styleImage={{ tintColor: colors.WHITE }}
                                            showImageBackground={true}
                                            borderRadius={0}
                                            marginVertical={scale.w(1)}
                                            paddingVertical={scale.w(1)}
                                            white={true}
                                            bold={true}
                                            elevation={false}
                                            source2={{ uri: mobile_hotel_layouts.spa_image }}
                                        />
                                    )}

                                    {this.props.feature.is_concierge_enabled && (
                                        <MenuButton
                                            height={wp(30)}
                                            width={wp(100)}
                                            source={require('../../images/icon_concierge_home_page.png')}
                                            text={conceirge_service}
                                            iconSize={wp(13)}
                                            onPress={this._handleConciergeService}
                                            styleImage={{ tintColor: colors.WHITE }}
                                            showImageBackground={true}
                                            borderRadius={0}
                                            marginVertical={scale.w(1)}
                                            paddingVertical={scale.w(1)}
                                            white={true}
                                            bold={true}
                                            elevation={false}
                                            source2={{ uri: mobile_hotel_layouts.concierge_image }}
                                        />
                                    )}

                                    {this.props.feature.is_experience && (
                                        <MenuButton
                                            height={wp(30)}
                                            width={wp(100)}
                                            source={require('../../images/icon_star.png')}
                                            text={experience}
                                            iconSize={wp(13)}
                                            onPress={this._handleExperience}
                                            styleImage={{ tintColor: colors.WHITE }}
                                            showImageBackground={true}
                                            borderRadius={0}
                                            marginVertical={scale.w(1)}
                                            paddingVertical={scale.w(1)}
                                            white={true}
                                            bold={true}
                                            elevation={false}
                                            source2={{ uri: mobile_hotel_layouts.experience_image }}
                                        />
                                    )}
                                </View>
                            </ScrollView>

                            <View style={{ justifyContent: 'center' }}>
                                <ViewAnimatable
                                    useNativeDriver
                                    animation="fadeIn"
                                    duration={300}
                                    style={{
                                        paddingHorizontal: scale.w(57),
                                        paddingTop: scale.w(6),
                                        paddingBottom: scale.w(6),
                                    }}
                                >
                                    <ButtonPrimary
                                        backgroundColor={color.primary_color || colors.LIGHT_BLUE}
                                        onPress={this._handleChat}
                                        text={live_chat}
                                    />
                                    {countUnreadMessage > 0 && (
                                        <View style={styles.notif_badge_container}>
                                            <H2 fontSize={scale.w(12)} color={colors.WHITE}>
                                                {countUnreadMessage > 99 ? '99+' : countUnreadMessage}
                                            </H2>
                                        </View>
                                    )}
                                </ViewAnimatable>
                            </View>
                        </View>
                    </RootContainer>
                </View>
            );
        } else {
            return (
                <View style={[base.container, { backgroundColor: '#FDFDFD' }]}>
                    <RootContainer>
                        <View style={styles.container}>
                            <View style={{ flex: 0.16, justifyContent: 'center', alignItems: 'center' }}>
                                <ViewAnimatable
                                    useNativeDriver
                                    animation="fadeIn"
                                    duration={400}
                                    style={{ paddingHorizontal: scale.w(10) }}
                                >
                                    <Image
                                        source={{ uri: hotel_logo }}
                                        resizeMode={'contain'}
                                        style={{ height: wp(25), width: wp(25) }}
                                    />

                                    {/* <H1
                                        fontSize={scale.w(36)}
                                        textAlign="center"
                                        color={color.primary_color || colors.BLUE}
                                    >
                                        {title !== '' ? title : 'Hotel Name'}
                                    </H1> */}
                                </ViewAnimatable>
                            </View>

                            <View style={styles.container_menu}>
                                <ScrollView showsVerticalScrollIndicator={false}>
                                    <View style={{ flexDirection: 'row', marginBottom: 5 }}>
                                        {this.props.feature.is_check_in_enabled && (
                                            <MenuButton
                                                height={hp(28)}
                                                width={wp(38)}
                                                source={require('../../images/icon_check_in.png')}
                                                text={check_in}
                                                onPress={this._handleCheckIn}
                                                iconSize={wp(13)}
                                                styleImage={{
                                                    marginRight: scale.w(20),
                                                    tintColor: icon.check_in_color,
                                                }}
                                            />
                                        )}

                                        {this.props.feature.is_check_out_enabled && (
                                            <MenuButton
                                                height={hp(28)}
                                                width={wp(38)}
                                                source={require('../../images/icon_check_out.png')}
                                                text={check_out}
                                                iconSize={wp(13)}
                                                onPress={this._handleCheckOut}
                                                styleImage={{ tintColor: icon.check_out_color }}
                                            />
                                        )}
                                    </View>

                                    <View style={{ flexDirection: 'row', marginBottom: 5 }}>
                                        {this.props.feature.is_restaurant_enabled && (
                                            <MenuButton
                                                height={hp(28)}
                                                width={wp(38)}
                                                source={require('../../images/icon_restaurant.png')}
                                                text={restaurant}
                                                iconSize={wp(13)}
                                                onPress={this._handleRestaurant}
                                                styleImage={{ tintColor: icon.restaurant_color }}
                                            />
                                        )}

                                        {this.props.feature.is_spa_enabled && (
                                            <MenuButton
                                                height={hp(28)}
                                                width={wp(38)}
                                                source={require('../../images/icon_spa.png')}
                                                text={spa}
                                                iconSize={wp(13)}
                                                onPress={this._handleSpa}
                                                styleImage={{ tintColor: icon.spa_color }}
                                            />
                                        )}
                                    </View>

                                    <View style={{ flexDirection: 'row', marginBottom: 5 }}>
                                        {this.props.feature.is_concierge_enabled && (
                                            <MenuButton
                                                height={hp(28)}
                                                width={wp(38)}
                                                source={require('../../images/icon_concierge_home_page.png')}
                                                text={conceirge_service}
                                                iconSize={wp(13)}
                                                onPress={this._handleConciergeService}
                                                styleImage={{ tintColor: icon.concierge_color }}
                                            />
                                        )}

                                        {this.props.feature.is_experience && (
                                            <MenuButton
                                                height={hp(28)}
                                                width={wp(38)}
                                                source={require('../../images/icon_star.png')}
                                                text={experience}
                                                iconSize={wp(13)}
                                                onPress={this._handleExperience}
                                                styleImage={{ tintColor: icon.cleaning_color }}
                                            />
                                        )}
                                    </View>
                                    <View style={{ flexDirection: 'row', marginBottom: 5 }}>
                                        {this.props.feature.is_concierge_enabled && (
                                            <MenuButton
                                                height={hp(28)}
                                                width={wp(81)}
                                                source={require('../../images/car-icon.png')}
                                                text={parking_and_valet}
                                                iconSize={wp(13)}
                                                onPress={() => {
                                                    Alert.alert('Coming soon');
                                                }}
                                                styleImage={{ tintColor: icon.concierge_color }}
                                            />
                                        )}
                                    </View>
                                </ScrollView>
                            </View>
                            <View style={{ flex: 0.14, justifyContent: 'center' }}>
                                <ViewAnimatable
                                    useNativeDriver
                                    animation="fadeIn"
                                    duration={300}
                                    style={{ paddingHorizontal: scale.w(57), paddingTop: scale.w(10) }}
                                >
                                    <ButtonPrimary
                                        backgroundColor={color.primary_color || colors.LIGHT_BLUE}
                                        onPress={this._handleChat}
                                        text={live_chat}
                                    />
                                    {countUnreadMessage > 0 && (
                                        <View style={styles.notif_badge_container}>
                                            <H2 fontSize={scale.w(12)} color={colors.WHITE}>
                                                {countUnreadMessage > 99 ? '99+' : countUnreadMessage}
                                            </H2>
                                        </View>
                                    )}
                                </ViewAnimatable>
                            </View>
                        </View>
                    </RootContainer>
                </View>
            );
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        // paddingVertical: scale.w(24),
    },
    container_menu: {
        flex: 0.7,
        justifyContent: 'center',
        alignContent: 'center',
        // flexDirection: 'row',
        // flexWrap: 'wrap',
        alignSelf: 'center',
    },
    notif_badge_container: {
        position: 'absolute',
        right: 0,
        top: 0,
        backgroundColor: colors.RED,
        height: scale.w(30),
        width: scale.w(30),
        borderRadius: scale.w(30 / 2),
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: scale.w(45),
    },
});

export default MainMenu;
