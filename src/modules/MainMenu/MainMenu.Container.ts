import { connect } from 'react-redux';
import MainMenu from './MainMenu';
import { bindActionCreators, Dispatch } from 'redux';
import { IState } from '../../types/state';
import {
    connectSendBird,
    onMessageReceived,
    removeOnMessageReceived,
    getTotalUnreadMessage,
    handleAppStateChange,
    removeAppStateChange,
    disconnectSendBird,
} from '../../actions/action.chat';
import {
    onTokenNotifRefresh,
    requestNotifPermission,
    removeOnTokenNotifRefresh,
} from '../../actions/action.notification';
import { getProfile } from '../../actions/action.account';
import { getHotelDetail } from '../../actions/action.hotel';

const mapStateToProps = (state: IState) => ({
    isCheckedIn: state.account.isCheckedIn,
    status: state.account.profile.status ? state.account.profile.status : 'pending',
    feature: state.hotel.feature,
    icon: state.hotel.icon,
    color: state.hotel.theme,
    title: state.hotel.name,
    countUnreadMessage: state.chat.unreadMessage,
    mobile_hotel_layout_id: state.hotel.mobile_hotel_layout_id,
    mobile_hotel_layouts: state.hotel.mobile_hotel_layouts,
    hotel_logo: state.hotel.logo.hotel_logo_md,
    selectedLanguage: state.language,
});

const mapDispatchToProps = (dispatch: Dispatch) => {
    return bindActionCreators(
        {
            connectSendBird,
            requestNotifPermission,
            onTokenNotifRefresh,
            removeOnTokenNotifRefresh,
            onMessageReceived,
            removeOnMessageReceived,
            getTotalUnreadMessage,
            handleAppStateChange,
            removeAppStateChange,
            getProfile,
            disconnectSendBird,
        },
        dispatch,
    );
};

export interface IMainMenuReduxProps
    extends ReturnType<typeof mapStateToProps>,
        ReturnType<typeof mapDispatchToProps> {}

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(MainMenu);
