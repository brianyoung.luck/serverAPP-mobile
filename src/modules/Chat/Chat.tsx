import React from 'react';
import { View, Platform, Image, Keyboard, StyleSheet, KeyboardAvoidingView } from 'react-native';
import base from '../../utils/baseStyles';
import { scale } from '../../utils/dimensions';
import { H4 } from '../_global/Text';
import Navbar from '../_global/Navbar';
import { Navigation } from 'react-native-navigation';
import { GiftedChat, IMessage, BubbleProps } from 'react-native-gifted-chat';
import { IChatReduxProps } from './Chat.Container';
import { IMessageChat } from '../../types/chat';
import * as Animatable from 'react-native-animatable';

export interface IChatBaseProps {
    from?: 'restaurant' | 'concierge_service' | 'main_menu';
}

interface IChatProps extends IChatBaseProps, IChatReduxProps {
    componentId: string;
}

interface IChatState {
    composedChat: string;
}

const openingChatFromAdmin: IMessageChat = {
    _id: 0,
    createdAt: new Date().getTime(),
    text: 'Hi! How can I help?',
    user: {
        _id: '-1',
        name: 'Admin',
        avatar: require('../../images/icon_concierge.png'),
    },
    messageType: 'other',
};

class Chat extends React.Component<IChatProps, IChatState> {
    constructor(props: IChatProps) {
        super(props);

        this.state = {
            composedChat: '',
        };

        props.connectSendBird();
        props.joinChannel();

        Navigation.mergeOptions(props.componentId, {
            statusBar: {
                backgroundColor: props.color,
                style: 'light',
            },
        });
        this._handleBack = this._handleBack.bind(this);
        this._handleSend = this._handleSend.bind(this);
        this._renderBubble = this._renderBubble.bind(this);
        this._renderLoadEarlier = this._renderLoadEarlier.bind(this);
    }

    componentDidMount() {
        this.props.toggleIsInChatScreen(true);
    }

    componentWillUnmount() {
        this.props.toggleIsInChatScreen(false);
        this.props.updateTotalUnreadMessageSuccess(0);
    }

    _handleBack() {
        Keyboard.dismiss();
        Navigation.pop(this.props.componentId);
    }

    _handleSend(messages: IMessage[]) {
        this.props.sendMessage(messages[0].text, () => {
            this.setState({ composedChat: '' });
        });
    }

    _renderBubble({ currentMessage }: BubbleProps<IMessage>) {
        return (
            <View style={styles.chat_bubble_container}>
                <H4 fontSize={scale.w(16)}>{currentMessage ? currentMessage.text : ''}</H4>
            </View>
        );
    }

    _renderLoadEarlier() {
        return (
            <Animatable.View
                animation="fadeIn"
                duration={400}
                style={styles.chat_picture_container}
                useNativeDriver
            >
                <Image
                    source={require('../../images/icon_chat_2.png')}
                    style={[styles.chat_picture, { tintColor: this.props.color }]}
                    resizeMode="contain"
                />

                <H4 textAlign="center" fontSize={scale.w(18)}>
                    {'Here you can live chat with a\nconcierge of your hotel!'}
                </H4>
            </Animatable.View>
        );
    }

    render() {
        return (
            <View style={base.container}>
                <Navbar
                    color={this.props.color !== '' ? this.props.color : undefined}
                    onClick={this._handleBack}
                    title="Chat"
                />

                <GiftedChat
                    isAnimated
                    loadEarlier
                    messages={GiftedChat.append([openingChatFromAdmin], this.props.messages)}
                    user={this.props.profile ? this.props.profile : undefined}
                    onSend={this._handleSend}
                    renderBubble={this._renderBubble}
                    renderLoadEarlier={this._renderLoadEarlier}
                    keyboardShouldPersistTaps="handled"
                />

                {/* <KeyboardAvoidingView
                    keyboardVerticalOffset={32}
                    behavior="padding"
                    enabled={Platform.OS === 'android'}
                /> */}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    chat_picture_container: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: scale.w(22),
        marginBottom: scale.w(24),
    },
    chat_picture: {
        height: scale.w(132),
        width: scale.w(132),
        alignSelf: 'center',
    },
    chat_bubble_container: {
        borderRadius: scale.w(28),
        paddingVertical: scale.w(9),
        paddingHorizontal: scale.w(14),
        marginVertical: scale.w(4),
        maxWidth: '70%',
        backgroundColor: '#fff',
        ...Platform.select({
            ios: {
                shadowColor: '#000',
                shadowOffset: { width: 0, height: 0 },
                shadowOpacity: 0.2,
                shadowRadius: 3,
            },
            android: {
                elevation: 3,
            },
        }),
    },
});

export default Chat;
