import { connect } from 'react-redux';
import Chat, { IChatBaseProps } from './Chat';
import { bindActionCreators, Dispatch } from 'redux';
import { IState } from '../../types/state';
import {
    connectSendBird,
    sendMessage,
    joinChannel,
    toggleIsInChatScreen,
    updateTotalUnreadMessageSuccess,
} from '../../actions/action.chat';
import { createSelector } from 'reselect';

const selectColorBasedMenu = createSelector(
    (state: IState) => state.hotel.icon,
    (state: IState) => state.hotel.theme.primary_color,
    (state: IState, props: IChatBaseProps) => props.from,
    (icon, color, from) => {
        if (from === 'restaurant') {
            return icon.restaurant_color;
        }

        if (from === 'concierge_service') {
            return icon.concierge_color;
        }

        return color;
    },
);

const mapStateToProps = (state: IState, props: IChatBaseProps) => ({
    isConnected: state.chat.isConnected,
    profile: state.chat.profile,
    messages: state.chat.messages,
    color: selectColorBasedMenu(state, props),
    selectedLanguage: state.language,
});

const mapDispatchToProps = (dispatch: Dispatch) => {
    return bindActionCreators(
        {
            connectSendBird,
            joinChannel,
            sendMessage,
            toggleIsInChatScreen,
            updateTotalUnreadMessageSuccess,
        },
        dispatch,
    );
};

export interface IChatReduxProps
    extends ReturnType<typeof mapStateToProps>,
        ReturnType<typeof mapDispatchToProps> {}

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Chat);
