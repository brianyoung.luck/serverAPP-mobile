import React from 'react';
import {
    TouchableOpacity,
    StyleSheet,
    View,
    ActivityIndicator,
    Platform,
    TouchableOpacityProps,
    TextStyle,
} from 'react-native';
import { widthPercentageToDP as wp, scale } from '../../utils/dimensions';
import colors from '../../constants/colors';
import { ButtonLabel, H4 } from './Text';
import IonIcon from 'react-native-vector-icons/Ionicons';
import { debounce } from 'lodash';
import { Text } from 'react-native-animatable';
import Icon from 'react-native-vector-icons/FontAwesome5';

interface IButtonProps extends TouchableOpacityProps, TextStyle {
    loading?: boolean;
    text: string;
    hideShadow?: boolean;
    onPress: () => void;
    backgroundColor?: string;
    loadingColor?: string;
    withIcon: boolean;
}

const ButtonPrimary = (props: IButtonProps) => {
    const btnProps = (({ activeOpacity, disabled }) => ({
        activeOpacity,
        disabled,
    }))(props);

    const { color, fontSize, textAlign } = props;

    return (
        <TouchableOpacity
            {...btnProps}
            onPress={debounce(props.onPress, 1000, {
                leading: true,
                trailing: false,
            })}
            activeOpacity={1}
        >
            <View
                style={StyleSheet.flatten([
                    styles.btn_container,
                    {
                        borderRadius: props.borderRadius || 12,
                        backgroundColor: props.backgroundColor ? props.backgroundColor : colors.LIGHT_BLUE,
                    },
                    !props.hideShadow &&
                        Platform.select({
                            ios: {
                                shadowColor: '#000',
                                shadowOffset: { width: 0, height: 4 },
                                shadowOpacity: 0.2,
                                shadowRadius: 3,
                            },
                            android: {
                                elevation: 8,
                            },
                        }),
                ])}
            >
                {props.loading ? (
                    <ActivityIndicator size="large" color={props.loadingColor || '#fff'} />
                ) : (
                    <H4
                        fontSize={fontSize ? fontSize : scale.w(16.5)}
                        color={color ? color : '#fff'}
                        textAlign={textAlign ? textAlign : 'center'}
                    >
                        {props.text}
                    </H4>
                )}
            </View>
        </TouchableOpacity>
    );
};

const ButtonSecondary = (props: IButtonProps) => {
    const btnProps = (({ activeOpacity, disabled }) => ({
        activeOpacity,
        disabled,
    }))(props);

    const { color, fontSize, textAlign, withIcon = false } = props;

    return (
        <TouchableOpacity
            {...btnProps}
            onPress={debounce(props.onPress, 1000, {
                leading: true,
                trailing: false,
            })}
            disabled={btnProps.disabled}
            activeOpacity={1}
        >
            <View
                style={StyleSheet.flatten([
                    withIcon
                        ? {
                              justifyContent: 'center',
                          }
                        : styles.btn_container,
                    {
                        backgroundColor: '#fff',
                        height: props.height || wp(10),
                        borderRadius: props.borderRadius || 20,
                    },
                    !props.hideShadow &&
                        Platform.select({
                            ios: {
                                shadowColor: '#000',
                                shadowOffset: { width: 0, height: 4 },
                                shadowOpacity: 0.2,
                                shadowRadius: 3,
                            },
                            android: {
                                elevation: 8,
                            },
                        }),
                ])}
            >
                {props.loading ? (
                    <ActivityIndicator size="large" color={props.loadingColor || colors.BLUE} />
                ) : withIcon ? (
                    <>
                        <View
                            style={{
                                flexDirection: 'row',
                            }}
                        >
                            <View style={{ width: scale.w(310), paddingLeft: scale.w(21) }}>
                                <H4
                                    fontSize={fontSize ? fontSize : scale.w(30)}
                                    color={color ? color : '#000'}
                                    // textAlign={textAlign ? textAlign : 'center'}
                                >
                                    {props.text}
                                </H4>
                            </View>
                            <View style={{ alignItems: 'flex-end', paddingTop: scale.h(7) }}>
                                <Icon name="angle-right" size={scale.h(20)} color={color ? color : '#000'} />
                            </View>
                        </View>
                    </>
                ) : (
                    <>
                        <H4
                            fontSize={fontSize ? fontSize : scale.w(30)}
                            color={color ? color : '#000'}
                            textAlign={textAlign ? textAlign : 'center'}
                        >
                            {props.text}
                        </H4>
                    </>
                )}
            </View>
        </TouchableOpacity>
    );
};

interface ICloseButtonProps {
    action: () => void;
    color?: string;
}

const CloseButton = (props: ICloseButtonProps) => (
    <TouchableOpacity
        onPress={debounce(props.action, 1000, {
            leading: true,
            trailing: false,
        })}
        activeOpacity={1}
    >
        <View style={styles.btn_close}>
            <IonIcon name="md-close" size={wp(8)} color={props.color || colors.DARK_BLUE} />
        </View>
    </TouchableOpacity>
);

const BackButton = (props: ICloseButtonProps) => (
    <TouchableOpacity
        onPress={debounce(props.action, 1000, {
            leading: true,
            trailing: false,
        })}
        activeOpacity={1}
    >
        <View style={styles.btn_close}>
            <IonIcon name="md-arrow-back" size={wp(6.5)} color={props.color || colors.DARK_BLUE} />
        </View>
    </TouchableOpacity>
);

const TransparentButton = (props: IButtonProps) => {
    const btnProps = (({ activeOpacity }) => ({ activeOpacity }))(props);
    const labelProps = (({ color, fontSize, textAlign }) => ({ color, fontSize, textAlign }))(props);

    return (
        <TouchableOpacity
            {...btnProps}
            onPress={debounce(props.onPress, 1000, {
                leading: true,
                trailing: false,
            })}
            activeOpacity={1}
        >
            <View style={styles.btn_transparent}>
                {props.loading ? (
                    <ActivityIndicator size="small" />
                ) : (
                    <ButtonLabel {...labelProps}>{props.text}</ButtonLabel>
                )}
            </View>
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    btn_container: {
        height: wp(14),
        alignItems: 'center',
        justifyContent: 'center',
    },
    btn_close: {
        width: wp(10),
        height: wp(10),
        borderRadius: wp(5),
        alignItems: 'center',
        justifyContent: 'center',
    },
    btn_transparent: {
        height: wp(10),
        borderRadius: 3,
        alignItems: 'center',
        justifyContent: 'center',
    },
});

export { ButtonPrimary, ButtonSecondary, CloseButton, BackButton, TransparentButton };
