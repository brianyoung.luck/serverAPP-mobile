import React from 'react';
import { View, TouchableOpacity, StyleSheet, Image } from 'react-native';
import { H1 } from './Text';
import { scale } from '../../utils/dimensions';
import colors from '../../constants/colors';
import { extraPad, phoneType } from '../../utils/extraPad';
import { View as ViewAnimatable } from 'react-native-animatable';
import { debounce } from 'lodash';

interface INavbarProps {
    onClick: () => void;
    rightButton?: boolean;
    onClickRight?: () => void;
    labelRight?: string;
    title?: string;
    isBrown?: boolean;
    isViolet?: boolean;
    color?: string;
}

class Navbar extends React.PureComponent<INavbarProps> {
    render() {
        let color = this.props.color ? this.props.color : colors.LIGHT_BLUE;

        if (this.props.isBrown) {
            color = colors.BROWN;
        }

        if (this.props.isViolet) {
            color = colors.VIOLET;
        }

        return (
            <View style={styles.navbar}>
                {this.props.title && (
                    <ViewAnimatable
                        useNativeDriver
                        animation="fadeInLeft"
                        duration={300}
                        delay={50}
                        style={styles.title_container}
                    >
                        <H1 color={color} textAlign="center" fontSize={scale.w(25)}>
                            {this.props.title}
                        </H1>
                    </ViewAnimatable>
                )}

                <View style={styles.btn_back_absolute}>
                    <TouchableOpacity
                        onPress={debounce(this.props.onClick, 1000, {
                            leading: true,
                            trailing: false,
                        })}
                        activeOpacity={0.8}
                    >
                        <ViewAnimatable
                            useNativeDriver
                            animation="fadeInLeft"
                            duration={300}
                            style={StyleSheet.flatten([
                                styles.btn_back_container,
                                {
                                    backgroundColor: color,
                                },
                            ])}
                        >
                            <Image
                                source={require('../../images/icon_back.png')}
                                style={{ width: scale.w(15), height: scale.w(20), marginLeft: scale.w(5) }}
                                resizeMode="contain"
                            />
                        </ViewAnimatable>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    navbar: {
        minHeight: scale.h(68) + extraPad[phoneType()].top,
        paddingTop: extraPad[phoneType()].top + scale.w(20),
    },
    btn_back_container: {
        width: scale.w(50),
        height: scale.w(36),
        justifyContent: 'center',
        paddingLeft: scale.w(10),
        borderTopRightRadius: scale.w(18),
        borderBottomRightRadius: scale.w(18),
    },
    btn_back_absolute: {
        position: 'absolute',
        left: 0,
        top: 0,
        bottom: 0,
        justifyContent: 'flex-start',
        paddingTop: extraPad[phoneType()].top + scale.w(20),
    },
    title_container: {
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginHorizontal: scale.w(55),
        paddingTop: scale.w(5),
    },
});

export default Navbar;
