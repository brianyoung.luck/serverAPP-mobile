import React from 'react';
import { View, StyleSheet, Image, ScrollView, Platform } from 'react-native';
import base from '../../utils/baseStyles';
import { scale } from '../../utils/dimensions';
import Navbar from '../_global/Navbar';
import { Navigation } from 'react-native-navigation';
import { spaBookingTime } from '../../utils/navigationControl';
import MenuButton from '../_global/experienceButton';
import { IExperienceReduxProps } from './Experience.Container';
import Carousel from 'react-native-snap-carousel';
import { screenWidth } from '../../utils/dimensions';
import { IExperience } from '../../types/experience';
import { promotionService, hotelMap, promotionDetails } from '../../utils/navigationControl';
import { getPromotionDetails, emptyPromotionDetails } from '../../actions/action.promotion';

export interface IExperienceProps extends IExperienceReduxProps {
    componentId: string;
    experience: IExperience;
}

interface IExperienceState {}

class Experience extends React.Component<IExperienceProps, IExperienceState> {
    constructor(props: IExperienceProps) {
        super(props);

        this.state = {};

        Navigation.mergeOptions(props.componentId, {
            statusBar: {
                backgroundColor: props.color,
                style: 'light',
            },
        });
        this._handleBack = this._handleBack.bind(this);
        this._handlePromotions = this._handlePromotions.bind(this);
        this._handleHotelMap = this._handleHotelMap.bind(this);

        // this._handleOrderRoomService = this._handleOrderRoomService.bind(this);
    }

    componentDidMount() {
        this.props.getExperience(this.props.idHotel);
    }
    // _fetch() {
    //     this.setState({ loadingGet: true });
    //     this.props.getExperience(
    //         this.props.idHotel,
    //         () => {
    //             this.setState({ loadingGet: false });
    //         },
    //         () => {
    //             this.setState({ loadingGet: false });
    //         },
    //     );
    // }

    _handleBack() {
        Navigation.pop(this.props.componentId);
    }

    _handlePromotions(idButton: number, btnTitle: string) {
        Navigation.push(
            this.props.componentId,
            promotionService({
                idButton,
                btnTitle,
            }),
        );

        // Navigation.push(
        //     this.props.componentId,
        //     spaBookingTime({
        //         isReserveSpaType: true,
        //         spa: this.props.spa,
        //     }),
        // );
    }
    _handlePromotionDetail(idButton: number, promotionTitle: string) {
        emptyPromotionDetails({ images: [] });
        Navigation.push(
            this.props.componentId,
            promotionDetails({
                idButton,
                promotionTitle,
                // name:''
            }),
        );
    }
    _handleHotelMap() {
        Navigation.push(this.props.componentId, hotelMap);
        // ({
        //     isReserveSpaType: false,
        //     spa: this.props.spa,
        // }),
    }

    _handleShuttleBusService() {
        // Navigation.push(
        //     this.props.componentId,
        //     spaBookingTime({
        //         isReserveSpaType: false,
        //         spa: this.props.spa,
        //     }),
        // );
    }

    render() {
        const { experience, color } = this.props;
        const experienceArr: any = Object.values(experience.experiences); //object to array
        // if (experienceArr.length == 0) {
        //     return null;
        // }
        // else{
        return (
            <View style={base.container}>
                <Navbar
                    isViolet={color === ''}
                    color={color}
                    onClick={this._handleBack}
                    title={this.props.selectedLanguage.experience}
                />
                <ScrollView>
                    <View style={styles.allButtons}>
                        <View style={styles.menu_btn_container}>
                            <MenuButton
                                onPress={() =>
                                    this._handlePromotions(0, this.props.selectedLanguage.Promotions)
                                }
                                source={require('../../images/icon_promotion.png')}
                                text={this.props.selectedLanguage.Promotions}
                                width={scale.w(265)}
                                height={scale.w(150)}
                                iconSize={scale.w(70)}
                                fontSize={scale.w(20)}
                                styleImage={{
                                    // marginTop: scale.w(-15),
                                    // marginBottom: scale.w(-15),
                                    tintColor: color,
                                }}
                            />
                        </View>
                        {experienceArr.map((data: any, i: any) => {
                            return (
                                <View style={styles.menu_btn_container}>
                                    <MenuButton
                                        onPress={() => this._handlePromotionDetail(data.id, data.title)}
                                        source={{ uri: `https://cms.servrhotels.com/icons/${data.icon}.png` }}
                                        text={data.title}
                                        width={scale.w(265)}
                                        height={scale.w(150)}
                                        iconSize={scale.w(70)}
                                        fontSize={scale.w(20)}
                                        styleImage={{
                                            // marginTop: scale.w(-15),
                                            // marginBottom: scale.w(-15),
                                            tintColor: color,
                                        }}
                                    />
                                </View>
                            );
                        })}
                    </View>
                </ScrollView>
            </View>
        );
        // }
    }
}

const styles = StyleSheet.create({
    text_container: {
        marginTop: scale.w(10),
        marginBottom: scale.w(20),
        marginHorizontal: scale.w(28),
    },
    image_container: {
        alignItems: 'center',
        marginTop: scale.w(60),
        marginBottom: scale.w(68),
    },
    menu_btn_container: {
        // paddingVertical: scale.w(10),
        marginBottom: scale.w(10),
        alignItems: 'center',
        justifyContent: 'center',
    },
    logo: {
        width: '100%',
        height: scale.w(200),
        resizeMode: 'contain',
        marginTop: scale.w(20),
    },
    allButtons: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: scale.w(60),
    },
});

export default Experience;
