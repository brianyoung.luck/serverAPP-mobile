import React from 'react';
import { View, StyleSheet, ScrollView } from 'react-native';
import base from '../../utils/baseStyles';
import { scale } from '../../utils/dimensions';
import { H4, H2 } from '../_global/Text';
import Navbar from '../_global/Navbar';
import { Navigation } from 'react-native-navigation';
import * as Animatable from 'react-native-animatable';
import { ButtonPrimary } from '../_global/Button';
import MenuButton from '../_global/MenuButton';
import {
    chat,
    requestItems,
    cleaningRequestComplete,
    laundryService,
    mainmenu,
    conciergeTrackingProgress,
} from '../../utils/navigationControl';
import { IConciergeServiceReduxProps } from './ConciergeService.Container';
import colors from '../../constants/colors';
import { ICleaningServiceReduxProps } from '../CleaningService/CleaningService.Container';

export interface IConciergeServiceProps extends IConciergeServiceReduxProps {
    componentId: string;
    backGround?: boolean;
}

interface IConciergeServiceState {
    loading: boolean;
}
// export interface ICleaningServiceProps extends ICleaningServiceReduxProps {
//     componentId: string;
// }

// interface ICleaningServiceState {
//     loading: boolean;
// }

class ConciergeService extends React.Component<IConciergeServiceProps, IConciergeServiceState> {
    constructor(props: IConciergeServiceProps) {
        super(props);

        this.state = {
            loading: false,
        };

        Navigation.mergeOptions(props.componentId, {
            statusBar: {
                backgroundColor: props.color,
                style: 'light',
            },
        });
        this._handleBack = this._handleBack.bind(this);
        this._handleRequestItems = this._handleRequestItems.bind(this);
        this._handleMyOrders = this._handleMyOrders.bind(this);
        this._handleRoomCleaning = this._handleRoomCleaning.bind(this);
        this._handleLaundryService = this._handleLaundryService.bind(this);
    }

    _handleBack() {
        if (this.props.backGround) {
            Navigation.push(this.props.componentId, mainmenu);
        } else {
            Navigation.pop(this.props.componentId);
        }
    }

    _handleRequestItems() {
        Navigation.push(this.props.componentId, requestItems);
    }

    _handleMyOrders() {
        Navigation.push(this.props.componentId, conciergeTrackingProgress);
        // Navigation.push(this.props.componentId, chat({ from: 'concierge_service' }));
    }

    _handleRoomCleaning() {
        this.setState({ loading: true });
        this.props.roomCleaningService(
            async () => {
                await Navigation.push(
                    this.props.componentId,
                    cleaningRequestComplete({ isRoomCleaning: true }),
                );
                this.setState({ loading: false });
            },
            () => {
                this.setState({ loading: false });
            },
        );
    }

    _handleLaundryService() {
        Navigation.push(this.props.componentId, laundryService);
    }

    render() {
        const { color } = this.props;
        const {
            conceirge_service,
            here_you_can_request_various_items_to_be_delivered_straight_to_you_room,
            request_items,
            request_room_cleaning,
            live_chat,
            my_orders,
        } = this.props.selectedLanguage;

        return (
            <View style={base.container}>
                <Navbar color={color} onClick={this._handleBack} title={conceirge_service} />
                <View style={styles.text_container}>
                    <H4 fontSize={scale.w(18)} textAlign="center">
                        {here_you_can_request_various_items_to_be_delivered_straight_to_you_room}
                    </H4>
                </View>
                <ScrollView>
                    <View style={{ flex: 1 }}>
                        {/* <View style={styles.image_container}>
                            <Animatable.Image
                                useNativeDriver
                                animation="bounceIn"
                                duration={400}
                                source={require('../../images/icon_concierge_home_page_light_blue.png')}
                                style={{
                                    width: scale.w(84),
                                    height: scale.w(84),
                                    tintColor: color,
                                }}
                            />
                        </View> */}
                        <View>
                            <View style={styles.menu_btn_container}>
                                <MenuButton
                                    onPress={this._handleRequestItems}
                                    source={require('../../images/icon_request_items.png')}
                                    text={request_items}
                                    width={scale.w(265)}
                                    height={scale.w(150)}
                                    iconSize={scale.w(118)}
                                    fontSize={scale.w(20)}
                                    styleImage={{
                                        marginTop: scale.w(-15),
                                        marginBottom: scale.w(-15),
                                        tintColor: color,
                                    }}
                                />
                            </View>
                            <View style={styles.menu_btn_container}>
                                <MenuButton
                                    onPress={this._handleRoomCleaning}
                                    source={require('../../images/icon_vacuum.png')}
                                    text={request_room_cleaning}
                                    width={scale.w(265)}
                                    height={scale.w(150)}
                                    iconSize={scale.w(70)}
                                    fontSize={scale.w(20)}
                                    disabled={this.state.loading}
                                    styleImage={{ tintColor: this.props.color }}
                                />
                            </View>

                            <View style={styles.menu_btn_container}>
                                <MenuButton
                                    onPress={this._handleLaundryService}
                                    source={require('../../images/icon_laundry.png')}
                                    text={this.props.selectedLanguage.request_laundry_service}
                                    width={scale.w(265)}
                                    height={scale.w(150)}
                                    iconSize={scale.w(70)}
                                    fontSize={scale.w(20)}
                                    styleImage={{ tintColor: this.props.color }}
                                />
                            </View>
                        </View>
                    </View>
                </ScrollView>
                <View style={{ paddingBottom: 20 }}>
                    <Animatable.View
                        useNativeDriver
                        animation="fadeIn"
                        duration={300}
                        style={{ paddingHorizontal: scale.w(57), paddingTop: scale.w(10) }}
                    >
                        <ButtonPrimary
                            backgroundColor={color}
                            onPress={this._handleMyOrders}
                            text={my_orders}
                        />
                        {/* {this.props.countUnreadMessage > 0 && (
                            <View style={styles.notif_badge_container}>
                                <H2 fontSize={scale.w(12)} color={colors.WHITE}>
                                    {this.props.countUnreadMessage > 99
                                        ? '99+'
                                        : this.props.countUnreadMessage}
                                </H2>
                            </View>
                        )} */}
                    </Animatable.View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    text_container: {
        marginTop: scale.w(10),
        marginHorizontal: scale.w(28),
        marginBottom: scale.h(10),
    },
    image_container: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 0.6,
        marginVertical: scale.w(10),
    },
    menu_btn_container: {
        paddingHorizontal: scale.w(55),
        // marginBottom: scale.w(30),
        alignItems: 'center',
        // paddingVertical:scale.w(5)
    },
    notif_badge_container: {
        position: 'absolute',
        right: 0,
        top: 0,
        backgroundColor: colors.RED,
        height: scale.w(30),
        width: scale.w(30),
        borderRadius: scale.w(30 / 2),
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: scale.w(45),
    },
});

export default ConciergeService;
