import { connect } from 'react-redux';
import TrackingProgress from './ConciergeTrackingProgress';
import { bindActionCreators, Dispatch } from 'redux';
import { IState } from '../../types/state';
import {
    conciergeTrackingProgressOrderRoomService,
    deleteConciergeOrder,
} from '../../actions/action.conciergeService';
import { createSelector } from 'reselect';

const selectCurrentOrderSelector = createSelector(
    (state: IState) => state.conciergeService.conciergeTrakingProgress,
    (orders) =>
        orders.filter(
            ({ status }) =>
                !(['cancelled', 'done', 'rejected', 'completed'] as (typeof status)[]).includes(status),
        ),
);

const selectPreviousOrderSelector = createSelector(
    (state: IState) => state.conciergeService.conciergeTrakingProgress,
    (orders) =>
        orders.filter(({ status }) =>
            (['cancelled', 'done', 'rejected', 'completed'] as (typeof status)[]).includes(status),
        ),
);

const mapStateToProps = (state: IState) => ({
    currentOrder: selectCurrentOrderSelector(state),
    previousOrder: selectPreviousOrderSelector(state),
    color: state.hotel.icon.restaurant_color,
    currency: state.hotel.currency,
    selectedLanguage: state.language,
});

const mapDispatchToProps = (dispatch: Dispatch) => {
    return bindActionCreators(
        {
            conciergeTrackingProgressOrderRoomService,
            deleteConciergeOrder,
        },
        dispatch,
    );
};

export interface ISpaTrackingProgressReduxProps
    extends ReturnType<typeof mapStateToProps>,
        ReturnType<typeof mapDispatchToProps> {}

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(TrackingProgress);
