import React from 'react';
import {
    View,
    StyleSheet,
    FlatList,
    TouchableOpacity,
    SafeAreaView,
    RefreshControl,
    Alert,
    Keyboard,
    Image,
    Text,
    Platform,
} from 'react-native';
import base from '../../utils/baseStyles';
import { scale, heightPercentageToDP as hp, widthPercentageToDP as wp } from '../../utils/dimensions';
import { H4, H3 } from '../_global/Text';
import Navbar from '../_global/Navbar';
import { Navigation } from 'react-native-navigation';
import * as Animatable from 'react-native-animatable';
import { ButtonPrimary } from '../_global/Button';
import { IRequestItemsReduxProps } from './RequestItems.Container';
import colors from '../../constants/colors';
import { IServiceItem } from '../../types/conciergeService';
import { debounce, find, findIndex } from 'lodash';
import { ICreateRequestItem } from '../../types/action.conciergeService';
import Ionicons from 'react-native-vector-icons/Ionicons';
import NoteServiceRequestItem from './Components/NoteServiceRequestItem';
import CustomModal from '../_global/CustomModal';
import Modal from 'react-native-modal';
import { conciergeTrackingProgress } from '../../utils/navigationControl';
interface IRequestItemsProps extends IRequestItemsReduxProps {
    componentId: string;
}

interface ISelectedItems extends ICreateRequestItem {
    name: string;
}

interface IRequestItemsState {
    items: ISelectedItems[];
    selectedItem: ISelectedItems;
    loadingGet: boolean;
    loading: boolean;
    modalVisible: boolean;
    title: string;
    description: string;
    item: any;
}

class RequestItems extends React.Component<IRequestItemsProps, IRequestItemsState> {
    private _modalNoteServiceRequestItem = React.createRef<CustomModal>();

    constructor(props: IRequestItemsProps) {
        super(props);

        this.state = {
            items: [],
            selectedItem: {
                service_id: 0,
                qty: 0,
                note: '',
                name: '',
            },
            loadingGet: false,
            loading: false,
            modalVisible: false,
            title: '',
            description: '',
            item: [],
        };

        Navigation.mergeOptions(props.componentId, {
            statusBar: {
                backgroundColor: props.color,
                style: 'light',
            },
        });
        this._fetch = this._fetch.bind(this);
        this._handleBack = this._handleBack.bind(this);
        this._handleRequestItems = this._handleRequestItems.bind(this);
        this._handleModalNoteServiceRequestItem = this._handleModalNoteServiceRequestItem.bind(this);
        this._onChangeText = this._onChangeText.bind(this);
        this._addTotalService = this._addTotalService.bind(this);
        this._substractTotalService = this._substractTotalService.bind(this);
        this._keyExtractor = this._keyExtractor.bind(this);
        this._renderListHeaderComponent = this._renderListHeaderComponent.bind(this);
        this._renderItemSeparatorComponent = this._renderItemSeparatorComponent.bind(this);
        this._renderItem = this._renderItem.bind(this);
    }

    componentDidMount() {
        // this.props.getConciergeServiceItems();
        this._fetch();
    }
    setModalVisible(visible) {
        this.setState({ modalVisible: visible });
    }
    _fetch() {
        this.setState({ loadingGet: true });
        this.props.getConciergeServiceItems(
            () => {
                this.setState({ loadingGet: false });
            },
            () => {
                this.setState({ loadingGet: false });
            },
        );
    }

    _handleBack() {
        Navigation.pop(this.props.componentId);
    }

    _handleRequestItems() {
        this.setState({ loading: true });
        this.props.createRequest(
            this.state.items,
            async (message) => {
                this.setState({ loading: false });
                Navigation.popTo('conciergeService');
                await Navigation.push('conciergeService', conciergeTrackingProgress);
                Alert.alert('Success', message);
            },
            () => {
                this.setState({ loading: false });
            },
        );
    }

    _handleModalNoteServiceRequestItem(item: IServiceItem | null, closeModal?: boolean) {
        return () => {
            Keyboard.dismiss();
            if (this._modalNoteServiceRequestItem.current) {
                if (closeModal) {
                    this.setState(
                        (prevState) => ({
                            items: prevState.items.map((dish) => {
                                if (dish.service_id === prevState.selectedItem.service_id) {
                                    return {
                                        ...dish,
                                        note: prevState.selectedItem.note,
                                    };
                                }

                                return dish;
                            }),
                        }),
                        this._modalNoteServiceRequestItem.current.hide,
                    );
                } else {
                    const selected = find(this.state.items, { service_id: item ? item.id : 0 });
                    this.setState(
                        { selectedItem: selected ? selected : this.state.selectedItem },
                        this._modalNoteServiceRequestItem.current.show,
                    );
                }
            }
        };
    }

    _onChangeText(text: string) {
        this.setState((prevState) => ({
            selectedItem: {
                ...prevState.selectedItem,
                note: text,
            },
        }));
    }

    _addTotalService(item: IServiceItem) {
        const index = findIndex(this.state.items, { service_id: item.id });

        if (index < 0) {
            this.setState((prevState) => ({
                items: [
                    ...prevState.items,
                    {
                        service_id: item.id,
                        qty: 1,
                        note: '',
                        name: item.name,
                    },
                ],
            }));
        } else {
            this.setState((prevState) => ({
                items: prevState.items.map((service) => {
                    if (service.service_id === item.id) {
                        return {
                            ...service,
                            qty: service.qty + 1,
                        };
                    }

                    return service;
                }),
            }));
        }
    }

    _substractTotalService(item: IServiceItem) {
        const selected = find(this.state.items, { service_id: item.id });

        if (selected && selected.qty <= 1) {
            this.setState((prevState) => ({
                items: prevState.items.filter(({ service_id }) => service_id !== item.id),
            }));
        } else {
            this.setState((prevState) => ({
                items: prevState.items.map((service) => {
                    if (service.service_id === item.id) {
                        return {
                            ...service,
                            qty: service.qty - 1,
                        };
                    }

                    return service;
                }),
            }));
        }
    }

    _keyExtractor(item: IServiceItem) {
        return item.id.toString();
    }

    _renderListHeaderComponent() {
        return (
            <View style={styles.text_container}>
                <H4 fontSize={scale.w(18)} textAlign="center">
                    {this.props.selectedLanguage.request_items_to_be_sent_to_your_room}
                </H4>
            </View>
        );
    }

    _renderItemSeparatorComponent() {
        return <View style={{ height: scale.w(0) }} />;
    }

    _renderItem({ item }: { item: IServiceItem }) {
        const selected = find(this.state.items, { service_id: item.id });
        const { color } = this.props;

        return (
            <View
                style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    paddingHorizontal: scale.w(20),
                    justifyContent: 'center',
                    marginHorizontal: scale.w(15),
                    backgroundColor: 'white',
                    height: scale.w(74),
                    borderRadius: scale.w(20),
                    marginBottom: scale.h(20),
                    ...Platform.select({
                        ios: {
                            shadowColor: '#000',
                            shadowOffset: { width: 0, height: 4 },
                            shadowOpacity: 0.2,
                            shadowRadius: 3,
                        },
                        android: {
                            elevation: 8,
                        },
                    }),
                }}
            >
                <TouchableOpacity
                    style={{ flexDirection: 'row', justifyContent: 'space-between', flex: 0.7 }}
                    onPress={() => {
                        this.setState({
                            description: item.description,
                            title: item.name,
                            item: item,
                        });
                        this.setModalVisible(true);
                    }}
                >
                    <View style={{ flex: 1, flexDirection: 'column' }}>
                        <View>
                            <H4 fontSize={scale.w(20)}>{item.name}</H4>
                        </View>
                        <View>
                            <H4 fontSize={scale.w(12)}>
                                {item.description
                                    ? item.description.length > 42
                                        ? item.description.substring(0, 42) + '...'
                                        : item.description
                                    : null}
                            </H4>
                        </View>
                    </View>
                </TouchableOpacity>
                <View style={{ flex: 0.3, flexDirection: 'row', justifyContent: 'flex-end' }}>
                    {selected && (
                        <TouchableOpacity
                            onPress={debounce(this._handleModalNoteServiceRequestItem(item, false), 1000, {
                                leading: true,
                                trailing: false,
                            })}
                            activeOpacity={0.7}
                            style={{ flex: 0.4 }}
                        >
                            <View
                                style={{
                                    borderWidth: 1,
                                    borderRadius: scale.w(8),
                                    borderColor: color || colors.BROWN,
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    flex: 1,
                                }}
                            >
                                <Ionicons
                                    name={
                                        selected && selected.note !== ''
                                            ? 'md-checkmark-circle-outline'
                                            : 'md-create'
                                    }
                                    color={color || colors.BROWN}
                                    size={scale.w(18)}
                                />
                            </View>
                        </TouchableOpacity>
                    )}
                    <View style={{ width: 5 }} />

                    {selected ? (
                        <View
                            style={{
                                flexDirection: 'row',
                                borderWidth: 1,
                                borderRadius: scale.w(8),
                                borderColor: color || colors.BROWN,
                                paddingHorizontal: scale.w(10),
                                alignItems: 'center',
                                justifyContent: 'flex-end',
                                flex: 1,
                                width: '80%',
                                paddingVertical: hp(0.2),
                            }}
                        >
                            <TouchableOpacity
                                onPress={() => this._substractTotalService(item)}
                                activeOpacity={0.7}
                                style={{ flex: 0.3 }}
                            >
                                <Ionicons name="md-remove" color={color || colors.BROWN} size={scale.w(22)} />
                            </TouchableOpacity>
                            <View
                                style={{
                                    flex: 0.4,
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                }}
                            >
                                <H4>{selected ? selected.qty : 0}</H4>
                            </View>
                            <TouchableOpacity
                                onPress={() => this._addTotalService(item)}
                                activeOpacity={0.7}
                                style={{ flex: 0.3 }}
                            >
                                <Ionicons name="md-add" color={color || colors.BROWN} size={scale.w(22)} />
                            </TouchableOpacity>
                        </View>
                    ) : (
                        <TouchableOpacity
                            onPress={() => this._addTotalService(item)}
                            activeOpacity={0.7}
                            style={{
                                flex: 1,
                                width: '80%',
                                flexDirection: 'row',
                                paddingVertical: hp(0.2),
                            }}
                        >
                            <View
                                style={{
                                    flexDirection: 'row',
                                    borderWidth: 1,
                                    borderRadius: scale.w(8),
                                    borderColor: color || colors.BROWN,
                                    flex: 0.7,
                                    paddingHorizontal: scale.w(10),
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                }}
                            >
                                <View
                                    style={{
                                        flex: 1,
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                    }}
                                >
                                    <H4>{this.props.selectedLanguage.add}</H4>
                                </View>
                                <Ionicons name="md-add" color={color || colors.BROWN} size={scale.w(22)} />
                            </View>
                        </TouchableOpacity>
                    )}
                </View>
            </View>
        );
    }

    render() {
        const { color } = this.props;
        const { conceirge_service, request_items } = this.props.selectedLanguage;
        return (
            <View style={base.container}>
                <Navbar color={color} onClick={this._handleBack} title={conceirge_service} />
                <Modal
                    onBackdropPress={() => {
                        this.setModalVisible(false);
                    }}
                    onBackButtonPress={() => {
                        this.setModalVisible(false);
                    }}
                    animationType="slide"
                    animationInTiming={500}
                    backdropOpacity={0.7}
                    onSwipeComplete={() => this.setState({ modalVisible: false })}
                    isVisible={this.state.modalVisible}
                >
                    <View
                        style={{
                            flex: 1,
                            justifyContent: 'center',
                            paddingHorizontal: wp(2),
                            alignItems: 'center',
                        }}
                    >
                        <View
                            style={{
                                width: '90%',
                                backgroundColor: 'white',
                                borderRadius: 50,
                                alignItems: 'center',
                                justifyContent: 'center',
                                paddingHorizontal: wp(3),
                            }}
                        >
                            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                <View style={{ height: hp(4) }} />

                                <H3 fontSize={scale.w(20)}>{this.state.title}</H3>
                                <View style={{ height: hp(4) }} />
                                {this.state.description && (
                                    <H4 fontSize={scale.w(12)}>{this.state.description}</H4>
                                )}

                                {this.state.description && <View style={{ height: hp(4) }} />}
                            </View>
                            <TouchableOpacity
                                onPress={() => {
                                    this._addTotalService(this.state.item);
                                    this.setModalVisible(false);
                                }}
                                style={{
                                    borderRadius: 100,
                                    height: hp(6),
                                    width: wp(40),
                                    backgroundColor: color,
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                }}
                            >
                                <Text
                                    style={{
                                        color: colors.WHITE,
                                        fontFamily: 'Roboto-Bold',
                                        fontSize: scale.w(14),
                                    }}
                                >
                                    {this.props.selectedLanguage.add_to_order}
                                </Text>
                            </TouchableOpacity>
                            <View style={{ height: hp(4) }} />
                        </View>
                    </View>
                    <View
                        style={{
                            position: 'absolute',
                            height: '100%',
                            alignSelf: 'flex-start',
                            paddingHorizontal: wp(1.2),
                        }}
                    >
                        <TouchableOpacity
                            onPress={() => {
                                this.setModalVisible(false);
                            }}
                        >
                            <Image
                                source={require('../../images/icon_back.png')}
                                style={{ width: scale.w(30), height: scale.w(30) }}
                                resizeMode={'contain'}
                            ></Image>
                        </TouchableOpacity>
                    </View>
                </Modal>
                {/* //////////////////////////// */}
                <View style={{ flex: 1 }}>
                    <FlatList
                        refreshControl={
                            <RefreshControl onRefresh={this._fetch} refreshing={this.state.loadingGet} />
                        }
                        data={this.props.serviceItems}
                        extraData={this.state}
                        keyExtractor={this._keyExtractor}
                        ListHeaderComponent={this._renderListHeaderComponent}
                        ItemSeparatorComponent={this._renderItemSeparatorComponent}
                        renderItem={this._renderItem}
                        initialNumToRender={10}
                    />

                    <Animatable.View
                        useNativeDriver
                        animation="fadeIn"
                        duration={300}
                        style={{ paddingHorizontal: scale.w(57), paddingTop: scale.w(10) }}
                    >
                        <ButtonPrimary
                            onPress={this._handleRequestItems}
                            text={request_items}
                            loading={this.state.loading}
                            disabled={this.state.loading}
                            backgroundColor={color}
                        />
                    </Animatable.View>
                    <SafeAreaView style={{ height: 20 }} />
                </View>

                <CustomModal
                    ref={this._modalNoteServiceRequestItem}
                    animationIn="fadeInUp"
                    animationOut="fadeOutDown"
                >
                    <NoteServiceRequestItem
                        value={this.state.selectedItem.note}
                        onChangeText={this._onChangeText}
                        showModal={this._handleModalNoteServiceRequestItem(null, true)}
                        title={`${this.state.selectedItem.name} Note`}
                        color={color}
                    />
                </CustomModal>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    text_container: {
        marginTop: scale.w(10),
        marginBottom: scale.w(40),
        marginHorizontal: scale.w(28),
    },
});

export default RequestItems;
