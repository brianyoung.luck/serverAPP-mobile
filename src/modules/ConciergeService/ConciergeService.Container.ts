import { connect } from 'react-redux';
import ConciergeService from './ConciergeService';
import { bindActionCreators, Dispatch } from 'redux';
import { IState } from '../../types/state';
import { roomCleaningService } from '../../actions/action.cleaningService';
import { getLaundryServicesMenu } from '../../actions/action.cleaningService';
const mapStateToProps = (state: IState) => {
    console.log('concergeee stateee', state);
    return {
        countUnreadMessage: state.chat.unreadMessage,
        color: state.hotel.icon.concierge_color,
        selectedLanguage: state.language,
    };
};

const mapDispatchToProps = (dispatch: Dispatch) => {
    return bindActionCreators(
        {
            getLaundryServicesMenu,
            roomCleaningService,
        },
        dispatch,
    );
};

export interface IConciergeServiceReduxProps
    extends ReturnType<typeof mapStateToProps>,
        ReturnType<typeof mapDispatchToProps> {}

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(ConciergeService);
