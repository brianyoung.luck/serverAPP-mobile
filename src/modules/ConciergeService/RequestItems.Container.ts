import { connect } from 'react-redux';
import RequestItems from './RequestItems';
import { bindActionCreators, Dispatch } from 'redux';
import { IState } from '../../types/state';
import { getConciergeServiceItems, createRequest } from '../../actions/action.conciergeService';

const mapStateToProps = (state: IState) => ({
    serviceItems: state.conciergeService.serviceItems,
    color: state.hotel.icon.concierge_color,
    selectedLanguage: state.language,
});

const mapDispatchToProps = (dispatch: Dispatch) => {
    return bindActionCreators(
        {
            getConciergeServiceItems,
            createRequest,
        },
        dispatch,
    );
};

export interface IRequestItemsReduxProps
    extends ReturnType<typeof mapStateToProps>,
        ReturnType<typeof mapDispatchToProps> {}

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(RequestItems);
