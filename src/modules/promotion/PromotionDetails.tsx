import React from 'react';
import {
    View,
    StyleSheet,
    Image,
    ScrollView,
    Linking,
    TouchableOpacity,
    ActivityIndicator,
    Platform,
    SafeAreaView,
    Dimensions,
} from 'react-native';
import base from '../../utils/baseStyles';
import { widthPercentageToDP as wp, heightPercentageToDP as hp, scale } from '../../utils/dimensions';
import Navbar from '../_global/Navbar';
import { Navigation } from 'react-native-navigation';
import Carousel from 'react-native-snap-carousel';
import { screenWidth } from '../../utils/dimensions';
import { IPromotionDetails } from '../../types/promotion';
import { IPromotionDetailsReduxProps } from './PromotionDetails.Container';
import { H1, ButtonLabel, H4 } from '../_global/Text';
import { ButtonPrimary } from '../_global/Button';
import Lightbox from 'react-native-lightbox';
import ImageZoom from 'react-native-image-pan-zoom';
import Modal from 'react-native-modal';

export interface IPromotionDetailsProps extends IPromotionDetailsReduxProps {
    componentId: string;
    promotionDetails: IPromotionDetails;
    idButton: number;
    promotionTitle: string;
}

interface IPromotionDetailsState {
    modalVisible: boolean;
    imageUrl: String;
}

class PromotionDetails extends React.Component<IPromotionDetailsProps, IPromotionDetailsState> {
    constructor(props: IPromotionDetailsProps) {
        super(props);

        this.state = {
            modalVisible: false,
            imageUrl: '',
        };

        Navigation.mergeOptions(props.componentId, {
            statusBar: {
                backgroundColor: props.color,
                style: 'light',
            },
        });
        this.getData();
        this._handleBack = this._handleBack.bind(this);
        // this._handleReserveSpa = this._handleReserveSpa.bind(this);
        // this._handleOrderRoomService = this._handleOrderRoomService.bind(this);
    }
    async getData() {
        this.props.emptyPromotionDetails({ images: [] });
        try {
            await this.props.getPromotionDetails(this.props.idHotel, this.props.idButton);
        } catch (e) {
            console.warn(e);
        }
    }
    setModalVisible(visible) {
        this.setState({ modalVisible: visible });
    }
    componentDidMount() {
        this.props.getPromotionDetails(this.props.idHotel, this.props.idButton);
    }

    _handleBack() {
        Navigation.pop(this.props.componentId);
    }

    _handleRedeem(redeem_link: any) {
        if (redeem_link.includes('https://www.')) {
            Linking.openURL(redeem_link.toString());
        } else if (redeem_link.indexOf('www') == 0) {
            Linking.openURL('https://' + redeem_link.toString());
        } else {
            Linking.openURL('https://www.' + redeem_link.toString());
        }
    }

    render() {
        const { promotionDetails, color, promotionTitle } = this.props;
        var promotionDetailsArr: any = [];
        if (promotionDetails.PromotionDetails != null && promotionDetails.PromotionDetails != undefined)
            promotionDetailsArr = Object.values(promotionDetails.PromotionDetails);
        var images;
        var imgs;
        if (promotionDetails.PromotionDetails != null && promotionDetails.PromotionDetails != undefined) {
            images = JSON.stringify(promotionDetails.PromotionDetails.images);
            imgs = JSON.parse(images);
        }
        console.log('promotionDetailsArr == ', promotionDetailsArr);
        // let imgs = [
        //     {
        //         title: 'Beautiful and dramatic Antelope Canyon',
        //         subtitle: 'Lorem ipsum dolor sit amet et nuncat mergitur',
        //         path: 'https://i.imgur.com/UYiroysl.jpg'
        //     },
        //     {
        //         title: 'Earlier this morning, NYC',
        //         subtitle: 'Lorem ipsum dolor sit amet',
        //         path: 'https://i.imgur.com/UPrs1EWl.jpg'
        //     },
        //     {
        //         title: 'White Pocket Sunset',
        //         subtitle: 'Lorem ipsum dolor sit amet et nuncat ',
        //         path: 'https://i.imgur.com/MABUbpDl.jpg'
        //     }
        // ]

        if (promotionDetailsArr.length == 0 || promotionDetailsArr == undefined) {
            return null;
        } else {
            return (
                <View style={base.container}>
                    <Modal
                        onBackdropPress={() => {
                            this.setModalVisible(false);
                        }}
                        isVisible={this.state.modalVisible}
                        animationType="slide"
                        animationInTiming={500}
                        style={styles.modal}
                    >
                        <View style={{ flex: 1 }}>
                            <View style={styles.modalContainer}>
                                <View
                                    style={{
                                        flexDirection: 'row',
                                        justifyContent: 'center',
                                        height: '100%',
                                    }}
                                >
                                    <View
                                        style={{
                                            justifyContent: 'center',
                                            // height: 450,
                                            alignSelf: 'center',
                                        }}
                                    >
                                        <ImageZoom
                                            cropWidth={wp(100)}
                                            cropHeight={hp(100)}
                                            imageWidth={wp(100)}
                                            imageHeight={hp(100)}
                                        >
                                            <Image
                                                resizeMode="contain"
                                                source={{ uri: this.state.imageUrl }}
                                                style={styles.image}
                                            ></Image>
                                        </ImageZoom>
                                    </View>
                                </View>
                            </View>
                            <View
                                style={{
                                    position: 'absolute',
                                    height: '100%',
                                    alignSelf: 'flex-start',
                                    paddingHorizontal: wp(1.2),
                                }}
                            >
                                <TouchableOpacity onPress={() => this.setModalVisible(false)}>
                                    <Image
                                        source={require('../../images/icon_back.png')}
                                        style={{ width: scale.w(30), height: scale.w(30) }}
                                        resizeMode={'contain'}
                                    ></Image>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </Modal>
                    <Navbar
                        isViolet={color === ''}
                        color={color}
                        onClick={this._handleBack}
                        title={promotionTitle}
                    />
                    <ScrollView>
                        {promotionDetailsArr[0].type == 'Promotion' ? (
                            <View style={{ height: hp('30%') }}>
                                <Carousel
                                    data={promotionDetails.PromotionDetails.images}
                                    renderItem={({ item }) => (
                                        <View style={{ flex: 1, justifyContent: 'center', height: 200 }}>
                                            {/* <Lightbox springConfig={{ tension: 15, friction: 7 }} swipeToDismiss={false}
                                        activeProps={{ width: '100%', height: '100%' }}> */}
                                            <TouchableOpacity
                                                onPress={() => {
                                                    this.setState({
                                                        imageUrl: item.path,
                                                    });
                                                    this.setModalVisible(true);
                                                }}
                                            >
                                                <Image
                                                    source={{ uri: item.path }}
                                                    style={{
                                                        width: '100%',
                                                        height: hp('25%'),
                                                        resizeMode: 'contain',
                                                        marginTop: scale.w(20),
                                                    }}
                                                />
                                            </TouchableOpacity>
                                            {/* </Lightbox> */}
                                        </View>
                                    )}
                                    sliderWidth={screenWidth}
                                    itemWidth={screenWidth}
                                    sliderHeight={200}
                                    autoplay
                                    loop
                                    // style={{height: scale.w(200)}}
                                />
                            </View>
                        ) : (
                            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                <Carousel
                                    data={promotionDetails.PromotionDetails.images}
                                    renderItem={({ item }) => (
                                        <View style={{}}>
                                            {/* <Lightbox springConfig={{ tension: 15, friction: 7 }} swipeToDismiss={false}
                                                activeProps={{ width: '100%', height: '100%' }}> */}
                                            {/* <ImageZoom cropWidth={wp(100)}
                                                    cropHeight={hp(40)}
                                                    imageWidth={wp(100)}
                                                    imageHeight={hp(40)}> */}
                                            <TouchableOpacity
                                                onPress={() => {
                                                    this.setState({
                                                        imageUrl: item.path,
                                                    });
                                                    this.setModalVisible(true);
                                                }}
                                            >
                                                <Image
                                                    source={{ uri: item.path }}
                                                    style={{
                                                        width: '100%',
                                                        height: hp('40%'),
                                                        resizeMode: 'contain',
                                                        alignSelf: 'center',
                                                        marginTop:
                                                            promotionDetailsArr[0].data == ''
                                                                ? scale.w(100)
                                                                : scale.w(20),
                                                        // marginTop: scale.w(70),
                                                    }}
                                                />
                                            </TouchableOpacity>
                                            {/* </ImageZoom> */}
                                            {/* </Lightbox> */}
                                        </View>
                                    )}
                                    sliderWidth={screenWidth}
                                    itemWidth={screenWidth}
                                    sliderHeight={200}
                                    autoplay
                                    loop
                                    // style={{height: scale.w(200)}}
                                />
                            </View>
                        )}
                        <View style={{ height: hp('5%') }}></View>
                        {promotionDetailsArr[0].data != '' && (
                            <View>
                                {promotionDetailsArr &&
                                    promotionDetailsArr.map((data: any, i: any) => {
                                        if (i != promotionDetailsArr.length - 1 && data.data != '') {
                                            let obj = JSON.parse(data.data);

                                            // return (i == (promotionDetailsArr.length - 1)) ? (null) : (
                                            return (
                                                <View
                                                    style={{ alignItems: 'center', justifyContent: 'center' }}
                                                >
                                                    <View style={{}}>
                                                        {(obj.sub_title || obj.sub_title == '') && (
                                                            <View>
                                                                <H1
                                                                    textAlign="center"
                                                                    fontSize={scale.w(20)}
                                                                    color={color}
                                                                >
                                                                    {obj.sub_title}
                                                                </H1>
                                                            </View>
                                                        )}
                                                        {obj.text && (
                                                            <View>
                                                                <ButtonLabel
                                                                    textAlign="center"
                                                                    fontSize={scale.w(20)}
                                                                    color={color}
                                                                >
                                                                    {obj.text}
                                                                </ButtonLabel>
                                                            </View>
                                                        )}
                                                        {obj.promotion_amount != undefined &&
                                                            obj.promotion_amount != null &&
                                                            obj.promotion_amount != '' && (
                                                                <View>
                                                                    <H1
                                                                        textAlign="center"
                                                                        fontSize={scale.w(24)}
                                                                        color={color}
                                                                    >
                                                                        {
                                                                            this.props.selectedLanguage
                                                                                .promotion_amount
                                                                        }
                                                                    </H1>
                                                                    <ButtonLabel
                                                                        textAlign="center"
                                                                        fontSize={scale.w(24)}
                                                                        color={color}
                                                                    >
                                                                        {'( '}
                                                                        {obj.promotion_amount}
                                                                        {' )'}
                                                                    </ButtonLabel>
                                                                </View>
                                                            )}
                                                        {obj.promotion_description != undefined &&
                                                            obj.promotion_description != null &&
                                                            obj.promotion_description != '' && (
                                                                <View>
                                                                    <H4
                                                                        textAlign="center"
                                                                        fontSize={scale.w(16)}
                                                                        color={'#888'}
                                                                    >
                                                                        {obj.promotion_description}
                                                                    </H4>
                                                                </View>
                                                            )}
                                                    </View>
                                                    {/* <View style={{ height: hp('15%') }}></View> */}

                                                    {obj.promotion_code != undefined &&
                                                        obj.promotion_code != null &&
                                                        obj.promotion_code != '' && (
                                                            <View>
                                                                <H1
                                                                    textAlign="center"
                                                                    fontSize={scale.w(24)}
                                                                    color={color}
                                                                >
                                                                    {this.props.selectedLanguage.use_code}
                                                                </H1>
                                                                <H1
                                                                    textAlign="center"
                                                                    fontSize={scale.w(24)}
                                                                    color={color}
                                                                >
                                                                    {obj.promotion_code}
                                                                </H1>
                                                            </View>
                                                        )}
                                                    {obj.redeem_link != undefined &&
                                                        obj.redeem_link != null &&
                                                        obj.redeem_link != '' && (
                                                            <View style={styles.btn_primary_container}>
                                                                <TouchableOpacity
                                                                    style={{
                                                                        height: hp('10%'),
                                                                        alignItems: 'center',
                                                                        justifyContent: 'center',
                                                                    }}
                                                                    onPress={() => {
                                                                        this._handleRedeem(obj.redeem_link);
                                                                    }}
                                                                    activeOpacity={1}
                                                                >
                                                                    <View
                                                                        style={StyleSheet.flatten([
                                                                            styles.btn_container,
                                                                            {
                                                                                height: wp(13),
                                                                                width: wp(60),
                                                                                borderRadius: 5,
                                                                                backgroundColor: color,
                                                                            },

                                                                            Platform.select({
                                                                                ios: {
                                                                                    shadowColor: '#000',
                                                                                    shadowOffset: {
                                                                                        width: 0,
                                                                                        height: 4,
                                                                                    },
                                                                                    shadowOpacity: 0.2,
                                                                                    shadowRadius: 3,
                                                                                },
                                                                                android: {
                                                                                    elevation: 8,
                                                                                },
                                                                            }),
                                                                        ])}
                                                                    >
                                                                        <H4
                                                                            textAlign="center"
                                                                            fontSize={scale.w(20)}
                                                                            color="white"
                                                                        >
                                                                            {
                                                                                this.props.selectedLanguage
                                                                                    .click_here_to_redeem
                                                                            }
                                                                        </H4>
                                                                    </View>
                                                                </TouchableOpacity>
                                                                <SafeAreaView />
                                                            </View>
                                                        )}
                                                </View>
                                            );
                                            // )
                                        }
                                    })}
                            </View>
                        )}
                    </ScrollView>
                </View>
            );
        }
    }
}

const styles = StyleSheet.create({
    text_container: {
        marginTop: scale.w(10),
        marginBottom: scale.w(20),
        marginHorizontal: scale.w(28),
    },
    image_container: {
        alignItems: 'center',
        marginTop: scale.w(60),
        marginBottom: scale.w(68),
    },
    menu_btn_container: {
        // paddingHorizontal: scale.w(10),
        marginBottom: scale.w(10),
        alignItems: 'flex-start',
    },
    logo: {
        width: '100%',
        height: scale.w(200),
        resizeMode: 'contain',
        marginTop: scale.w(20),
    },
    btn_primary_container: {
        paddingHorizontal: scale.w(80),
        paddingTop: scale.w(8),
        paddingBottom: scale.w(38),
        alignSelf: 'flex-end',
        // position:'relative'
    },
    btn_container: {
        height: scale.w(56),
        alignItems: 'center',
        justifyContent: 'center',
    },
    modalContainer: {
        height: '100%',
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        marginHorizontal: 10,
    },
    modal: {
        height: '100%',
        marginLeft: -1,
        paddingVertical: 20,
        marginBottom: -1,
    },
    image: {
        alignContent: 'center',
        width: '80%',
        height: hp('100%'),
        resizeMode: 'contain',
        alignSelf: 'center',
        // position: 'relative',
    },
});

export default PromotionDetails;
