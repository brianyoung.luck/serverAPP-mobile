import React from 'react';
import {
    View,
    StyleSheet,
    ScrollView,
    TouchableOpacity,
    Image,
    FlatList,
    Keyboard,
    SafeAreaView,
    Text,
    Platform,
} from 'react-native';
import base from '../../utils/baseStyles';
import { scale, widthPercentageToDP as wp, heightPercentageToDP as hp } from '../../utils/dimensions';
import Navbar from '../_global/Navbar';
import { Navigation } from 'react-native-navigation';
import * as Animatable from 'react-native-animatable';
import { orderRoomService, cleaningRequestComplete } from '../../utils/navigationControl';
import MenuButton from '../_global/MenuButton';
import { ButtonPrimary } from '../_global/Button';
import { H4, H3 } from '../_global/Text';
import { ILaundryServiceReduxProps, IDataLaundry } from './LaundryService.Container';
import { find, findIndex } from 'lodash';
import colors from '../../constants/colors';
import { debounce } from 'lodash';
import Ionicons from 'react-native-vector-icons/Ionicons';
import CustomModal from '../_global/CustomModal';
import NoteOrderItem from '../Restaurant/Components/NoteOrderItem';
import Modal from 'react-native-modal';

export interface ILaundryServiceProps extends ILaundryServiceReduxProps {
    componentId: string;
}

interface ILaundryServiceState {
    loading: boolean;
    items: any;
    selectedItem: any;
    modalVisible: boolean;
    title: string;
    description: string;
    item: any;
}

class LaundryService extends React.Component<ILaundryServiceProps, ILaundryServiceState> {
    private _modalNoteOrderItem = React.createRef<CustomModal>();

    constructor(props: ILaundryServiceProps) {
        super(props);

        this.state = {
            loading: false,
            items: [],
            selectedItem: {
                qty: 0,
                notes: '',
                name: '',
                laundry_service_id: '',
                modalVisible: false,
                title: '',
                description: '',
                item: [],
            },
        };

        Navigation.mergeOptions(props.componentId, {
            statusBar: {
                backgroundColor: props.color,
                style: 'light',
            },
        });
        this.getData();
        this._handleBack = this._handleBack.bind(this);
        this._handleSelectService = this._handleSelectService.bind(this);
        this._renderListHeaderComponent = this._renderListHeaderComponent.bind(this);
        this._renderItemSeparatorComponent = this._renderItemSeparatorComponent.bind(this);
        this._renderItem = this._renderItem.bind(this);
        this._handleModalNoteOrderItem = this._handleModalNoteOrderItem.bind(this);
        this._onChangeText = this._onChangeText.bind(this);
        this._addTotalDish = this._addTotalDish.bind(this);
        this._substractTotalDish = this._substractTotalDish.bind(this);
        this._keyExtractor = this._keyExtractor.bind(this);
    }
    setModalVisible(visible) {
        this.setState({ modalVisible: visible });
    }
    async getData() {
        try {
            this.props.getLaundryServicesMenu();
        } catch (e) {
            console.warn(e);
        }
    }
    _handleBack() {
        Navigation.pop(this.props.componentId);
    }

    componentDidMount() {
        // this.props.getLaundryServicesMenu();
    }
    _handleSelectService(item: IDataLaundry) {
        this.setState({ loading: true });
        const { items } = this.state;
        this.props.laundryOrder(
            items,
            async () => {
                await Navigation.push(
                    this.props.componentId,
                    cleaningRequestComplete({ isRoomCleaning: false }),
                );
                this.setState({ loading: false });
            },
            () => {
                this.setState({ loading: false });
            },
        );
    }

    _renderListHeaderComponent() {
        return (
            <View style={styles.text_container}>
                <H4 fontSize={scale.w(21)} textAlign="center">
                    {this.props.selectedLanguage.choose_a_valet_laundary_service + ':'}
                </H4>
            </View>
        );
    }

    _renderItemSeparatorComponent() {
        return <View style={{ height: scale.w(0) }} />;
    }
    _handleModalNoteOrderItem(item: any | null, closeModal?: boolean) {
        return () => {
            Keyboard.dismiss();
            if (this._modalNoteOrderItem.current) {
                if (closeModal) {
                    this.setState(
                        (prevState) => ({
                            items: prevState.items.map((item: any) => {
                                if (item.laundry_service_id === prevState.selectedItem.laundry_service_id) {
                                    return {
                                        ...item,
                                        notes: prevState.selectedItem.notes,
                                    };
                                }

                                return item;
                            }),
                        }),
                        this._modalNoteOrderItem.current.hide,
                    );
                } else {
                    const selected = find(this.state.items, { laundry_service_id: item ? item.id : '' });
                    this.setState(
                        { selectedItem: selected ? selected : this.state.selectedItem },
                        this._modalNoteOrderItem.current.show,
                    );
                }
            }
        };
    }
    _onChangeText(text: string) {
        this.setState((prevState) => ({
            selectedItem: {
                ...prevState.selectedItem,
                notes: text,
            },
        }));
    }
    _addTotalDish(item: any) {
        const index = findIndex(this.state.items, { laundry_service_id: item.id });

        if (index < 0) {
            this.setState((prevState) => ({
                items: [
                    ...prevState.items,
                    {
                        qty: 1,
                        notes: '',
                        name: item.name,
                        laundry_service_id: item.id,
                    },
                ],
            }));
        } else {
            this.setState((prevState) => ({
                items: prevState.items.map((i: any) => {
                    if (i.laundry_service_id === item.id) {
                        return {
                            ...i,
                            qty: i.qty + 1,
                        };
                    }

                    return i;
                }),
            }));
        }
    }

    _substractTotalDish(item: any) {
        const selected = find(this.state.items, { laundry_service_id: item.id });

        if (selected && selected.qty <= 1) {
            this.setState((prevState) => ({
                items: prevState.items.filter(({ laundry_service_id }) => laundry_service_id !== item.id),
            }));
        } else {
            this.setState((prevState) => ({
                items: prevState.items.map((i: any) => {
                    if (i.laundry_service_id === item.id) {
                        return {
                            ...i,
                            qty: i.qty - 1,
                        };
                    }

                    return i;
                }),
            }));
        }
    }

    _keyExtractor(item: any, index: number) {
        return `${item.name}_${index}`;
    }
    _renderItem({ item }: { item: IDataLaundry }) {
        const selected = find(this.state.items, { laundry_service_id: item.id });
        const { color, currency } = this.props;

        return (
            // <View

            // >
            <View style={styles.item_container}>
                <TouchableOpacity
                    style={{ width: scale.w(210) }}
                    onPress={() => {
                        this.setState({
                            description: item.description,
                            title: item.name,
                            item: item,
                        });
                        this.setModalVisible(true);
                    }}
                >
                    <View style={{ paddingLeft: scale.w(10) }}>
                        <View>
                            <H4 fontSize={scale.w(20)}>{item.name}</H4>
                        </View>
                        <View style={{}}>
                            <H4 fontSize={scale.w(12)}>
                                {item.description
                                    ? item.description.length > 30
                                        ? item.description.substring(0, 30) + '...'
                                        : item.description
                                    : null}
                            </H4>
                        </View>
                        <View>
                            <H4 fontSize={scale.w(16)}>
                                {currency}
                                {item.price}
                            </H4>
                        </View>
                    </View>
                </TouchableOpacity>
                {/* <Image
                        source={require('../../images/icon_next.png')}
                        style={{ width: scale.w(30), height: scale.w(30) }}
                        resizeMode="contain"
                    /> */}

                <View style={{ width: scale.w(90), flexDirection: 'row' }}>
                    {selected && (
                        <TouchableOpacity
                            onPress={debounce(this._handleModalNoteOrderItem(item, false), 1000, {
                                leading: true,
                                trailing: false,
                            })}
                            activeOpacity={0.7}
                            style={{ flex: 0.4 }}
                        >
                            <View
                                style={{
                                    borderWidth: 1,
                                    borderRadius: scale.w(8),
                                    borderColor: color || colors.BROWN,
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    flex: 1,
                                }}
                            >
                                <Ionicons
                                    name={
                                        selected && selected.notes !== ''
                                            ? 'md-checkmark-circle-outline'
                                            : 'md-create'
                                    }
                                    color={color || colors.BROWN}
                                    size={scale.w(18)}
                                />
                            </View>
                        </TouchableOpacity>
                    )}
                    <View style={{ width: 5 }} />

                    {selected ? (
                        <View
                            style={{
                                flexDirection: 'row',
                                borderWidth: 1,
                                borderRadius: scale.w(8),
                                borderColor: color || colors.BROWN,
                                paddingHorizontal: scale.w(10),
                                alignItems: 'center',
                                justifyContent: 'center',
                                flex: 1,
                                width: '80%',
                                paddingVertical: hp(0.2),
                            }}
                        >
                            <TouchableOpacity
                                onPress={() => this._substractTotalDish(item)}
                                activeOpacity={0.7}
                                style={{ flex: 0.3 }}
                            >
                                <Ionicons name="md-remove" color={color || colors.BROWN} size={scale.w(22)} />
                            </TouchableOpacity>
                            <View
                                style={{
                                    flex: 0.4,
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                }}
                            >
                                <H4>{selected ? selected.qty : 0}</H4>
                            </View>
                            <TouchableOpacity
                                onPress={() => this._addTotalDish(item)}
                                activeOpacity={0.7}
                                style={{ flex: 0.3 }}
                            >
                                <Ionicons name="md-add" color={color || colors.BROWN} size={scale.w(22)} />
                            </TouchableOpacity>
                        </View>
                    ) : (
                        <TouchableOpacity
                            onPress={() => this._addTotalDish(item)}
                            activeOpacity={0.7}
                            style={{
                                flex: 1,
                                width: '80%',
                                flexDirection: 'row',
                                paddingVertical: hp(0.2),
                            }}
                        >
                            <View style={{ flex: 0.3 }}></View>
                            <View
                                style={{
                                    flexDirection: 'row',
                                    borderWidth: 1,
                                    borderRadius: scale.w(8),
                                    borderColor: color || colors.BROWN,
                                    flex: 0.7,
                                    paddingHorizontal: scale.w(10),
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                }}
                            >
                                <View
                                    style={{
                                        flex: 1,
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                    }}
                                >
                                    <H4>{this.props.selectedLanguage.add}</H4>
                                </View>
                                <Ionicons name="md-add" color={color || colors.BROWN} size={scale.w(22)} />
                            </View>
                        </TouchableOpacity>
                    )}
                </View>
            </View>
        );
    }

    render() {
        const { color } = this.props;
        const { laundry_service, confirm_order, note } = this.props.selectedLanguage;
        console.log('concergeee screen propsss', this.props);
        return (
            <View style={base.container}>
                <Navbar color={this.props.color} onClick={this._handleBack} title={laundry_service} />
                <Modal
                    onBackdropPress={() => {
                        this.setModalVisible(false);
                    }}
                    onBackButtonPress={() => {
                        this.setModalVisible(false);
                    }}
                    animationType="slide"
                    animationInTiming={500}
                    backdropOpacity={0.7}
                    onSwipeComplete={() => this.setState({ modalVisible: false })}
                    isVisible={this.state.modalVisible}
                >
                    <View
                        style={{
                            flex: 1,
                            justifyContent: 'center',
                            paddingHorizontal: wp(2),
                            alignItems: 'center',
                        }}
                    >
                        <View
                            style={{
                                width: '90%',
                                backgroundColor: 'white',
                                borderRadius: 50,
                                alignItems: 'center',
                                justifyContent: 'center',
                                paddingHorizontal: wp(3),
                            }}
                        >
                            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                <View style={{ height: hp(4) }} />

                                <H3 fontSize={scale.w(20)}>{this.state.title}</H3>
                                <View style={{ height: hp(4) }} />
                                {this.state.description && (
                                    <H4 fontSize={scale.w(12)}>{this.state.description}</H4>
                                )}

                                {this.state.description && <View style={{ height: hp(4) }} />}
                            </View>
                            <TouchableOpacity
                                onPress={() => {
                                    this._addTotalDish(this.state.item);
                                    this.setModalVisible(false);
                                }}
                                style={{
                                    borderRadius: 100,
                                    height: hp(6),
                                    width: wp(40),
                                    backgroundColor: color,
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                }}
                            >
                                <Text
                                    style={{
                                        color: colors.WHITE,
                                        fontFamily: 'Roboto-Bold',
                                        fontSize: scale.w(14),
                                    }}
                                >
                                    {this.props.selectedLanguage.add_to_order}
                                </Text>
                            </TouchableOpacity>
                            <View style={{ height: hp(4) }} />
                        </View>
                    </View>
                    <View
                        style={{
                            position: 'absolute',
                            height: '100%',
                            alignSelf: 'flex-start',
                            paddingHorizontal: wp(1.2),
                        }}
                    >
                        <TouchableOpacity
                            onPress={() => {
                                this.setModalVisible(false);
                            }}
                        >
                            <Image
                                source={require('../../images/icon_back.png')}
                                style={{ width: scale.w(30), height: scale.w(30) }}
                                resizeMode={'contain'}
                            ></Image>
                        </TouchableOpacity>
                    </View>
                </Modal>
                {/* //////////////////////////// */}
                <View style={{ flex: 1 }}>
                    <FlatList
                        data={this.props.data}
                        extraData={this.state}
                        keyExtractor={(item) => item.id.toString()}
                        initialNumToRender={5}
                        ListHeaderComponent={this._renderListHeaderComponent}
                        ItemSeparatorComponent={this._renderItemSeparatorComponent}
                        renderItem={this._renderItem}
                    />
                    <Animatable.View
                        useNativeDriver
                        animation="fadeIn"
                        duration={300}
                        style={styles.submit_btn_container}
                    >
                        <ButtonPrimary
                            onPress={this._handleSelectService}
                            loading={this.state.loading}
                            disabled={this.state.loading}
                            text={confirm_order}
                            backgroundColor={color || colors.BROWN}
                        />
                    </Animatable.View>
                    <SafeAreaView style={{ marginBottom: wp(4) }} />
                </View>
                <CustomModal ref={this._modalNoteOrderItem} animationIn="fadeInUp" animationOut="fadeOutDown">
                    <NoteOrderItem
                        value={this.state.selectedItem.notes}
                        onChangeText={this._onChangeText}
                        showModal={this._handleModalNoteOrderItem(null, true)}
                        title={`${this.state.selectedItem.name} ${note}`}
                        color={color}
                    />
                </CustomModal>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    text_container: {
        paddingHorizontal: scale.w(20),
        marginTop: scale.w(14),
        marginBottom: scale.w(70),
    },
    item_container: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: scale.w(10),
        marginBottom: scale.h(20),
        backgroundColor: 'white',
        marginHorizontal: scale.w(15),
        height: scale.h(99),
        borderRadius: scale.w(20),
        ...Platform.select({
            ios: {
                shadowColor: '#000',
                shadowOffset: { width: 0, height: 4 },
                shadowOpacity: 0.2,
                shadowRadius: 3,
            },
            android: {
                elevation: 8,
            },
        }),
    },
    menu_btn_container: {
        paddingHorizontal: scale.w(55),
        marginBottom: scale.w(20),
        alignItems: 'center',
    },
    submit_btn_container: {
        paddingHorizontal: scale.w(57),
        marginTop: scale.w(18),
    },
});

export default LaundryService;
