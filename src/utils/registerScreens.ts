import { Provider } from 'react-redux';
import { Navigation } from 'react-native-navigation';
import { Store } from 'redux';
import SplashScreen from '../modules/SplashScreen/SplashScreen.Container';
import PickHotel from '../modules/PickHotel/PickHotel.Container';
import MainMenu from '../modules/MainMenu/MainMenu.Container';
import CheckIn from '../modules/CheckIn/CheckIn.Container';
import CheckOut from '../modules/CheckOut/CheckOut.Container';
import Chat from '../modules/Chat/Chat.Container';
import InAppNotification from '../modules/_global/InAppNotification';
import ConciergeService from '../modules/ConciergeService/ConciergeService.Container';
import RequestItems from '../modules/ConciergeService/RequestItems.Container';
import RestaurantList from '../modules/Restaurant/RestaurantList.Container';
import RestaurantService from '../modules/Restaurant/RestaurantService';
import BookATable from '../modules/Restaurant/BookATable.Container';
import OrderRoomService from '../modules/Restaurant/OrderRoomService.Container';
import TrackingProgress from '../modules/Restaurant/TrackingProgres.Container';
import SpaService from '../modules/Spa/SpaService.Container';
import SpaBookingTime from '../modules/Spa/SpaBookingTime.Container';
import SpaTreatmentList from '../modules/Spa/SpaTreatmentList.Container';
import CleaningService from '../modules/CleaningService/CleaningService.Container';
import CleaningRequestComplete from '../modules/CleaningService/CleaningRequestComplete.Container';
import LaundryService from '../modules/CleaningService/LaundryService.Container';
import PromotionService from '../modules/promotion/PromotionService.Container';
import ExperienceService from '../modules/Experience/Experience.Container';
import HotelMap from '../modules/Experience/HotelMap.Container';
import PromotionDetails from '../modules/promotion/PromotionDetails.Container';
import { gestureHandlerRootHOC } from 'react-native-gesture-handler';
import { Alert, AppRegistry, Platform } from 'react-native';
import SpaTrackingProgress from '../modules/Spa/SpaTrackingProgres.Container';
import ConciergeTrackingProgress from '../modules/ConciergeService/ConciergeTrackingProgres.Container';

export default (store: Store) => {
    Navigation.registerComponentWithRedux('SplashScreen', () => SplashScreen, Provider, store);
    Navigation.registerComponentWithRedux('PickHotel', () => PickHotel, Provider, store);
    Navigation.registerComponentWithRedux('MainMenu', () => MainMenu, Provider, store);
    Navigation.registerComponentWithRedux('CheckIn', () => CheckIn, Provider, store);
    Navigation.registerComponentWithRedux('CheckOut', () => CheckOut, Provider, store);
    Navigation.registerComponentWithRedux('Chat', () => Chat, Provider, store);
    Navigation.registerComponentWithRedux('InAppNotification', () => InAppNotification, Provider, store);
    Navigation.registerComponentWithRedux('ConciergeService', () => ConciergeService, Provider, store);
    Navigation.registerComponentWithRedux('RequestItems', () => RequestItems, Provider, store);
    Navigation.registerComponentWithRedux('RestaurantList', () => RestaurantList, Provider, store);
    Navigation.registerComponentWithRedux('RestaurantService', () => RestaurantService, Provider, store);
    Navigation.registerComponentWithRedux('BookATable', () => BookATable, Provider, store);
    Navigation.registerComponentWithRedux('OrderRoomService', () => OrderRoomService, Provider, store);
    Navigation.registerComponentWithRedux('TrackingProgress', () => TrackingProgress, Provider, store);
    Navigation.registerComponentWithRedux('SpaService', () => SpaService, Provider, store);
    Navigation.registerComponentWithRedux('SpaBookingTime', () => SpaBookingTime, Provider, store);
    Navigation.registerComponentWithRedux('SpaTreatmentList', () => SpaTreatmentList, Provider, store);
    Navigation.registerComponentWithRedux('CleaningService', () => CleaningService, Provider, store);
    Navigation.registerComponentWithRedux(
        'CleaningRequestComplete',
        () => CleaningRequestComplete,
        Provider,
        store,
    );
    Navigation.registerComponentWithRedux('PromotionService', () => PromotionService, Provider, store);
    Navigation.registerComponentWithRedux('ExperienceService', () => ExperienceService, Provider, store);
    Navigation.registerComponentWithRedux('LaundryService', () => LaundryService, Provider, store);
    Navigation.registerComponentWithRedux('HotelMap', () => HotelMap, Provider, store);
    Navigation.registerComponentWithRedux(
        'PromotionDetails',
        () => gestureHandlerRootHOC(PromotionDetails),
        Provider,
        store,
    );
    Navigation.registerComponentWithRedux('SpaTrackingProgress', () => SpaTrackingProgress, Provider, store);
    Navigation.registerComponentWithRedux(
        'ConciergeTrackingProgress',
        () => ConciergeTrackingProgress,
        Provider,
        store,
    );
};
