import { AxiosError } from 'axios';
import { IFailedResponse } from '../types/responseApi';
import { Alert, ToastAndroid, Platform } from 'react-native';
import { checkOut } from '../actions/action.account';
import firebase from 'react-native-firebase';

export const toast = (msg: string | { message: string }, title: any = null) => {
    if (__DEV__) {
        console.log('TOAST: ', msg);
    }

    const message = typeof msg !== 'string' ? msg.message : msg;

    if (Platform.OS === 'ios') {
        Alert.alert(title, message);
    } else {
        ToastAndroid.show(message, ToastAndroid.SHORT);
    }
};

interface IHandleError {
    error: AxiosError;
    dispatch: any;
    displayMessage?: boolean;
    failedAction: object;
    type: string;
    onFailed?: (error: AxiosError) => void;
    delayShowMessage?: boolean;
}

export const handleError = ({
    error,
    dispatch,
    displayMessage = true,
    failedAction,
    type,
    onFailed,
    delayShowMessage = false,
}: IHandleError) => {
    // dispatch and log first
    dispatch(failedAction);

    if (__DEV__) {
        console.log(`${type}: `, error);
    }

    // check for unauthorized
    if (error.response && error.response.status === 401) {
        dispatch(checkOut());
    }

    // display message if error from server
    if (displayMessage && error.response && error.response.data) {
        const { message } = <IFailedResponse>error.response.data;
        const msg = message && message !== '' ? message : "Dont't worry, and please try again.";

        if (delayShowMessage) {
            setTimeout(() => {
                toast(msg, 'Error');
            }, 1500);
        } else {
            toast(msg, 'Error');
        }
    }

    if (onFailed) {
        onFailed(error);
    }
};

export interface IRulesFormValidation {
    isValid: boolean;
    message: string;
}

export const handleFormValidation = (
    rules: IRulesFormValidation[],
    onSuccessValidation?: () => void,
    onFailedValidation?: (props: IRulesFormValidation) => void,
) => {
    let valid = true;
    rules.some((rule) => {
        if (!rule.isValid) {
            if (onFailedValidation) {
                onFailedValidation(rule);
            }
            valid = false;

            return true;
        }

        return false;
    });

    if (valid && onSuccessValidation) {
        onSuccessValidation();
    }
};

export const handleLocalNotification = (text: string, messageId: string, payload: object) => {
    const localNotification = new firebase.notifications.Notification()
        .setNotificationId(messageId)
        .setTitle('New message')
        .setBody(text)
        .setData(payload)
        .setSound('default');

    if (Platform.OS === 'android') {
        localNotification.android
            .setChannelId('servr')
            .android.setSmallIcon('ic_launcher_round')
            .android.setPriority(firebase.notifications.Android.Priority.High);
    }

    return firebase.notifications().displayNotification(localNotification);
};
