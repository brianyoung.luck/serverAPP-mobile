import { Layout } from 'react-native-navigation';
import { IInAppNotificationProps } from '../modules/_global/InAppNotification';
import { IRestaurantServiceProps } from '../modules/Restaurant/RestaurantService';
import { IRestaurantListProps } from '../modules/Restaurant/RestaurantList';
import { IBookATableProps } from '../modules/Restaurant/BookATable';
import { IOrderRoomServiceProps } from '../modules/Restaurant/OrderRoomService';
import { ISpaBookingTimeProps } from '../modules/Spa/SpaBookingTime';
import { ISpaTreatmentListProps } from '../modules/Spa/SpaTreatmentList';
import { ISpaServiceProps } from '../modules/Spa/SpaService';
import { ICleaningRequestCompleteProps } from '../modules/CleaningService/CleaningRequestComplete';
import { IChatBaseProps } from '../modules/Chat/Chat';
import { IPromotionDetailsProps } from '../modules/promotion/PromotionDetails';
import { IPromotionServiceProps } from '../modules/promotion/PromotionService';
import { IConciergeServiceProps } from '../modules/ConciergeService/ConciergeService';
import { ICheckInProps } from '../modules/CheckIn/CheckIn';

export const splashscreen: Layout = {
    component: {
        id: 'splashscreen',
        name: 'SplashScreen',
    },
};

export const pickHotel: Layout = {
    component: {
        id: 'pickHotel',
        name: 'PickHotel',
    },
};

export const mainmenu: Layout = {
    stack: {
        children: [
            {
                component: {
                    id: 'mainmenu',
                    name: 'MainMenu',
                },
            },
        ],
    },
};

export const checkin = (passProps?: Partial<ICheckInProps>): Layout => ({
    component: {
        id: 'checkin',
        name: 'CheckIn',
        passProps,
    },
});

export const checkout: Layout = {
    component: {
        id: 'checkout',
        name: 'CheckOut',
    },
};

export const chat = (passProps?: IChatBaseProps): Layout => ({
    component: {
        id: 'chat',
        name: 'Chat',
        passProps,
    },
});

export const inAppNotification = (passProps?: Partial<IInAppNotificationProps>): Layout => ({
    component: {
        name: 'InAppNotification',
        options: {
            overlay: {
                interceptTouchOutside: true,
            },
        },
        passProps,
    },
});

export const conciergeService = (passProps?: Partial<IConciergeServiceProps>): Layout => ({
    component: {
        id: 'conciergeService',
        name: 'ConciergeService',
        passProps,
    },
});

export const requestItems: Layout = {
    component: {
        id: 'requestItems',
        name: 'RequestItems',
    },
};

export const restaurantList = (passProps?: Partial<IRestaurantListProps>): Layout => ({
    component: {
        id: 'restaurantList',
        name: 'RestaurantList',
        passProps,
    },
});

export const restaurantService = (passProps?: Partial<IRestaurantServiceProps>): Layout => ({
    component: {
        id: 'restaurantService',
        name: 'RestaurantService',
        passProps,
    },
});

export const bookATable = (passProps?: Partial<IBookATableProps>): Layout => ({
    component: {
        id: 'bookATable',
        name: 'BookATable',
        passProps,
    },
});

export const orderRoomService = (passProps?: Partial<IOrderRoomServiceProps>): Layout => ({
    component: {
        id: 'orderRoomService',
        name: 'OrderRoomService',
        passProps,
    },
});

export const trackingProgress: Layout = {
    component: {
        id: 'trackingProgress',
        name: 'TrackingProgress',
    },
};

export const spaService = (passProps: Partial<ISpaServiceProps>): Layout => ({
    component: {
        id: 'spaService',
        name: 'SpaService',
        passProps,
    },
});
export const experienceService: Layout = {
    component: {
        id: 'experienceService',
        name: 'ExperienceService',
    },
};
export const promotionService = (passProps: Partial<IPromotionServiceProps>): Layout => ({
    component: {
        id: 'promotionService',
        name: 'PromotionService',
        passProps,
    },
});
export const promotionDetails = (passProps: Partial<IPromotionDetailsProps>): Layout => ({
    component: {
        id: 'promotionDetails',
        name: 'PromotionDetails',
        passProps,
    },
});

export const spaBookingTime = (passProps: Partial<ISpaBookingTimeProps>): Layout => ({
    component: {
        id: 'spaBookingTime',
        name: 'SpaBookingTime',
        passProps,
    },
});

export const spaTreatmentList = (passProps: Partial<ISpaTreatmentListProps>): Layout => ({
    component: {
        id: 'spaTreatmentList',
        name: 'SpaTreatmentList',
        passProps,
    },
});

export const cleaningService: Layout = {
    component: {
        id: 'cleaningService',
        name: 'CleaningService',
    },
};

export const cleaningRequestComplete = (passProps?: Partial<ICleaningRequestCompleteProps>): Layout => ({
    component: {
        id: 'cleaningRequestComplete',
        name: 'CleaningRequestComplete',
        passProps,
    },
});

export const laundryService: Layout = {
    component: {
        id: 'laundryService',
        name: 'LaundryService',
    },
};
export const hotelMap: Layout = {
    component: {
        id: 'hotelmap',
        name: 'HotelMap',
    },
};

export const spaTrackingProgress: Layout = {
    component: {
        id: 'spaTrackingProgress',
        name: 'SpaTrackingProgress',
    },
};

export const conciergeTrackingProgress: Layout = {
    component: {
        id: 'conciergeTrackingProgress',
        name: 'ConciergeTrackingProgress',
    },
};
