import { IConciergeServiceState } from '../types/conciergeService';
import {
    ActionConciergeServiceType,
    GET_CONCIERGE_SERVICE_ITEMS_SUCCESS,
    CONCIERGE_SERVICE_FAILED,
    CONCIERGE_TRACKING_PROGRESS_ORDER_ROOM_SERVICE_SUCCESS,
} from '../types/action.conciergeService';

const initialState: IConciergeServiceState = {
    serviceItems: [],
    error: {},
    conciergeTrakingProgress: [],
};

export default (state = initialState, action: ActionConciergeServiceType): IConciergeServiceState => {
    switch (action.type) {
        case GET_CONCIERGE_SERVICE_ITEMS_SUCCESS:
            return {
                ...state,
                serviceItems: action.payload.serviceItems,
            };

        case CONCIERGE_SERVICE_FAILED:
            return {
                ...state,
                error: action.payload.error,
            };

        case CONCIERGE_TRACKING_PROGRESS_ORDER_ROOM_SERVICE_SUCCESS:
            console.log('in reducerrrr', action.payload);
            return {
                ...state,
                conciergeTrakingProgress: action.payload.conciergeTrakingProgress.data,
            };

        default:
            return state;
    }
};
