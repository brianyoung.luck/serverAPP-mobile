import { ILaundryState } from '../types/laundry';
import {
    ActionCleaningServiceType,
    GET_LAUNDRY_SERVICES_MENU,
    GET_LAUNDRY_SERVICES_MENU_SUCCESS,
} from '../actions/action.cleaningService';

const initialState: ILaundryState = {
    laundry: { laundries: [] },
    error: {},
};

export default (state = initialState, action: ActionCleaningServiceType): ILaundryState => {
    switch (action.type) {
        case GET_LAUNDRY_SERVICES_MENU:
            return {
                ...state,
                // laundries: action.payload.,
            };
        case GET_LAUNDRY_SERVICES_MENU_SUCCESS:
            return {
                ...state,
                laundry: action.payload.laundry,
            };
        default:
            return state;
    }
};
