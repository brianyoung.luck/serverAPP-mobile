import { IAccountState } from '../types/account';
import {
    ActionAccountType,
    GET_PROFILE_SUCCESS,
    CHECK_IN_SUCCESS,
    ACCOUNT_FAILED,
    CHECK_OUT_SUCCESS,
    VERIFY_PHONE_NUMBER_SUCCESS,
    LOGIN_SUCCESS,
} from '../types/action.account';

export const initialState: IAccountState = {
    access_token: '',
    profile: {},
    isCheckedIn: false,
    confirmationResult: null,
    error: {},
};

export default (state = initialState, action: ActionAccountType): IAccountState => {
    switch (action.type) {
        case LOGIN_SUCCESS:
        case CHECK_IN_SUCCESS:
            return {
                ...state,
                access_token: action.payload.token,
                profile: action.payload.profile,
                isCheckedIn: true,
            };

        case VERIFY_PHONE_NUMBER_SUCCESS:
            return {
                ...state,
                confirmationResult: action.payload.confirmationResult,
            };

        case GET_PROFILE_SUCCESS:
            console.log('in reducerrr', action.payload.profile);
            return {
                ...state,
                profile: {
                    ...state.profile,
                    ...action.payload.profile,
                },
            };

        case CHECK_OUT_SUCCESS:
            return {
                ...state,
                ...initialState,
            };

        case ACCOUNT_FAILED:
            return {
                ...state,
                error: action.payload.error,
            };

        default:
            return state;
    }
};
