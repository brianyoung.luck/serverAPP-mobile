import { combineReducers } from 'redux';
import account from './reducer.account';
import hotel from './reducer.hotel';
import chat from './reducer.chat';
import restaurant from './reducer.restaurant';
import conciergeService from './reducer.conciergeService';
import spa from './reducer.spa';
import promotion from './reducer.promotion';
import experience from './reducer.experience';
import laundry from './reducer.laundry';
import language from './reducer.language';

export default combineReducers({
    account,
    hotel,
    chat,
    restaurant,
    conciergeService,
    spa,
    promotion,
    experience,
    laundry,
    language,
});
