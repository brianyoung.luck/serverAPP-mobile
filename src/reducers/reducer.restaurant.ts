import { IRestaurantState } from '../types/restaurant';
import {
    ActionRestaurantType,
    RESTAURANT_FAILED,
    GET_RESTAURANT_LIST_SUCCESS,
    GET_RESTAURANT_CATEGORY_DISH_SUCCESS,
    TRACKING_PROGRESS_ORDER_ROOM_SERVICE_SUCCESS,
} from '../types/action.restaurant';

const initialState: IRestaurantState = {
    restaurants: [],
    dishCategories: [],
    trackingProgressOrderRoomService: [],
    error: {},
};

export default (state = initialState, action: ActionRestaurantType): IRestaurantState => {
    switch (action.type) {
        case GET_RESTAURANT_LIST_SUCCESS:
            return {
                ...state,
                restaurants: action.payload.restaurants,
            };

        case GET_RESTAURANT_CATEGORY_DISH_SUCCESS:
            return {
                ...state,
                dishCategories: action.payload.dishCategories,
            };

        case TRACKING_PROGRESS_ORDER_ROOM_SERVICE_SUCCESS:
            return {
                ...state,
                trackingProgressOrderRoomService: action.payload.trakingProgress,
            };

        case RESTAURANT_FAILED:
            return {
                ...state,
                error: action.payload.error,
            };

        default:
            return state;
    }
};
