import {
    TGetRestaurantList,
    GET_RESTAURANT_LIST,
    TGetRestaurantListSuccess,
    GET_RESTAURANT_LIST_SUCCESS,
    TGetRestaurantCategoryDish,
    GET_RESTAURANT_CATEGORY_DISH,
    TGetRestaurantCategoryDishSuccess,
    GET_RESTAURANT_CATEGORY_DISH_SUCCESS,
    TBookATable,
    BOOK_A_TABLE,
    TBookATableSuccess,
    BOOK_A_TABLE_SUCCESS,
    TOrderRoomService,
    ORDER_ROOM_SERVICE,
    TOrderRoomServiceSuccess,
    ORDER_ROOM_SERVICE_SUCCESS,
    TRestaurantFailed,
    RESTAURANT_FAILED,
    TTrackingProgressOrderRoomService,
    TRACKING_PROGRESS_ORDER_ROOM_SERVICE,
    TTrackingProgressOrderRoomServiceSuccess,
    TRACKING_PROGRESS_ORDER_ROOM_SERVICE_SUCCESS,
    DELETE_ORDER,
    DELETE_ORDER_SUCCESS,
    TDeleteOrder,
    TDeleteOrderSuccess,
} from '../types/action.restaurant';

export const getRestaurantList: TGetRestaurantList = (onSuccess, onFailed) => ({
    type: GET_RESTAURANT_LIST,
    payload: {
        onSuccess,
        onFailed,
    },
});

export const getRestaurantListSuccess: TGetRestaurantListSuccess = (restaurants) => ({
    type: GET_RESTAURANT_LIST_SUCCESS,
    payload: {
        restaurants,
    },
});

export const getRestaurantCategoryDish: TGetRestaurantCategoryDish = (id, onSuccess, onFailed) => ({
    type: GET_RESTAURANT_CATEGORY_DISH,
    payload: {
        id,
        onSuccess,
        onFailed,
    },
});

export const getRestaurantCategoryDishSuccess: TGetRestaurantCategoryDishSuccess = (dishCategories) => ({
    type: GET_RESTAURANT_CATEGORY_DISH_SUCCESS,
    payload: {
        dishCategories,
    },
});

export const bookATable: TBookATable = (
    restaurantId,
    numberPeople,
    time,
    date,
    table_no,
    onSuccess,
    onFailed,
) => ({
    type: BOOK_A_TABLE,
    payload: {
        restaurantId,
        numberPeople,
        time,
        date,
        table_no,
        onSuccess,
        onFailed,
    },
});

export const bookATableSuccess: TBookATableSuccess = () => ({
    type: BOOK_A_TABLE_SUCCESS,
});

export const orderRoomService: TOrderRoomService = (restaurantId, items, onSuccess, onFailed) => ({
    type: ORDER_ROOM_SERVICE,
    payload: {
        restaurantId,
        items,
        onSuccess,
        onFailed,
    },
});

export const orderRoomServiceSuccess: TOrderRoomServiceSuccess = () => ({
    type: ORDER_ROOM_SERVICE_SUCCESS,
});

export const trackingProgressOrderRoomService: TTrackingProgressOrderRoomService = (onSuccess, onFailed) => ({
    type: TRACKING_PROGRESS_ORDER_ROOM_SERVICE,
    payload: {
        onSuccess,
        onFailed,
    },
});

export const trackingProgressOrderRoomServiceSuccess: TTrackingProgressOrderRoomServiceSuccess = (
    trakingProgress,
) => ({
    type: TRACKING_PROGRESS_ORDER_ROOM_SERVICE_SUCCESS,
    payload: {
        trakingProgress,
    },
});

export const restaurantFailed: TRestaurantFailed = (error, type) => ({
    type: RESTAURANT_FAILED,
    payload: {
        error: {
            error,
            type,
        },
    },
});

export const deleteOrder: TDeleteOrder = (id, onSuccess, onFailed) => ({
    type: DELETE_ORDER,
    payload: {
        id,
        onSuccess,
        onFailed,
    },
});

export const deleteOrderSuccess: TDeleteOrderSuccess = () => ({
    type: DELETE_ORDER_SUCCESS,
});

// export const deleteOrder: TDeleteOrder = (onSuccess, onFailed) => ({
//     type: DELETE_ORDER,
//     payload: {
//         onSuccess,
//         onFailed,
//     },
// });

// export const deleteOrderSuccess: TDeleteOrderSuccess = (
//     trakingProgress,
// ) => ({
//     type: DELETE_ORDER_SUCCESS,
//     payload: {
//         trakingProgress,
//     },
// });
