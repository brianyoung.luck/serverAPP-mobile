import {
    TGetConciergeServiceItems,
    GET_CONCIERGE_SERVICE_ITEMS,
    TGetConciergeServiceItemsSuccess,
    GET_CONCIERGE_SERVICE_ITEMS_SUCCESS,
    TCreateRequest,
    CREATE_REQUEST,
    TCreateRequestSuccess,
    CREATE_REQUEST_SUCCESS,
    TConciergeServiceFailed,
    CONCIERGE_SERVICE_FAILED,
    TGetConciergeTrackingProgress,
    TGetConciergeTrackingProgressSuccess,
    CONCIERGE_TRACKING_PROGRESS_ORDER_ROOM_SERVICE,
    CONCIERGE_TRACKING_PROGRESS_ORDER_ROOM_SERVICE_SUCCESS,
    DELETE_CONCIERGE_ORDER,
    DELETE_CONCIERGE_ORDER_SUCCESS,
    TDeleteConciergeOrder,
    TDeleteConciergeOrderSuccess,
} from '../types/action.conciergeService';

// import {IConciergeTrackingProgressOrderRoomService} from '../types/conciergeService'

// export const CONCIERGE_TRACKING_PROGRESS_ORDER_ROOM_SERVICE = 'CONCIERGE_TRACKING_PROGRESS_ORDER_ROOM_SERVICE';
// export const CONCIERGE_TRACKING_PROGRESS_ORDER_ROOM_SERVICE_SUCCESS = 'CONCIERGE_TRACKING_PROGRESS_ORDER_ROOM_SERVICE_SUCCESS';

export const getConciergeServiceItems: TGetConciergeServiceItems = (onSuccess, onFailed) => ({
    type: GET_CONCIERGE_SERVICE_ITEMS,
    payload: {
        onSuccess,
        onFailed,
    },
});

export const getConciergeServiceItemsSuccess: TGetConciergeServiceItemsSuccess = (serviceItems) => ({
    type: GET_CONCIERGE_SERVICE_ITEMS_SUCCESS,
    payload: {
        serviceItems,
    },
});

export const createRequest: TCreateRequest = (items, onSuccess, onFailed) => ({
    type: CREATE_REQUEST,
    payload: {
        items,
        onSuccess,
        onFailed,
    },
});

export const createRequestSuccess: TCreateRequestSuccess = () => ({
    type: CREATE_REQUEST_SUCCESS,
});

export const conciergeServiceFailed: TConciergeServiceFailed = (error, type) => ({
    type: CONCIERGE_SERVICE_FAILED,
    payload: {
        error: {
            error,
            type,
        },
    },
});

export const conciergeTrackingProgressOrderRoomService: TGetConciergeTrackingProgress = (
    onSuccess,
    onFailed,
) => ({
    type: CONCIERGE_TRACKING_PROGRESS_ORDER_ROOM_SERVICE,
    payload: {
        onSuccess,
        onFailed,
    },
});

export const conciergeTrackingProgressOrderRoomServiceSuccess: TGetConciergeTrackingProgressSuccess = (
    conciergeTrakingProgress,
) => ({
    type: CONCIERGE_TRACKING_PROGRESS_ORDER_ROOM_SERVICE_SUCCESS,
    payload: {
        conciergeTrakingProgress,
    },
});

export const deleteConciergeOrder: TDeleteConciergeOrder = (id, type, onSuccess, onFailed) => ({
    type: DELETE_CONCIERGE_ORDER,
    payload: {
        id,
        type,
        onSuccess,
        onFailed,
    },
});

export const deleteConciergeOrderSuccess: TDeleteConciergeOrderSuccess = () => ({
    type: DELETE_CONCIERGE_ORDER_SUCCESS,
});
