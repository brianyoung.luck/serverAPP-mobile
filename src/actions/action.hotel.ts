import {
    GET_HOTEL_DETAIL,
    GET_HOTEL_DETAIL_SUCCESS,
    HOTEL_FAILED,
    TGetHotelDetail,
    TGetHotelDetailSuccess,
    THotelFailed,
} from '../types/action.hotel';

export const getHotelDetail: TGetHotelDetail = (code, onSuccess, onFailed) => ({
    type: GET_HOTEL_DETAIL,
    payload: {
        code,
        onSuccess,
        onFailed,
    },
});

export const getHotelDetailSuccess: TGetHotelDetailSuccess = (
    id,
    code,
    description,
    name,
    logo,
    theme,
    icon,
    feature,
    category,
    currency,
    mobile_hotel_layout_id,
    mobile_hotel_layouts,
) => ({
    type: GET_HOTEL_DETAIL_SUCCESS,
    payload: {
        id,
        code,
        description,
        name,
        logo,
        theme,
        icon,
        feature,
        category,
        currency,
        mobile_hotel_layout_id,
        mobile_hotel_layouts,
    },
});

export const hotelFailed: THotelFailed = (error, type) => ({
    type: HOTEL_FAILED,
    payload: {
        error: {
            error,
            type,
        },
    },
});
