import {
    CHECK_IN,
    CHECK_IN_SUCCESS,
    CHECK_OUT,
    CHECK_OUT_SUCCESS,
    GET_PROFILE,
    GET_PROFILE_SUCCESS,
    VERIFY_PHONE_NUMBER,
    VERIFY_PHONE_NUMBER_SUCCESS,
    VERIFY_PIN,
    VERIFY_PIN_SUCCESS,
    ACCOUNT_FAILED,
    TCheckIn,
    TCheckInSuccess,
    TCheckOut,
    TCheckOutSuccess,
    TGetProfile,
    TGetProfileSuccess,
    TVerifyPhoneNumber,
    TVerifyPhoneNumberSuccess,
    TVerifyPin,
    TVerifyPinSuccess,
    TAccountFailed,
    TOnAuthStateChanged,
    ON_AUTH_STATE_CHANGED,
    TOnAuthStateChangedSuccess,
    ON_AUTH_STATE_CHANGED_SUCCESS,
    TLogin,
    LOGIN,
    TLoginSuccess,
    LOGIN_SUCCESS,
    TRemoveOnAuthStateChanged,
    REMOVE_ON_AUTH_STATE_CHANGED,
    TLateCheckOut,
    LATE_CHECK_OUT,
    TLateCheckOutSuccess,
    LATE_CHECK_OUT_SUCCESS,
    TUserCheckOut,
    TUserCheckOutSuccess,
    USER_CHECK_OUT,
    USER_CHECK_OUT_SUCCESS,
} from '../types/action.account';

export const checkIn: TCheckIn = (data, onSuccess, onFailed) => ({
    type: CHECK_IN,
    payload: {
        data,
        onSuccess,
        onFailed,
    },
});

export const checkInSuccess: TCheckInSuccess = (token, profile) => ({
    type: CHECK_IN_SUCCESS,
    payload: {
        token,
        profile,
    },
});

export const checkOut: TCheckOut = (onSuccess, onFailed) => ({
    type: CHECK_OUT,
    payload: {
        onSuccess,
        onFailed,
    },
});

export const checkOutSuccess: TCheckOutSuccess = () => ({
    type: CHECK_OUT_SUCCESS,
});

export const lateCheckOut: TLateCheckOut = (onSuccess, onFailed) => ({
    type: LATE_CHECK_OUT,
    payload: {
        onSuccess,
        onFailed,
    },
});

export const lateCheckOutSuccess: TLateCheckOutSuccess = () => ({
    type: LATE_CHECK_OUT_SUCCESS,
});

export const getProfile: TGetProfile = (onSuccess, onFailed) => ({
    type: GET_PROFILE,
    payload: {
        onSuccess,
        onFailed,
    },
});

export const getProfileSuccess: TGetProfileSuccess = (profile) => ({
    type: GET_PROFILE_SUCCESS,
    payload: {
        profile,
    },
});

export const verifyPhoneNumber: TVerifyPhoneNumber = (phoneNumber, onSuccess, onFailed) => ({
    type: VERIFY_PHONE_NUMBER,
    payload: {
        phoneNumber,
        onSuccess,
        onFailed,
    },
});

export const verifyPhoneNumberSuccess: TVerifyPhoneNumberSuccess = (confirmationResult) => ({
    type: VERIFY_PHONE_NUMBER_SUCCESS,
    payload: {
        confirmationResult,
    },
});

export const verifyPin: TVerifyPin = (pin, onSuccess, onFailed) => ({
    type: VERIFY_PIN,
    payload: {
        pin,
        onSuccess,
        onFailed,
    },
});

export const verifyPinSuccess: TVerifyPinSuccess = () => ({
    type: VERIFY_PIN_SUCCESS,
});

export const onAuthStateChanged: TOnAuthStateChanged = (onCallback) => ({
    type: ON_AUTH_STATE_CHANGED,
    payload: {
        onCallback,
    },
});

export const onAuthStateChangedSuccess: TOnAuthStateChangedSuccess = () => ({
    type: ON_AUTH_STATE_CHANGED_SUCCESS,
});

export const removeOnAuthStateChanged: TRemoveOnAuthStateChanged = () => ({
    type: REMOVE_ON_AUTH_STATE_CHANGED,
});

export const login: TLogin = (token, onSuccess, onFailed) => ({
    type: LOGIN,
    payload: {
        token,
        onSuccess,
        onFailed,
    },
});

export const loginSuccess: TLoginSuccess = (token, profile) => ({
    type: LOGIN_SUCCESS,
    payload: {
        token,
        profile,
    },
});

export const accountFailed: TAccountFailed = (error, type) => ({
    type: ACCOUNT_FAILED,
    payload: {
        error: {
            error,
            type,
        },
    },
});

export const userCheckOut: TUserCheckOut = (onSuccess, onFailed) => ({
    type: USER_CHECK_OUT,
    payload: {
        onSuccess,
        onFailed,
    },
});

export const userCheckOutSuccess: TUserCheckOutSuccess = () => ({
    type: USER_CHECK_OUT_SUCCESS,
});
