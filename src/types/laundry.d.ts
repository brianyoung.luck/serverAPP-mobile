import { IError } from './responseApi';

export interface ILaundry {
    // id: number;
    // name: string;
    // logo_url: string[];
    laundries: {};
}

export interface ILaundryState {
    laundry: ILaundry;
    error: Partial<IError>;
}
