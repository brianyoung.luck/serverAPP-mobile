import { IError } from './responseApi';

export interface IServiceItem {
    id: number;
    name: string;
    description: null | string;
}

export interface IConciergeTrackingProgressOrderRoomService {
    id: number;
    request_type: string;
    status: string;
    created_at: string;
    services: string[];
}

export interface IConciergeServiceState {
    serviceItems: IServiceItem[];
    error: Partial<IError>;
    conciergeTrakingProgress: IConciergeTrackingProgressOrderRoomService[];
}
