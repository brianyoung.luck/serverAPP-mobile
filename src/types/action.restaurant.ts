import { IRestaurant, ICategoryDish, ITrackingProgressOrderRoomService } from './restaurant';
import { IError, ISuccessBookATable } from './responseApi';

export const GET_RESTAURANT_LIST = 'GET_RESTAURANT_LIST';
export const GET_RESTAURANT_LIST_SUCCESS = 'GET_RESTAURANT_LIST_SUCCESS';

export const GET_RESTAURANT_CATEGORY_DISH = 'GET_RESTAURANT_CATEGORY_DISH';
export const GET_RESTAURANT_CATEGORY_DISH_SUCCESS = 'GET_RESTAURANT_CATEGORY_DISH_SUCCESS';

export const BOOK_A_TABLE = 'BOOK_A_TABLE';
export const BOOK_A_TABLE_SUCCESS = 'BOOK_A_TABLE_SUCCESS';

export const ORDER_ROOM_SERVICE = 'ORDER_ROOM_SERVICE';
export const ORDER_ROOM_SERVICE_SUCCESS = 'ORDER_ROOM_SERVICE_SUCCESS';

export const TRACKING_PROGRESS_ORDER_ROOM_SERVICE = 'TRACKING_PROGRESS_ORDER_ROOM_SERVICE';
export const TRACKING_PROGRESS_ORDER_ROOM_SERVICE_SUCCESS = 'TRACKING_PROGRESS_ORDER_ROOM_SERVICE_SUCCESS';

export const DELETE_ORDER = 'DELETE_ORDER';
export const DELETE_ORDER_SUCCESS = 'DELETE_ORDER_SUCCESS';

export const RESTAURANT_FAILED = 'RESTAURANT_FAILED';

export type TGetRestaurantList<T = () => void> = (
    onSuccess?: T,
    onFailed?: T,
) => {
    type: typeof GET_RESTAURANT_LIST;
    payload: {
        onSuccess?: T;
        onFailed?: T;
    };
};

export type TGetRestaurantListSuccess<T = IRestaurant[]> = (
    restaurants: T,
) => {
    type: typeof GET_RESTAURANT_LIST_SUCCESS;
    payload: {
        restaurants: T;
    };
};

export type TGetRestaurantCategoryDish<T = number, K = () => void> = (
    id: T,
    onSuccess?: K,
    onFailed?: K,
) => {
    type: typeof GET_RESTAURANT_CATEGORY_DISH;
    payload: {
        id: T;
        onSuccess?: K;
        onFailed?: K;
    };
};

export type TGetRestaurantCategoryDishSuccess<T = ICategoryDish[]> = (
    dishCategories: T,
) => {
    type: typeof GET_RESTAURANT_CATEGORY_DISH_SUCCESS;
    payload: {
        dishCategories: T;
    };
};

export type TBookATable<T = number, K = string, I = (data: ISuccessBookATable) => void, J = () => void> = (
    restaurantId: T,
    numberPeople: T,
    time: K,
    date: K,
    table_bo: K,
    onSuccess?: I,
    onFailed?: J,
) => {
    type: typeof BOOK_A_TABLE;
    payload: {
        restaurantId: T;
        numberPeople: T;
        time: K;
        date: K;
        table_no: K;
        onSuccess?: I;
        onFailed?: J;
    };
};

export type TBookATableSuccess = () => {
    type: typeof BOOK_A_TABLE_SUCCESS;
};

export interface IOrderItem {
    dish_id: number;
    qty: number;
    note: string;
}

export type TOrderRoomService<T = number, K = IOrderItem[], I = () => void> = (
    restaurantId: T,
    items: K,
    onSuccess?: I,
    onFailed?: I,
) => {
    type: typeof ORDER_ROOM_SERVICE;
    payload: {
        restaurantId: T;
        items: K;
        onSuccess?: I;
        onFailed?: I;
    };
};

export type TOrderRoomServiceSuccess = () => {
    type: typeof ORDER_ROOM_SERVICE_SUCCESS;
};

export type TTrackingProgressOrderRoomService = (
    onSuccess?: (data: ITrackingProgressOrderRoomService[]) => void,
    onFailed?: () => void,
) => {
    type: typeof TRACKING_PROGRESS_ORDER_ROOM_SERVICE;
    payload: {
        onSuccess: typeof onSuccess;
        onFailed: typeof onFailed;
    };
};

export type TTrackingProgressOrderRoomServiceSuccess = (
    trakingProgress: ITrackingProgressOrderRoomService[],
) => {
    type: typeof TRACKING_PROGRESS_ORDER_ROOM_SERVICE_SUCCESS;
    payload: {
        trakingProgress: typeof trakingProgress;
    };
};

export type TRestaurantFailed<T = any, K = string> = (
    error: T,
    type: K,
) => {
    type: typeof RESTAURANT_FAILED;
    payload: {
        error: IError;
    };
};

export type TDeleteOrder<T = number, I = () => void, J = () => void> = (
    id: T,
    onSuccess: I,
    onFailed: J,
) => {
    type: typeof DELETE_ORDER;
    payload: {
        id: T;
        onSuccess?: I;
        onFailed?: J;
    };
};

export type TDeleteOrderSuccess = () => {
    type: typeof DELETE_ORDER_SUCCESS;
};

export type ActionRestaurantType = ReturnType<
    | TGetRestaurantList
    | TGetRestaurantListSuccess
    | TGetRestaurantCategoryDish
    | TGetRestaurantCategoryDishSuccess
    | TBookATable
    | TBookATableSuccess
    | TOrderRoomService
    | TOrderRoomServiceSuccess
    | TTrackingProgressOrderRoomService
    | TTrackingProgressOrderRoomServiceSuccess
    | TRestaurantFailed
    | TDeleteOrder
    | TDeleteOrderSuccess
>;
