import { IServiceItem, IConciergeTrackingProgressOrderRoomService } from './conciergeService';
import { IError } from './responseApi';

export const GET_CONCIERGE_SERVICE_ITEMS = 'GET_CONCIERGE_SERVICE_ITEMS';
export const GET_CONCIERGE_SERVICE_ITEMS_SUCCESS = 'GET_CONCIERGE_SERVICE_ITEMS_SUCCESS';

export const CREATE_REQUEST = 'CREATE_REQUEST';
export const CREATE_REQUEST_SUCCESS = 'CREATE_REQUEST_SUCCESS';

export const CONCIERGE_SERVICE_FAILED = 'CONCIERGE_SERVICE_FAILED';

export const CONCIERGE_TRACKING_PROGRESS_ORDER_ROOM_SERVICE =
    'CONCIERGE_TRACKING_PROGRESS_ORDER_ROOM_SERVICE';
export const CONCIERGE_TRACKING_PROGRESS_ORDER_ROOM_SERVICE_SUCCESS =
    'CONCIERGE_TRACKING_PROGRESS_ORDER_ROOM_SERVICE_SUCCESS';

export const DELETE_CONCIERGE_ORDER = 'DELETE_CONCIERGE_ORDER';
export const DELETE_CONCIERGE_ORDER_SUCCESS = 'DELETE_CONCIERGE_ORDER_SUCCESS';

export type TGetConciergeServiceItems<T = () => void> = (
    onSuccess?: T,
    onFailed?: T,
) => {
    type: typeof GET_CONCIERGE_SERVICE_ITEMS;
    payload: {
        onSuccess?: T;
        onFailed?: T;
    };
};

export type TGetConciergeServiceItemsSuccess<T = IServiceItem[]> = (
    serviceItems: T,
) => {
    type: typeof GET_CONCIERGE_SERVICE_ITEMS_SUCCESS;
    payload: {
        serviceItems: T;
    };
};

export interface ICreateRequestItem {
    service_id: number;
    qty: number;
    note: string;
}

export type TCreateRequest<T = ICreateRequestItem[], K = (message: string) => void, I = () => void> = (
    items: T,
    onSuccess?: K,
    onFailed?: I,
) => {
    type: typeof CREATE_REQUEST;
    payload: {
        items: T;
        onSuccess?: K;
        onFailed?: I;
    };
};

export type TCreateRequestSuccess = () => {
    type: typeof CREATE_REQUEST_SUCCESS;
};

export type TConciergeServiceFailed<T = any, K = string> = (
    error: T,
    type: K,
) => {
    type: typeof CONCIERGE_SERVICE_FAILED;
    payload: {
        error: IError;
    };
};

export type TGetConciergeTrackingProgress<T = () => void> = (
    onSuccess?: T,
    onFailed?: T,
) => {
    type: typeof CONCIERGE_TRACKING_PROGRESS_ORDER_ROOM_SERVICE;
    payload: {
        onSuccess?: T;
        onFailed?: T;
    };
};

export type TGetConciergeTrackingProgressSuccess<T = IConciergeTrackingProgressOrderRoomService[]> = (
    conciergeTrakingProgress: T,
) => {
    type: typeof CONCIERGE_TRACKING_PROGRESS_ORDER_ROOM_SERVICE_SUCCESS;
    payload: {
        conciergeTrakingProgress: T;
    };
};

export type TDeleteConciergeOrder<T = number, I = () => void, J = () => void, K = string> = (
    id: T,
    type: K,
    onSuccess: I,
    onFailed: J,
) => {
    type: typeof DELETE_CONCIERGE_ORDER;
    payload: {
        id: T;
        type: K;
        onSuccess?: I;
        onFailed?: J;
    };
};

export type TDeleteConciergeOrderSuccess = () => {
    type: typeof DELETE_CONCIERGE_ORDER_SUCCESS;
};

export type ActionConciergeServiceType =
    | ReturnType<TGetConciergeServiceItems>
    | ReturnType<TGetConciergeServiceItemsSuccess>
    | ReturnType<TCreateRequest>
    | ReturnType<TCreateRequestSuccess>
    | ReturnType<TConciergeServiceFailed>
    | ReturnType<TGetConciergeTrackingProgress>
    | ReturnType<TGetConciergeTrackingProgressSuccess>;
