import { IError } from './responseApi';
import { RNFirebase } from 'react-native-firebase';

export type TLateCheckoutStatus = null | 'pending' | 'accepted' | 'rejected';

export type TStatusCheckIn = 'accepted' | 'pending';

export interface IProfile {
    reference: null | string;
    arrival_date: string;
    departure_date: string;
    cardholder_name: string;
    card_number: string;
    card_expiry_date: string;
    card_address: string;
    phone_number: string;
    passport_photos: string[];
    sendbird_user_id: string;
    sendbird_access_token: string;
    sendbird_channel_url: string;
    checkout_time: string;
    late_checkout_time: string;
    late_checkout_status: TLateCheckoutStatus;
    status: TStatusCheckIn;
    room_number: number;
    note_request: string;
    signature_photo: string[];
    terms_and_condition: boolean;
}

export interface IAccountState {
    access_token: string;
    isCheckedIn: boolean;
    confirmationResult: RNFirebase.ConfirmationResult | null;
    profile: Partial<IProfile>;
    error: Partial<IError>;
}
