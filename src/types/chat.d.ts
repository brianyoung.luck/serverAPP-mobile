import { IError } from './responseApi';

export interface IProfileChat {
    _id: string;
    name: string;
    avatar: string;
}

export interface IMessageChat {
    _id: number;
    text: string;
    createdAt: number;
    user: IProfileChat;
    messageType: string;
}

export interface IChatState {
    isConnected: boolean;
    profile: null | IProfileChat;
    messages: IMessageChat[];
    groupChannel: null | SendBird.GroupChannel;
    unreadMessage: number;
    isInChatScreen: boolean;
    error: Partial<IError>;
}
