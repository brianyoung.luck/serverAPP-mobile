import { ILogoHotel, IThemeHotel, IIconHotel, IFeatureHotel } from './hotel';
import { IError } from './responseApi';

export const GET_HOTEL_DETAIL = 'GET_HOTEL_DETAIL';
export const GET_HOTEL_DETAIL_SUCCESS = 'GET_HOTEL_DETAIL_SUCCESS';

export const HOTEL_FAILED = 'HOTEL_FAILED';

export type TGetHotelDetail<T = string, K = () => void> = (
    code: T,
    onSuccess?: K,
    onFailed?: K,
) => {
    type: typeof GET_HOTEL_DETAIL;
    payload: {
        code: string;
        onSuccess?: K;
        onFailed?: K;
    };
};

export type TGetHotelDetailSuccess = (
    id: number,
    code: string,
    description: string,
    name: string,
    logo: ILogoHotel,
    theme: IThemeHotel,
    icon: IIconHotel,
    feature: IFeatureHotel,
    category: string,
    currency: string,
    mobile_hotel_layout_id: number,
    mobile_hotel_layouts: any,
) => {
    type: typeof GET_HOTEL_DETAIL_SUCCESS;
    payload: {
        id: number;
        code: string;
        description: string;
        name: string;
        logo: ILogoHotel;
        theme: IThemeHotel;
        icon: IIconHotel;
        feature: IFeatureHotel;
        category: string;
        currency: string;
        mobile_hotel_layout_id: number;
        mobile_hotel_layouts: any;
    };
};

export type THotelFailed<T = any, K = string> = (
    error: T,
    type: K,
) => {
    type: typeof HOTEL_FAILED;
    payload: {
        error: IError;
    };
};

export type ActionHotelType =
    | ReturnType<TGetHotelDetail>
    | ReturnType<TGetHotelDetailSuccess>
    | ReturnType<THotelFailed>;
