import { IError } from './responseApi';
import { IProfile } from './account';
import { RNFirebase } from 'react-native-firebase';

export const CHECK_IN = 'CHECK_IN';
export const CHECK_IN_SUCCESS = 'CHECK_IN_SUCCESS';

export const CHECK_OUT = 'CHECK_OUT';
export const CHECK_OUT_SUCCESS = 'CHECK_OUT_SUCCESS';

export const LATE_CHECK_OUT = 'LATE_CHECKOUT';
export const LATE_CHECK_OUT_SUCCESS = 'LATE_CHECKOUT_SUCCESS';

export const GET_PROFILE = 'GET_PROFILE';
export const GET_PROFILE_SUCCESS = 'GET_PROFILE_SUCCESS';

export const VERIFY_PHONE_NUMBER = 'VERIFY_PHONE_NUMBER';
export const VERIFY_PHONE_NUMBER_SUCCESS = 'VERIFY_PHONE_NUMBER_SUCCESS';

export const VERIFY_PIN = 'VERIFY_PIN';
export const VERIFY_PIN_SUCCESS = 'VERIFY_PIN_SUCCESS';

export const ON_AUTH_STATE_CHANGED = 'ON_AUTH_STATE_CHANGED';
export const ON_AUTH_STATE_CHANGED_SUCCESS = 'ON_AUTH_STATE_CHANGED_SUCCESS';
export const REMOVE_ON_AUTH_STATE_CHANGED = 'REMOVE_ON_AUTH_STATE_CHANGED';

export const LOGIN = 'LOGIN';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';

export const ACCOUNT_FAILED = 'ACCOUNT_FAILED';

export const USER_CHECK_OUT = 'USER_CHECK_OUT';
export const USER_CHECK_OUT_SUCCESS = 'USER_CHECK_OUT_SUCCESS';

export interface IPhoto {
    uri: string;
    name: string;
    type: 'image/jpeg';
}

export interface ICheckInBody {
    hotel_id: number;
    arrival_date: string;
    departure_date: string;
    cardholder_name: string;
    card_expiry_date: string;
    card_address: string;
    card_number: string;
    phone_number: string;
    reference: string;
    passport_photos: IPhoto[];
    note_request: string;
    signature_photo: Object;
    terms_and_condition: boolean;
}

export type TCheckIn<T = ICheckInBody, K = () => void> = (
    data: T,
    onSuccess?: K,
    onFailed?: K,
) => {
    type: typeof CHECK_IN;
    payload: {
        data: T;
        onSuccess?: K;
        onFailed?: K;
    };
};

export type TCheckInSuccess<T = string, K = IProfile> = (
    token: T,
    profile: K,
) => {
    type: typeof CHECK_IN_SUCCESS;
    payload: {
        token: string;
        profile: IProfile;
    };
};

export type TCheckOut<T = () => void> = (
    onSuccess?: T,
    onFailed?: T,
) => {
    type: typeof CHECK_OUT;
    payload: {
        onSuccess?: T;
        onFailed?: T;
    };
};

export type TCheckOutSuccess = () => {
    type: typeof CHECK_OUT_SUCCESS;
};

export type TGetProfile<T = () => void> = (
    onSuccess?: T,
    onFailed?: T,
) => {
    type: typeof GET_PROFILE;
    payload: {
        onSuccess?: T;
        onFailed?: T;
    };
};

export type TLateCheckOut = (
    onSuccess?: () => void,
    onFailed?: () => void,
) => {
    type: typeof LATE_CHECK_OUT;
    payload: {
        onSuccess: typeof onSuccess;
        onFailed: typeof onFailed;
    };
};

export type TLateCheckOutSuccess = () => {
    type: typeof LATE_CHECK_OUT_SUCCESS;
};

export type TGetProfileSuccess<T = IProfile> = (
    profile: T,
) => {
    type: typeof GET_PROFILE_SUCCESS;
    payload: {
        profile: T;
    };
};

export type TVerifyPhoneNumber<
    T = string,
    K = (confirmationResult: RNFirebase.ConfirmationResult) => void,
    I = () => void
> = (
    phoneNumber: T,
    onSuccess?: K,
    onFailed?: I,
) => {
    type: typeof VERIFY_PHONE_NUMBER;
    payload: {
        phoneNumber: string;
        onSuccess?: K;
        onFailed?: I;
    };
};

export type TVerifyPhoneNumberSuccess<T = RNFirebase.ConfirmationResult> = (
    confirmationResult: T,
) => {
    type: typeof VERIFY_PHONE_NUMBER_SUCCESS;
    payload: {
        confirmationResult: T;
    };
};

export type TVerifyPin<T = string, K = (token: string) => void, I = () => void> = (
    pin: T,
    onSuccess?: K,
    onFailed?: I,
) => {
    type: typeof VERIFY_PIN;
    payload: {
        pin: string;
        onSuccess?: K;
        onFailed?: I;
    };
};

export type TVerifyPinSuccess = () => {
    type: typeof VERIFY_PIN_SUCCESS;
};

export type TOnAuthStateChanged<T = (token: string) => void> = (
    onCallback?: T,
) => {
    type: typeof ON_AUTH_STATE_CHANGED;
    payload: {
        onCallback?: T;
    };
};

export type TOnAuthStateChangedSuccess = () => {
    type: typeof ON_AUTH_STATE_CHANGED_SUCCESS;
};

export type TRemoveOnAuthStateChanged = () => {
    type: typeof REMOVE_ON_AUTH_STATE_CHANGED;
};

export type TLogin<T = string, K = (token: string, profile: IProfile) => void, I = () => void> = (
    token: T,
    onSuccess?: K,
    onFailed?: I,
) => {
    type: typeof LOGIN;
    payload: {
        token: T;
        onSuccess?: K;
        onFailed?: I;
    };
};

export type TLoginSuccess<T = string, K = IProfile> = (
    token: T,
    profile: K,
) => {
    type: typeof LOGIN_SUCCESS;
    payload: {
        token: T;
        profile: K;
    };
};

export type TAccountFailed<T = any, K = string> = (
    error: T,
    type: K,
) => {
    type: typeof ACCOUNT_FAILED;
    payload: {
        error: IError;
    };
};

export type TUserCheckOut = (
    onSuccess?: () => void,
    onFailed?: () => void,
) => {
    type: typeof USER_CHECK_OUT;
    payload: {
        onSuccess: typeof onSuccess;
        onFailed: typeof onFailed;
    };
};

export type TUserCheckOutSuccess = () => {
    type: typeof USER_CHECK_OUT_SUCCESS;
};

export type ActionAccountType = ReturnType<
    | TCheckIn
    | TCheckInSuccess
    | TCheckOut
    | TCheckOutSuccess
    | TGetProfile
    | TGetProfileSuccess
    | TLateCheckOut
    | TLateCheckOutSuccess
    | TVerifyPhoneNumber
    | TVerifyPhoneNumberSuccess
    | TVerifyPin
    | TVerifyPinSuccess
    | TOnAuthStateChanged
    | TOnAuthStateChangedSuccess
    | TRemoveOnAuthStateChanged
    | TLogin
    | TLoginSuccess
    | TAccountFailed
    | TUserCheckOut
    | TUserCheckOutSuccess
>;
