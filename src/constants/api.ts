// import Config from 'react-native-config';
// http://192.168.100.39/server_hotels/public/api
// account
// var oldBaseUrl = "https://api.servrhotels.com"
// var baseUrl = 'http://api.servr.5stardesigners.net/';
var baseUrl = 'http://apiuat.servr.5stardesigners.net/';

export const LOGIN_CUSTOMER = `${baseUrl}/api/auth/login`;
export const ME = `${baseUrl}/auth/me`;
export const LATE_CHECKOUT = `${baseUrl}/late-check-out`;
export const USER_CHECKOUT = `${baseUrl}/checkout`;

// booking
export const BOOKING = `${baseUrl}/bookings`;
export const HOTEL_DETAIL = `${baseUrl}/hotels/{code}`;

// concierge service
export const CONCIERGE_SERVICE_ITEMS = `${baseUrl}/concierge-services`; // get
export const CONCIERGE_CREATE_REQUEST = `${baseUrl}/concierge-services`; // post
export const CONCIERGE_TRACKING_PROGRESS_ORDER_ROOM_SERVICE_API = `${baseUrl}/orders/all-concierge-services`; // post
export const DELETE_CONCIERGE_ORDER_API = `${baseUrl}/orders/delete-concierge-service?id={order_id}&type={order_type}`;

// restaurant
export const GET_RESTAURANTS = `${baseUrl}/restaurants`;
export const GET_RESTAURANT_DISHES = `${baseUrl}/restaurants/{restaurant_id}/dishes`;
export const CREATE_ROOM_SERVICE_ORDER = `${baseUrl}/restaurants/{restaurant_id}/room-order`;
export const CREATE_BOOK_A_TABLE = `${baseUrl}/restaurants/{restaurant_id}/reservation`;
export const TRACKING_PROGRESS_ORDER_ROOM_SERVICE_API = `${baseUrl}/orders/restaurant/room-orders`;
export const DELETE_ORDER_API = `${baseUrl}/orders/restaurant/delete-room-service/{order_id}`;

// spa
export const GET_SPA_API = `${baseUrl}/spas`;
export const GET_SPA_TREATMENT_API = `${baseUrl}/spas/{spa_id}/treatments`;
export const BOOKING_SPA = `${baseUrl}/spas/{spa_id}/bookings`;
export const SPA_TRACKING_PROGRESS_ORDER_ROOM_SERVICE_API = `${baseUrl}/orders/spas`;
export const DELETE_SPA_ORDER_API = `${baseUrl}/orders/spa-delete/{order_id}`;

// cleaning service
export const ROOM_CLEANING_SERVICE_API = `${baseUrl}/room-cleanings`;
export const LAUNDRY_SERVICES_MENU_API = `${baseUrl}/orders/laundry-services`;
export const LAUNDRY_ORDER_API = `${baseUrl}/laundries`;

// promotion service
// export const GET_PROMOTION_API = `http://api.servrhotelclientdemo.tk`;
// Experience service
export const GET_EXPERIENCE_API = `${baseUrl}/experience/{hotel_id}`;
export const GET_SPECIFIC_PROMOTION_API = `${baseUrl}/experience/`; //details
export const GET_PROMOTION_API = `${baseUrl}/experience/promotion/`;

// export const GET_PROMOTION_API = `https://api.servrhotels.com/experience/promotion/{hotel_id}`;
