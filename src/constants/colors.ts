export default {
    BLACK: '#000000',
    WHITE: '#FFFFFF',
    LIGHT_BLUE: '#72D7FF',
    BLUE: '#5AB9EA',
    DARK_BLUE: '#102133',
    RED: '#FF4B4B',
    DARK_GREY: '#6D6D6D',

    LIGHT_GREY: '#f1f5fb',

    NEON: '#39FF14',

    DARK: '#1D1D35',
    GREY: '#627899',

    BROWN: '#362720',

    VIOLET: '#A42DE7',
};
