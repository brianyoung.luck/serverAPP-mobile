import { createLogic } from 'redux-logic';
import {
    GET_RESTAURANT_LIST,
    GET_RESTAURANT_CATEGORY_DISH,
    BOOK_A_TABLE,
    ORDER_ROOM_SERVICE,
    TGetRestaurantList,
    TGetRestaurantCategoryDish,
    TBookATable,
    TOrderRoomService,
    TRACKING_PROGRESS_ORDER_ROOM_SERVICE,
    TTrackingProgressOrderRoomService,
    DELETE_ORDER,
    TDeleteOrder,
} from '../types/action.restaurant';
import {
    IDependencies,
    ISuccessGetRestaurants,
    ISuccessGetRestaurantDishes,
    ISuccessBookATable,
    ISuccessTrackingProgressOrderRoomService,
    ISuccessDeleteOrder,
} from '../types/responseApi';
import {
    GET_RESTAURANTS,
    GET_RESTAURANT_DISHES,
    CREATE_BOOK_A_TABLE,
    CREATE_ROOM_SERVICE_ORDER,
    TRACKING_PROGRESS_ORDER_ROOM_SERVICE_API,
    DELETE_ORDER_API,
} from '../constants/api';
import { AxiosResponse } from 'axios';
import {
    getRestaurantListSuccess,
    restaurantFailed,
    getRestaurantCategoryDishSuccess,
    bookATableSuccess,
    orderRoomServiceSuccess,
    trackingProgressOrderRoomServiceSuccess,
    deleteOrderSuccess,
} from '../actions/action.restaurant';
import { handleError, IRulesFormValidation, handleFormValidation } from '../utils/handleLogic';
import { printUrl } from '../utils/formating';
import { format } from 'date-fns';
import { Alert } from 'react-native';
import moment from 'moment';

const getRestaurantListLogic = createLogic({
    type: GET_RESTAURANT_LIST,
    process({ httpClient, getState, action }: IDependencies<ReturnType<TGetRestaurantList>>, dispatch, done) {
        httpClient
            .get(GET_RESTAURANTS, {
                headers: {
                    Accept: 'application/json',
                    Authorization: `Bearer ${getState().account.access_token}`,
                    // Authorization: "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9hcGkuc2VydnJob3RlbGNsaWVudGRlbW8udGtcL1wvYm9va2luZ3MiLCJpYXQiOjE1ODMyNDA3NTMsImV4cCI6MTU4MzQ1Mjc5OSwibmJmIjoxNTgzMjQwNzUzLCJqdGkiOiJsZ0JiNWNGT1FMWnpuOVVIIiwic3ViIjoxMTIsInBydiI6IjI4ZDBjNTc2NGQ2MTQzMjcxNGFlOTBmM2I1Yjg0NmFiZmJiOGRiNDMifQ.RejBO9P1tGl--1JNcZLMFAg4YR0qYhLrG9HLCv0GdkY",
                },
            })
            .then((response: AxiosResponse<ISuccessGetRestaurants>) => {
                if (__DEV__) {
                    console.log(`${action.type}: `, response);
                }

                return response.data;
            })
            .then((response) => {
                dispatch(getRestaurantListSuccess(response.data));

                if (action.payload.onSuccess) {
                    action.payload.onSuccess();
                }
            })
            .catch((error) => {
                handleError({
                    error,
                    dispatch,
                    failedAction: restaurantFailed(error, action.type),
                    type: action.type,
                    onFailed: action.payload.onFailed,
                });
            })
            .then(() => done());
    },
});

const getRestaurantCategoryDishLogic = createLogic({
    type: GET_RESTAURANT_CATEGORY_DISH,
    process(
        { httpClient, getState, action }: IDependencies<ReturnType<TGetRestaurantCategoryDish>>,
        dispatch,
        done,
    ) {
        httpClient
            .get(printUrl(GET_RESTAURANT_DISHES, action.payload.id.toString()), {
                headers: {
                    Accept: 'application/json',
                    Authorization: `Bearer ${getState().account.access_token}`,
                    // Authorization: "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9hcGkuc2VydnJob3RlbGNsaWVudGRlbW8udGtcL1wvYm9va2luZ3MiLCJpYXQiOjE1ODMyNDA3NTMsImV4cCI6MTU4MzQ1Mjc5OSwibmJmIjoxNTgzMjQwNzUzLCJqdGkiOiJsZ0JiNWNGT1FMWnpuOVVIIiwic3ViIjoxMTIsInBydiI6IjI4ZDBjNTc2NGQ2MTQzMjcxNGFlOTBmM2I1Yjg0NmFiZmJiOGRiNDMifQ.RejBO9P1tGl--1JNcZLMFAg4YR0qYhLrG9HLCv0GdkY",
                },
            })
            .then((response: AxiosResponse<ISuccessGetRestaurantDishes>) => {
                if (__DEV__) {
                    console.log(`${action.type}: `, response);
                    console.log('1234 response', Object.values(response.data));
                }

                return response.data;
            })
            .then((response) => {
                dispatch(getRestaurantCategoryDishSuccess(response));

                if (action.payload.onSuccess) {
                    action.payload.onSuccess();
                }
            })
            .catch((error) => {
                handleError({
                    error,
                    dispatch,
                    failedAction: restaurantFailed(error, action.type),
                    type: action.type,
                    onFailed: action.payload.onFailed,
                });
            })
            .then(() => done());
    },
});

const bookATableLogic = createLogic({
    type: BOOK_A_TABLE,
    validate({ action }: IDependencies<ReturnType<TBookATable>>, allow, reject) {
        console.log('actioonnnnn=====', action);
        const { time, date, numberPeople, table_no } = action.payload;
        const rules: IRulesFormValidation[] = [
            {
                isValid: time !== '',
                message: 'Please select your booking time',
            },
            {
                isValid: date !== '',
                message: 'Please select your booking date',
            },
            {
                isValid: numberPeople > 0,
                message: 'Number of people must be greater than 0',
            },
            {
                isValid: table_no !== '',
                message: 'Please select your table number',
            },
        ];

        handleFormValidation(
            rules,
            () => allow(action),
            (rule) => {
                Alert.alert('Attention', rule.message);
                if (action.payload.onFailed) {
                    action.payload.onFailed();
                }
                reject(restaurantFailed(rule.message, action.type));
            },
        );
    },
    process({ httpClient, getState, action }: IDependencies<ReturnType<TBookATable>>, dispatch, done) {
        const { date, numberPeople, time, restaurantId, table_no } = action.payload;

        httpClient
            .post(
                printUrl(CREATE_BOOK_A_TABLE, restaurantId.toString()),
                {
                    booking_date: `${format(date, 'YYYY-MM-DD')} ${format(time, 'HH:mm')}`,
                    people_number: numberPeople,
                    current_time: moment().format('YYYY-MM-DD HH:mm:ss'),
                    table_no: table_no,
                },
                {
                    headers: {
                        Accept: 'application/json',
                        Authorization: `Bearer ${getState().account.access_token}`,
                        // Authorization: "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9hcGkuc2VydnJob3RlbGNsaWVudGRlbW8udGtcL1wvYm9va2luZ3MiLCJpYXQiOjE1ODMyNDA3NTMsImV4cCI6MTU4MzQ1Mjc5OSwibmJmIjoxNTgzMjQwNzUzLCJqdGkiOiJsZ0JiNWNGT1FMWnpuOVVIIiwic3ViIjoxMTIsInBydiI6IjI4ZDBjNTc2NGQ2MTQzMjcxNGFlOTBmM2I1Yjg0NmFiZmJiOGRiNDMifQ.RejBO9P1tGl--1JNcZLMFAg4YR0qYhLrG9HLCv0GdkY",
                    },
                },
            )
            .then((response: AxiosResponse<ISuccessBookATable>) => {
                if (__DEV__) {
                    console.log(`${action.type}: `, response);
                }

                return response.data;
            })
            .then((response) => {
                dispatch(bookATableSuccess());

                if (action.payload.onSuccess) {
                    action.payload.onSuccess(response);
                }
            })
            .catch((error) => {
                console.log('errro in book a table', error.response);
                handleError({
                    error,
                    dispatch,
                    failedAction: restaurantFailed(error, action.type),
                    type: action.type,
                    onFailed: action.payload.onFailed,
                });
            })
            .then(() => done());
    },
});

const orderRoomServiceLogic = createLogic({
    type: ORDER_ROOM_SERVICE,
    validate({ action }: IDependencies<ReturnType<TOrderRoomService>>, allow, reject) {
        if (action.payload.items.length <= 0) {
            Alert.alert('Attention', 'At least select one dish');
            if (action.payload.onFailed) {
                action.payload.onFailed();
            }
            return reject(restaurantFailed('At least select one dish', action.type));
        }

        return allow(action);
    },
    process({ httpClient, getState, action }: IDependencies<ReturnType<TOrderRoomService>>, dispatch, done) {
        const { restaurantId, items } = action.payload;

        httpClient
            .post(
                printUrl(CREATE_ROOM_SERVICE_ORDER, restaurantId.toString()),
                { orders: items, current_time: moment().format('YYYY-MM-DD HH:mm:ss') },
                {
                    headers: {
                        Accept: 'application/json',
                        Authorization: `Bearer ${getState().account.access_token}`,
                        // Authorization: "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9hcGkuc2VydnJob3RlbGNsaWVudGRlbW8udGtcL1wvYm9va2luZ3MiLCJpYXQiOjE1ODMyNDA3NTMsImV4cCI6MTU4MzQ1Mjc5OSwibmJmIjoxNTgzMjQwNzUzLCJqdGkiOiJsZ0JiNWNGT1FMWnpuOVVIIiwic3ViIjoxMTIsInBydiI6IjI4ZDBjNTc2NGQ2MTQzMjcxNGFlOTBmM2I1Yjg0NmFiZmJiOGRiNDMifQ.RejBO9P1tGl--1JNcZLMFAg4YR0qYhLrG9HLCv0GdkY",
                    },
                },
            )
            .then((response: AxiosResponse) => {
                if (__DEV__) {
                    console.log(`${action.type}: `, response);
                }

                return response.data;
            })
            .then(() => {
                dispatch(orderRoomServiceSuccess());

                if (action.payload.onSuccess) {
                    action.payload.onSuccess();
                }
            })
            .catch((error) => {
                handleError({
                    error,
                    dispatch,
                    failedAction: restaurantFailed(error, action.type),
                    type: action.type,
                    onFailed: action.payload.onFailed,
                });
            })
            .then(() => done());
    },
});

const trackingProgressOrderRoomServiceLogic = createLogic({
    type: TRACKING_PROGRESS_ORDER_ROOM_SERVICE,
    process(
        { httpClient, getState, action }: IDependencies<ReturnType<TTrackingProgressOrderRoomService>>,
        dispatch,
        done,
    ) {
        httpClient
            .get(TRACKING_PROGRESS_ORDER_ROOM_SERVICE_API, {
                headers: {
                    Accept: 'application/json',
                    Authorization: `Bearer ${getState().account.access_token}`,
                    // Authorization: "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9hcGkuc2VydnJob3RlbGNsaWVudGRlbW8udGtcL1wvYm9va2luZ3MiLCJpYXQiOjE1ODMyNDA3NTMsImV4cCI6MTU4MzQ1Mjc5OSwibmJmIjoxNTgzMjQwNzUzLCJqdGkiOiJsZ0JiNWNGT1FMWnpuOVVIIiwic3ViIjoxMTIsInBydiI6IjI4ZDBjNTc2NGQ2MTQzMjcxNGFlOTBmM2I1Yjg0NmFiZmJiOGRiNDMifQ.RejBO9P1tGl--1JNcZLMFAg4YR0qYhLrG9HLCv0GdkY",
                },
            })
            .then((response: AxiosResponse<ISuccessTrackingProgressOrderRoomService>) => {
                if (__DEV__) {
                    console.log(`${action.type}: `, response);
                }

                return response.data;
            })
            .then((response) => {
                dispatch(trackingProgressOrderRoomServiceSuccess(response.data));

                if (action.payload.onSuccess) {
                    action.payload.onSuccess(response.data);
                }
            })
            .catch((error) => {
                handleError({
                    error,
                    dispatch,
                    failedAction: restaurantFailed(error, action.type),
                    type: action.type,
                    onFailed: action.payload.onFailed,
                });
            })
            .then(() => done());
    },
});

const deleteOrderLogic = createLogic({
    type: DELETE_ORDER,
    process({ httpClient, getState, action }: IDependencies<ReturnType<TDeleteOrder>>, dispatch, done) {
        const { id } = action.payload;

        httpClient
            .delete(printUrl(DELETE_ORDER_API, id.toString()), {
                headers: {
                    Accept: 'application/json',
                    Authorization: `Bearer ${getState().account.access_token}`,
                    // Authorization: "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9hcGkuc2VydnJob3RlbGNsaWVudGRlbW8udGtcL1wvYm9va2luZ3MiLCJpYXQiOjE1ODMyNDA3NTMsImV4cCI6MTU4MzQ1Mjc5OSwibmJmIjoxNTgzMjQwNzUzLCJqdGkiOiJsZ0JiNWNGT1FMWnpuOVVIIiwic3ViIjoxMTIsInBydiI6IjI4ZDBjNTc2NGQ2MTQzMjcxNGFlOTBmM2I1Yjg0NmFiZmJiOGRiNDMifQ.RejBO9P1tGl--1JNcZLMFAg4YR0qYhLrG9HLCv0GdkY",
                },
            })
            .then((response: AxiosResponse<ISuccessDeleteOrder>) => {
                if (__DEV__) {
                    console.log(`${action.type}: `, response);
                }

                console.log('response delete order', response);

                return response.data;
            })
            .then((response) => {
                dispatch(deleteOrderSuccess());

                if (action.payload.onSuccess) {
                    action.payload.onSuccess();
                }
            })
            .catch((error) => {
                console.log('errror delete order', error.response);
                handleError({
                    error,
                    dispatch,
                    failedAction: restaurantFailed(error, action.type),
                    type: action.type,
                    onFailed: action.payload.onFailed,
                });
            })
            .then(() => done());
    },
});

export default [
    getRestaurantListLogic,
    getRestaurantCategoryDishLogic,
    bookATableLogic,
    orderRoomServiceLogic,
    trackingProgressOrderRoomServiceLogic,
    deleteOrderLogic,
];
