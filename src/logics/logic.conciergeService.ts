import { createLogic } from 'redux-logic';
import {
    GET_CONCIERGE_SERVICE_ITEMS,
    CREATE_REQUEST,
    TGetConciergeServiceItems,
    TCreateRequest,
    CONCIERGE_TRACKING_PROGRESS_ORDER_ROOM_SERVICE,
    TGetConciergeTrackingProgress,
    TGetConciergeTrackingProgressSuccess,
    DELETE_CONCIERGE_ORDER,
    TDeleteConciergeOrder,
} from '../types/action.conciergeService';
import {
    CONCIERGE_SERVICE_ITEMS,
    CONCIERGE_CREATE_REQUEST,
    CONCIERGE_TRACKING_PROGRESS_ORDER_ROOM_SERVICE_API,
    DELETE_CONCIERGE_ORDER_API,
} from '../constants/api';
import {
    IDependencies,
    ISuccessConciergeServiceItems,
    ISuccessCreateRequest,
    TSuccessConciergeTrackingProgressSuccess,
    ISuccessDeleteOrder,
} from '../types/responseApi';
import { AxiosResponse } from 'axios';
import {
    getConciergeServiceItemsSuccess,
    conciergeServiceFailed,
    createRequestSuccess,
    conciergeTrackingProgressOrderRoomServiceSuccess,
    deleteConciergeOrderSuccess,
} from '../actions/action.conciergeService';
import { handleError, IRulesFormValidation, handleFormValidation, toast } from '../utils/handleLogic';
import moment from 'moment';
import { printUrl } from '../utils/formating';

const getConciergeServiceItemsLogic = createLogic({
    type: GET_CONCIERGE_SERVICE_ITEMS,
    process(
        { httpClient, getState, action }: IDependencies<ReturnType<TGetConciergeServiceItems>>,
        dispatch,
        done,
    ) {
        httpClient
            .get(CONCIERGE_SERVICE_ITEMS, {
                headers: {
                    Accept: 'application/json',
                    Authorization: `Bearer ${getState().account.access_token}`,
                    // Authorization: "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9hcGkuc2VydnJob3RlbGNsaWVudGRlbW8udGtcL1wvYm9va2luZ3MiLCJpYXQiOjE1ODMyNDA3NTMsImV4cCI6MTU4MzQ1Mjc5OSwibmJmIjoxNTgzMjQwNzUzLCJqdGkiOiJsZ0JiNWNGT1FMWnpuOVVIIiwic3ViIjoxMTIsInBydiI6IjI4ZDBjNTc2NGQ2MTQzMjcxNGFlOTBmM2I1Yjg0NmFiZmJiOGRiNDMifQ.RejBO9P1tGl--1JNcZLMFAg4YR0qYhLrG9HLCv0GdkY",
                },
            })
            .then((response: AxiosResponse<ISuccessConciergeServiceItems>) => {
                if (__DEV__) {
                    console.log(`${action.type}: `, response);
                }

                return response.data;
            })
            .then((response) => {
                dispatch(getConciergeServiceItemsSuccess(response));

                if (action.payload.onSuccess) {
                    action.payload.onSuccess();
                }
            })
            .catch((error) => {
                handleError({
                    error,
                    dispatch,
                    failedAction: conciergeServiceFailed(error, action.type),
                    type: action.type,
                    onFailed: action.payload.onFailed,
                });
            })
            .then(() => done());
    },
});

const createRequestLogic = createLogic({
    type: CREATE_REQUEST,
    validate({ action }: IDependencies<ReturnType<TCreateRequest>>, allow, reject) {
        const rules: IRulesFormValidation[] = [
            { isValid: action.payload.items.length > 0, message: 'At least select one service' },
        ];

        handleFormValidation(
            rules,
            () => allow(action),
            (rule) => {
                toast(rule.message, 'Attention');
                if (action.payload.onFailed) {
                    action.payload.onFailed();
                }
                reject(conciergeServiceFailed(rule.message, action.type));
            },
        );
    },
    process({ httpClient, getState, action }: IDependencies<ReturnType<TCreateRequest>>, dispatch, done) {
        httpClient
            .post(
                CONCIERGE_CREATE_REQUEST,
                {
                    concierge_services: action.payload.items,
                    current_time: moment().format('YYYY-MM-DD HH:mm:ss'),
                },
                {
                    headers: {
                        Accept: 'application/json',
                        Authorization: `Bearer ${getState().account.access_token}`,
                        // Authorization: "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9hcGkuc2VydnJob3RlbGNsaWVudGRlbW8udGtcL1wvYm9va2luZ3MiLCJpYXQiOjE1ODMyNDA3NTMsImV4cCI6MTU4MzQ1Mjc5OSwibmJmIjoxNTgzMjQwNzUzLCJqdGkiOiJsZ0JiNWNGT1FMWnpuOVVIIiwic3ViIjoxMTIsInBydiI6IjI4ZDBjNTc2NGQ2MTQzMjcxNGFlOTBmM2I1Yjg0NmFiZmJiOGRiNDMifQ.RejBO9P1tGl--1JNcZLMFAg4YR0qYhLrG9HLCv0GdkY",
                    },
                },
            )
            .then((response: AxiosResponse<ISuccessCreateRequest>) => {
                if (__DEV__) {
                    console.log(`${action.type}: `, response);
                }
                console.log('data', action.payload.items);
                console.log('request item', response);

                return response.data;
            })
            .then((response) => {
                dispatch(createRequestSuccess());

                if (action.payload.onSuccess) {
                    action.payload.onSuccess(response.message);
                }
            })
            .catch((error) => {
                console.log('error in request item', error.response);
                handleError({
                    error,
                    dispatch,
                    failedAction: conciergeServiceFailed(error, action.type),
                    type: action.type,
                    onFailed: action.payload.onFailed,
                });
            })
            .then(() => done());
    },
});

const getConciergeTrackingProgressLogic = createLogic({
    type: CONCIERGE_TRACKING_PROGRESS_ORDER_ROOM_SERVICE,
    process(
        { httpClient, getState, action }: IDependencies<ReturnType<TGetConciergeTrackingProgress>>,
        dispatch,
        done,
    ) {
        httpClient
            .get(CONCIERGE_TRACKING_PROGRESS_ORDER_ROOM_SERVICE_API, {
                headers: {
                    Accept: 'application/json',
                    Authorization: `Bearer ${getState().account.access_token}`,
                    // Authorization: "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9hcGkuc2VydnJob3RlbGNsaWVudGRlbW8udGtcL1wvYm9va2luZ3MiLCJpYXQiOjE1ODMyNDA3NTMsImV4cCI6MTU4MzQ1Mjc5OSwibmJmIjoxNTgzMjQwNzUzLCJqdGkiOiJsZ0JiNWNGT1FMWnpuOVVIIiwic3ViIjoxMTIsInBydiI6IjI4ZDBjNTc2NGQ2MTQzMjcxNGFlOTBmM2I1Yjg0NmFiZmJiOGRiNDMifQ.RejBO9P1tGl--1JNcZLMFAg4YR0qYhLrG9HLCv0GdkY",
                },
            })
            .then((response: AxiosResponse<TSuccessConciergeTrackingProgressSuccess>) => {
                if (__DEV__) {
                    console.log(`${action.type}: `, response);
                }

                console.log('response get conceirge order', response);

                return response.data;
            })
            .then((response) => {
                dispatch(conciergeTrackingProgressOrderRoomServiceSuccess(response));

                if (action.payload.onSuccess) {
                    action.payload.onSuccess();
                }
            })
            .catch((error) => {
                console.log('error get conceirge order', error.response);

                handleError({
                    error,
                    dispatch,
                    failedAction: conciergeServiceFailed(error, action.type),
                    type: action.type,
                    onFailed: action.payload.onFailed,
                });
            })
            .then(() => done());
    },
});

const deleteConciergeOrderLogic = createLogic({
    type: DELETE_CONCIERGE_ORDER,
    process(
        { httpClient, getState, action }: IDependencies<ReturnType<TDeleteConciergeOrder>>,
        dispatch,
        done,
    ) {
        const { id, type } = action.payload;
        console.log('data', id, type);
        httpClient
            .delete(printUrl(DELETE_CONCIERGE_ORDER_API, [id.toString(), type.toString()]), {
                headers: {
                    Accept: 'application/json',
                    Authorization: `Bearer ${getState().account.access_token}`,
                    // Authorization: "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9hcGkuc2VydnJob3RlbGNsaWVudGRlbW8udGtcL1wvYm9va2luZ3MiLCJpYXQiOjE1ODMyNDA3NTMsImV4cCI6MTU4MzQ1Mjc5OSwibmJmIjoxNTgzMjQwNzUzLCJqdGkiOiJsZ0JiNWNGT1FMWnpuOVVIIiwic3ViIjoxMTIsInBydiI6IjI4ZDBjNTc2NGQ2MTQzMjcxNGFlOTBmM2I1Yjg0NmFiZmJiOGRiNDMifQ.RejBO9P1tGl--1JNcZLMFAg4YR0qYhLrG9HLCv0GdkY",
                },
            })
            .then((response: AxiosResponse<ISuccessDeleteOrder>) => {
                if (__DEV__) {
                    console.log(`${action.type}: `, response);
                }

                console.log('response delete concierge order', response);

                return response.data;
            })
            .then((response) => {
                dispatch(deleteConciergeOrderSuccess());

                if (action.payload.onSuccess) {
                    action.payload.onSuccess();
                }
            })
            .catch((error) => {
                console.log('errror delete concierge order', error.response);
                // handleError({
                //     error,
                //     dispatch,
                //     failedAction: restaurantFailed(error, action.type),
                //     type: action.type,
                //     onFailed: action.payload.onFailed,
                // });
            })
            .then(() => done());
    },
});

export default [
    getConciergeServiceItemsLogic,
    createRequestLogic,
    getConciergeTrackingProgressLogic,
    deleteConciergeOrderLogic,
];
