import { createLogic } from 'redux-logic';
import {
    IDependencies,
    ISuccessGetMe,
    ISuccessCheckIn,
    IFailedResponse,
    ISuccessLogin,
} from '../types/responseApi';
import {
    CHECK_IN,
    CHECK_OUT,
    GET_PROFILE,
    VERIFY_PHONE_NUMBER,
    VERIFY_PIN,
    TCheckIn,
    TCheckOut,
    TGetProfile,
    TVerifyPhoneNumber,
    TVerifyPin,
    LOGIN,
    TLogin,
    ON_AUTH_STATE_CHANGED,
    TOnAuthStateChanged,
    ON_AUTH_STATE_CHANGED_SUCCESS,
    REMOVE_ON_AUTH_STATE_CHANGED,
    TLateCheckOut,
    LATE_CHECK_OUT,
    TUserCheckOut,
    USER_CHECK_OUT,
} from '../types/action.account';
import {
    checkOutSuccess,
    accountFailed,
    getProfileSuccess,
    checkInSuccess,
    verifyPhoneNumberSuccess,
    verifyPinSuccess,
    loginSuccess,
    onAuthStateChangedSuccess,
    lateCheckOutSuccess,
    getProfile,
    userCheckOutSuccess,
} from '../actions/action.account';
import { handleError, IRulesFormValidation, handleFormValidation, toast } from '../utils/handleLogic';
import { ME, BOOKING, LOGIN_CUSTOMER, LATE_CHECKOUT, USER_CHECKOUT } from '../constants/api';
import { AxiosResponse, AxiosError } from 'axios';
import { forEach, isArray } from 'lodash';
import { isAfter } from 'date-fns';
import { connectSendBird } from '../actions/action.chat';
import { Alert } from 'react-native';
import { Navigation } from 'react-native-navigation';
import { pickHotel } from '../utils/navigationControl';

const checkInLogic = createLogic({
    type: CHECK_IN,
    validate({ action }: IDependencies<ReturnType<TCheckIn>>, allow, reject) {
        const {
            passport_photos,
            arrival_date,
            departure_date,
            cardholder_name,
            card_expiry_date,
            card_address,
            phone_number,
            card_number,
            reference,
            signature_photo,
            terms_and_condition,
        } = action.payload.data;
        console.log('acion payload', action.payload);
        // validation
        const rules: IRulesFormValidation[] = [
            {
                isValid: !passport_photos.map((photo) => photo.uri).includes(''),
                message: 'Please upload all passport photos',
            },
            {
                isValid: arrival_date !== '',
                message: 'Please enter the arrival date',
            },
            {
                isValid: departure_date !== '',
                message: 'Please enter the departure date',
            },
            {
                isValid: isAfter(departure_date, arrival_date),
                message: 'Arrival date must be before departure date',
            },
            {
                isValid: cardholder_name !== '',
                message: 'Please enter the cardholder name',
            },
            {
                isValid: card_number !== '',
                message: 'Please enter the card number',
            },
            {
                isValid: card_number.length === 16,
                message: 'Card number must be exactly 16 digit',
            },
            {
                isValid: /[0-9]/gi.test(card_number),
                message: 'Card number must be all numeric',
            },
            {
                isValid: card_expiry_date !== '',
                message: 'Please enter the card expiry date',
            },
            {
                isValid: card_address !== '',
                message: 'Please enter the card address',
            },
            {
                isValid: reference !== '',
                message: 'Please enter the booking reference',
            },
            {
                isValid: phone_number !== '',
                message: 'Please enter the phone number',
            },
            {
                isValid: signature_photo.name,
                message: 'Your signature is required',
            },
            {
                isValid: terms_and_condition,
                message: 'You must have accept terms and condition',
            },
        ];

        handleFormValidation(
            rules,
            () => allow(action),
            (rule) => {
                toast(rule.message, 'Attention');
                if (action.payload.onFailed) {
                    action.payload.onFailed();
                }
                reject(accountFailed(rule.message, CHECK_IN));
            },
        );
    },
    process({ httpClient, action }: IDependencies<ReturnType<TCheckIn>>, dispatch, done) {
        // transform to form data
        const formData = new FormData();
        forEach(action.payload.data, (value, key) => {
            console.log('value', value, key);
            // un-comment this to skip card_number being send to server
            // if (<keyof typeof action.payload.data>key === 'card_number') {
            //     return false;
            // }

            if (isArray(value)) {
                value.forEach((photo) => formData.append(`${key}[]`, photo));
            } else {
                formData.append(key, value);
            }
        });

        console.log('form data', formData);

        // send it
        httpClient
            .post(BOOKING, formData, {
                headers: {
                    Accept: 'application/json',
                },
            })
            .then((response: AxiosResponse<ISuccessCheckIn>) => {
                if (__DEV__) {
                    console.log(`${action.type}: `, response);
                }
                console.log('CHECK IN RESPONSE ===', response);
                return response.data;
            })
            .then((response) => {
                dispatch(
                    checkInSuccess(response.data.token, {
                        ...response.data.booking,
                        card_number: action.payload.data.card_number,
                    }),
                );

                // give sometime to store previous dispatch
                // i believe, the operation is async
                // edit: not firing, if wrapped in settimeout
                dispatch(connectSendBird());

                if (action.payload.onSuccess) {
                    action.payload.onSuccess();
                }
            })
            .catch((error) => {
                console.log('error checkin', error.response);
                dispatch(accountFailed(error, CHECK_IN));

                if (__DEV__) {
                    console.log(`${action.type}: `, { error });
                }

                const { message, errors } = <IFailedResponse>error.response.data;
                let msg = message;
                if (errors) {
                    const key = Object.keys(errors);
                    msg = errors[key[0]][0];
                    if (msg == 'The phone number format is invalid.') {
                        msg = 'Please enter your registered phone number.';
                    }
                }

                toast(msg, 'Error');

                if (action.payload.onFailed) {
                    action.payload.onFailed();
                }
            })
            .then(() => done());
    },
});

const checkOutLogic = createLogic({
    type: CHECK_OUT,
    validate({ action, getState }: IDependencies<ReturnType<TCheckOut>>, allow, reject) {
        if (!getState().account.isCheckedIn) {
            return reject(accountFailed('already checkout', action.type));
        }

        return allow(action);
    },
    async process({ action }: IDependencies<ReturnType<TCheckOut>>, dispatch, done) {
        dispatch(checkOutSuccess());

        await Navigation.setRoot({ root: pickHotel });

        if (action.payload.onSuccess) {
            action.payload.onSuccess();
        }

        Alert.alert('Attention', 'You already checkout. Thank you for using our service, Have a nice day.');

        done();
    },
});

const lateCheckOutLogic = createLogic({
    type: LATE_CHECK_OUT,
    process({ action, httpClient, getState }: IDependencies<ReturnType<TUserCheckOut>>, dispatch, done) {
        httpClient
            .post(
                LATE_CHECKOUT,
                {},
                {
                    headers: {
                        Accept: 'application/json',
                        Authorization: `Bearer ${getState().account.access_token}`,
                        // Authorization: "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9hcGkuc2VydnJob3RlbGNsaWVudGRlbW8udGtcL1wvYm9va2luZ3MiLCJpYXQiOjE1ODMyNDA3NTMsImV4cCI6MTU4MzQ1Mjc5OSwibmJmIjoxNTgzMjQwNzUzLCJqdGkiOiJsZ0JiNWNGT1FMWnpuOVVIIiwic3ViIjoxMTIsInBydiI6IjI4ZDBjNTc2NGQ2MTQzMjcxNGFlOTBmM2I1Yjg0NmFiZmJiOGRiNDMifQ.RejBO9P1tGl--1JNcZLMFAg4YR0qYhLrG9HLCv0GdkY",
                    },
                },
            )
            .then((response: AxiosResponse<any>) => {
                if (__DEV__) {
                    console.log(`${action.type}: `, response);
                }

                return response.data;
            })
            .then(() => {
                dispatch(lateCheckOutSuccess());
                dispatch(getProfile(action.payload.onSuccess, action.payload.onFailed));
            })
            .catch((error) => {
                handleError({
                    error,
                    dispatch,
                    failedAction: accountFailed(error, action.type),
                    type: action.type,
                    onFailed: action.payload.onFailed,
                });
            })
            .then(() => done());
    },
});

const getProfileLogic = createLogic({
    type: GET_PROFILE,
    process({ httpClient, action, getState }: IDependencies<ReturnType<TGetProfile>>, dispatch, done) {
        httpClient
            .get(ME, {
                headers: {
                    Accept: 'application/json',
                    Authorization: `Bearer ${getState().account.access_token}`,
                    // Authorization: "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9hcGkuc2VydnJob3RlbGNsaWVudGRlbW8udGtcL1wvYm9va2luZ3MiLCJpYXQiOjE1ODMyNDA3NTMsImV4cCI6MTU4MzQ1Mjc5OSwibmJmIjoxNTgzMjQwNzUzLCJqdGkiOiJsZ0JiNWNGT1FMWnpuOVVIIiwic3ViIjoxMTIsInBydiI6IjI4ZDBjNTc2NGQ2MTQzMjcxNGFlOTBmM2I1Yjg0NmFiZmJiOGRiNDMifQ.RejBO9P1tGl--1JNcZLMFAg4YR0qYhLrG9HLCv0GdkY",
                },
            })
            .then((response: AxiosResponse<ISuccessGetMe>) => {
                if (__DEV__) {
                    console.log(`${action.type}: `, response);
                }
                // console.log("printing response of getProfile",response)
                return response.data;
            })
            .then((response) => {
                console.log(response, 'get profile');
                dispatch(getProfileSuccess(response.data));

                if (action.payload.onSuccess) {
                    action.payload.onSuccess();
                }
            })
            .catch((error: AxiosError) => {
                console.log(error.response, 'error get profile');
                handleError({
                    error,
                    dispatch,
                    failedAction: accountFailed(error, GET_PROFILE),
                    type: GET_PROFILE,
                    onFailed: action.payload.onFailed,
                    displayMessage: action.payload.onFailed !== undefined,
                });
            })
            .then(() => done());
    },
});

const verifyPhoneLogic = createLogic({
    type: VERIFY_PHONE_NUMBER,
    validate({ action }: IDependencies<ReturnType<TVerifyPhoneNumber>>, allow, reject) {
        const rules: IRulesFormValidation[] = [
            {
                isValid: action.payload.phoneNumber !== '',
                message: 'Please enter the phone number',
            },
        ];

        handleFormValidation(
            rules,
            () => allow(action),
            (rule) => {
                toast(rule.message, 'Attention');
                if (action.payload.onFailed) {
                    action.payload.onFailed();
                }
                reject(accountFailed(rule.message, action.type));
            },
        );
    },
    process({ action, firebase }: IDependencies<ReturnType<TVerifyPhoneNumber>>, dispatch, done) {
        firebase
            .auth()
            .signInWithPhoneNumber(action.payload.phoneNumber)
            .then((confirmationResult) => {
                dispatch(verifyPhoneNumberSuccess(confirmationResult));

                if (action.payload.onSuccess) {
                    action.payload.onSuccess(confirmationResult);
                }
            })
            .catch((error) => {
                handleError({
                    error,
                    dispatch,
                    failedAction: accountFailed(error, action.type),
                    type: action.type,
                    onFailed: action.payload.onFailed,
                });
            })
            .then(() => done());
    },
});

const verifyPinLogic = createLogic({
    type: VERIFY_PIN,
    validate({ action, getState }: IDependencies<ReturnType<TVerifyPin>>, allow, reject) {
        const rules: IRulesFormValidation[] = [
            {
                isValid: action.payload.pin !== '',
                message: 'PIN is required',
            },
            {
                isValid: action.payload.pin.length === 6,
                message: 'PIN must be 6 digit',
            },
            {
                isValid: /[0-9]/gi.test(action.payload.pin),
                message: 'PIN must be numeric',
            },
            {
                isValid: getState().account.confirmationResult !== null,
                message: 'Something went wrong',
            },
        ];

        handleFormValidation(
            rules,
            () => allow(action),
            (rule) => {
                toast(rule.message, 'Attention');
                if (action.payload.onFailed) {
                    action.payload.onFailed();
                }
                reject(accountFailed(rule.message, action.type));
            },
        );
    },
    process({ getState, action }: IDependencies<ReturnType<TVerifyPin>>, dispatch, done) {
        const { confirmationResult } = getState().account;

        if (confirmationResult) {
            confirmationResult
                .confirm(action.payload.pin)
                .then((userResult) => {
                    if (userResult) {
                        return userResult.getIdToken();
                    }

                    return '';
                })
                .then((token) => {
                    if (token !== '') {
                        dispatch(verifyPinSuccess());

                        if (action.payload.onSuccess) {
                            action.payload.onSuccess(token);
                        }

                        return true;
                    }

                    if (action.payload.onFailed) {
                        action.payload.onFailed();
                    }
                })
                .catch((error) => {
                    handleError({
                        error,
                        dispatch,
                        failedAction: accountFailed(error, action.type),
                        type: action.type,
                        onFailed: action.payload.onFailed,
                    });
                })
                .then(() => done());
        }
    },
});

const loginLogic = createLogic({
    type: LOGIN,
    process({ httpClient, action }: IDependencies<ReturnType<TLogin>>, dispatch, done) {
        httpClient
            .post(
                LOGIN_CUSTOMER,
                { token: action.payload.token },
                {
                    headers: {
                        Accept: 'application/json',
                    },
                },
            )
            .then((response: AxiosResponse<ISuccessLogin>) => {
                if (__DEV__) {
                    console.log(`${action.type}: `, { response });
                }

                return response.data;
            })
            .then(({ data }) => {
                dispatch(loginSuccess(data.token, data.booking));

                if (action.payload.onSuccess) {
                    action.payload.onSuccess(data.token, data.booking);
                }
            })
            .catch((error) => {
                handleError({
                    error,
                    dispatch,
                    failedAction: accountFailed(error, action.type),
                    type: action.type,
                    onFailed: action.payload.onFailed,
                });
            })
            .then(() => done());
    },
});

const onAuthStateChangedLogic = createLogic({
    type: ON_AUTH_STATE_CHANGED,
    cancelType: [REMOVE_ON_AUTH_STATE_CHANGED, ON_AUTH_STATE_CHANGED_SUCCESS],
    warnTimeout: 0,
    process(
        { cancelled$, firebase, action }: IDependencies<ReturnType<TOnAuthStateChanged>>,
        dispatch,
        done,
    ) {
        const onAuthFirebase = firebase.auth().onAuthStateChanged((authSnapshot) => {
            if (authSnapshot) {
                authSnapshot
                    .getIdToken()
                    .then((token) => {
                        if (action.payload.onCallback) {
                            action.payload.onCallback(token);
                        }

                        dispatch(onAuthStateChangedSuccess());
                    })
                    .catch((error) => {
                        handleError({
                            error,
                            dispatch,
                            failedAction: accountFailed(error, action.type),
                            type: action.type,
                            displayMessage: false,
                        });
                    });
            }
        });

        // cancelation
        cancelled$.subscribe(() => {
            onAuthFirebase();
            done();
        });
    },
});

const userCheckOutLogic = createLogic({
    type: USER_CHECK_OUT,
    process({ action, httpClient, getState }: IDependencies<ReturnType<TUserCheckOut>>, dispatch, done) {
        httpClient
            .post(
                USER_CHECKOUT,
                {},
                {
                    headers: {
                        Accept: 'application/json',
                        Authorization: `Bearer ${getState().account.access_token}`,
                        // Authorization: "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9hcGkuc2VydnJob3RlbGNsaWVudGRlbW8udGtcL1wvYm9va2luZ3MiLCJpYXQiOjE1ODMyNDA3NTMsImV4cCI6MTU4MzQ1Mjc5OSwibmJmIjoxNTgzMjQwNzUzLCJqdGkiOiJsZ0JiNWNGT1FMWnpuOVVIIiwic3ViIjoxMTIsInBydiI6IjI4ZDBjNTc2NGQ2MTQzMjcxNGFlOTBmM2I1Yjg0NmFiZmJiOGRiNDMifQ.RejBO9P1tGl--1JNcZLMFAg4YR0qYhLrG9HLCv0GdkY",
                    },
                },
            )
            .then((response: AxiosResponse<any>) => {
                if (__DEV__) {
                    console.log(`${action.type}: `, response);
                }

                console.log('check outt===========', response);

                return response.data;
            })
            .then(() => {
                dispatch(userCheckOutSuccess());
                dispatch(getProfile(action.payload.onSuccess, action.payload.onFailed));
            })
            .catch((error) => {
                console.log('check outt errorrrrrr===========', error.response);

                handleError({
                    error,
                    dispatch,
                    failedAction: accountFailed(error, action.type),
                    type: action.type,
                    onFailed: action.payload.onFailed,
                });
            })
            .then(() => done());
    },
});

export default [
    checkInLogic,
    checkOutLogic,
    lateCheckOutLogic,
    getProfileLogic,
    verifyPhoneLogic,
    verifyPinLogic,
    loginLogic,
    onAuthStateChangedLogic,
    userCheckOutLogic,
];
