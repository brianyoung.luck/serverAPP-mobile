import { createLogic } from 'redux-logic';
import {
    ROOM_CLEANING_SERVICE,
    LAUNDRY_ORDER,
    laundryOrder,
    roomCleaningService,
    roomCleaningServiceSuccess,
    cleaningServiceFailed,
    laundryOrderSuccess,
    GET_LAUNDRY_SERVICES_MENU,
    getLaundryServicesMenu,
    getLaundryServicesMenuSuccess,
} from '../actions/action.cleaningService';
import {
    IDependencies,
    ISuccessRoomCleaningService,
    ISuccessLaundryOrder,
    ISuccessGetLaundriesMenu,
} from '../types/responseApi';
import { LAUNDRY_SERVICES_MENU_API, ROOM_CLEANING_SERVICE_API, LAUNDRY_ORDER_API } from '../constants/api';
import { AxiosResponse } from 'axios';
import { handleError } from '../utils/handleLogic';
import { Alert } from 'react-native';
import moment from 'moment';

const getLaundryServicesMenuLogic = createLogic({
    type: GET_LAUNDRY_SERVICES_MENU,
    process(
        { httpClient, getState, action }: IDependencies<ReturnType<typeof getLaundryServicesMenu>>,
        dispatch,
        done,
    ) {
        httpClient
            .get(LAUNDRY_SERVICES_MENU_API, {
                headers: {
                    Accept: 'application/json',
                    Authorization: `Bearer ${getState().account.access_token}`,
                    // Authorization: "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9hcGkuc2VydnJob3RlbGNsaWVudGRlbW8udGtcL1wvYm9va2luZ3MiLCJpYXQiOjE1ODMyNDA3NTMsImV4cCI6MTU4MzQ1Mjc5OSwibmJmIjoxNTgzMjQwNzUzLCJqdGkiOiJsZ0JiNWNGT1FMWnpuOVVIIiwic3ViIjoxMTIsInBydiI6IjI4ZDBjNTc2NGQ2MTQzMjcxNGFlOTBmM2I1Yjg0NmFiZmJiOGRiNDMifQ.RejBO9P1tGl--1JNcZLMFAg4YR0qYhLrG9HLCv0GdkY",
                },
            })
            .then((response: AxiosResponse<ISuccessGetLaundriesMenu>) => {
                if (__DEV__) {
                    console.log(`${action.type}: `, response);
                }

                return response.data;
            })
            .then((response) => {
                dispatch(
                    getLaundryServicesMenuSuccess({
                        ...response.data,
                        // logo_url: [response.data.logo_url as any, response.data.logo_url as any],
                        laundries: response,
                    }),
                );

                // if (action.payload.onSuccess) {
                //     action.payload.onSuccess(response.data);
                // }
            })
            .catch((error) => {
                // handleError({
                //     error,
                //     dispatch,
                //     failedAction: spaFailed(error, action.type),
                //     type: action.type,
                //     onFailed: action.payload.onFailed,
                // });
                console.log('ERROR:::', error);
            })
            .then(() => {
                done();
            });
    },
});

/////////////////////////////

const roomCleaningServiceLogic = createLogic({
    type: ROOM_CLEANING_SERVICE,
    process(
        { httpClient, action, getState }: IDependencies<ReturnType<typeof roomCleaningService>>,
        dispatch,
        done,
    ) {
        httpClient
            .post(
                ROOM_CLEANING_SERVICE_API,
                {
                    current_time: moment().format('YYYY-MM-DD HH:mm:ss'),
                },
                {
                    headers: {
                        Accept: 'application/json',
                        Authorization: `Bearer ${getState().account.access_token}`,
                        // Authorization: "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9hcGkuc2VydnJob3RlbGNsaWVudGRlbW8udGtcL1wvYm9va2luZ3MiLCJpYXQiOjE1ODMyNDA3NTMsImV4cCI6MTU4MzQ1Mjc5OSwibmJmIjoxNTgzMjQwNzUzLCJqdGkiOiJsZ0JiNWNGT1FMWnpuOVVIIiwic3ViIjoxMTIsInBydiI6IjI4ZDBjNTc2NGQ2MTQzMjcxNGFlOTBmM2I1Yjg0NmFiZmJiOGRiNDMifQ.RejBO9P1tGl--1JNcZLMFAg4YR0qYhLrG9HLCv0GdkY",
                    },
                },
            )
            .then((response: AxiosResponse<ISuccessRoomCleaningService>) => {
                if (__DEV__) {
                    console.log(`${action.type}: `, response);
                }
                return response.data;
            })
            .then(() => {
                dispatch(roomCleaningServiceSuccess());

                if (action.payload.onSuccess) {
                    action.payload.onSuccess();
                }
            })
            .catch((error) => {
                console.log('error room cleaning sevices', error.response);
                handleError({
                    error,
                    dispatch,
                    failedAction: cleaningServiceFailed(error, action.type),
                    type: action.type,
                    onFailed: action.payload.onFailed,
                });
            })
            .then(() => done());
    },
});

const laundryOrderLogic = createLogic({
    type: LAUNDRY_ORDER,
    validate({ action }: IDependencies<ReturnType<any>>, allow, reject) {
        if (action.payload.items.length <= 0) {
            Alert.alert('Attention', 'At least select one service');
            if (action.payload.onFailed) {
                action.payload.onFailed();
            }
            return reject(cleaningServiceFailed('At least select one service', action.type));
        }

        return allow(action);
    },
    process(
        { httpClient, action, getState }: IDependencies<ReturnType<typeof laundryOrder>>,
        dispatch,
        done,
    ) {
        httpClient
            .post(
                LAUNDRY_ORDER_API,
                {
                    laundry_orders: action.payload.items,
                    current_time: moment().format('YYYY-MM-DD HH:mm:ss'),
                },
                {
                    headers: {
                        Accept: 'application/json',
                        Authorization: `Bearer ${getState().account.access_token}`,
                        // Authorization: "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9hcGkuc2VydnJob3RlbGNsaWVudGRlbW8udGtcL1wvYm9va2luZ3MiLCJpYXQiOjE1ODMyNDA3NTMsImV4cCI6MTU4MzQ1Mjc5OSwibmJmIjoxNTgzMjQwNzUzLCJqdGkiOiJsZ0JiNWNGT1FMWnpuOVVIIiwic3ViIjoxMTIsInBydiI6IjI4ZDBjNTc2NGQ2MTQzMjcxNGFlOTBmM2I1Yjg0NmFiZmJiOGRiNDMifQ.RejBO9P1tGl--1JNcZLMFAg4YR0qYhLrG9HLCv0GdkY",
                    },
                },
            )
            .then((response: AxiosResponse<ISuccessLaundryOrder>) => {
                if (__DEV__) {
                    console.log(`${action.type}: `, response);
                }

                return response.data;
            })
            .then(() => {
                dispatch(laundryOrderSuccess());

                if (action.payload.onSuccess) {
                    action.payload.onSuccess();
                }
            })
            .catch((error) => {
                console.log('error laundires', error.response);
                handleError({
                    error,
                    dispatch,
                    failedAction: cleaningServiceFailed(error, action.type),
                    type: action.type,
                    onFailed: action.payload.onFailed,
                });
            })
            .then(() => done());
    },
});

export default [roomCleaningServiceLogic, laundryOrderLogic, getLaundryServicesMenuLogic];
