import { createLogic } from 'redux-logic';
import { GET_HOTEL_DETAIL, TGetHotelDetail } from '../types/action.hotel';
import { IDependencies, ISuccessGetHotelDetail } from '../types/responseApi';
import { printUrl } from '../utils/formating';
import { HOTEL_DETAIL } from '../constants/api';
import { AxiosResponse } from 'axios';
import { getHotelDetailSuccess, hotelFailed } from '../actions/action.hotel';
import { handleError, IRulesFormValidation, handleFormValidation, toast } from '../utils/handleLogic';

const getHotelDetail = createLogic({
    type: GET_HOTEL_DETAIL,
    validate({ action }: IDependencies<ReturnType<TGetHotelDetail>>, allow, reject) {
        const { code } = action.payload;

        // validation
        const rules: IRulesFormValidation[] = [
            {
                isValid: code !== '',
                message: 'Code hotel is required',
            },
            {
                isValid: /^[a-zA-Z0-9]*$/gi.test(code),
                message: 'Code hotel must be alphabet or number',
            },
        ];

        handleFormValidation(
            rules,
            () => allow(action),
            (rule) => {
                toast(rule.message, 'Attention');
                if (action.payload.onFailed) {
                    action.payload.onFailed();
                }
                reject(hotelFailed(rule.message, GET_HOTEL_DETAIL));
            },
        );
    },
    process({ httpClient, action }: IDependencies<ReturnType<TGetHotelDetail>>, dispatch, done) {
        httpClient
            .get(printUrl(HOTEL_DETAIL, action.payload.code), {
                headers: {
                    Accept: 'application/json',
                },
            })
            .then((response: AxiosResponse<ISuccessGetHotelDetail>) => {
                if (__DEV__) {
                    console.log(`${action.type}: `, response);
                }

                return response.data;
            })
            .then((response) => {
                console.log(response, 'get hotel detail');
                const {
                    id,
                    description,
                    code,
                    layout,
                    hotel_features,
                    name,
                    category,
                    currency,
                    mobile_hotel_layout_id,
                    mobile_hotel_layouts,
                    ...logo
                } = response.data;

                dispatch(
                    getHotelDetailSuccess(
                        id,
                        code,
                        description,
                        name,
                        logo,
                        layout.theme,
                        layout.icons,
                        hotel_features,
                        category,
                        currency,
                        mobile_hotel_layout_id,
                        mobile_hotel_layouts,
                    ),
                );
                if (action.payload.onSuccess) {
                    action.payload.onSuccess();
                }
            })
            .catch((error) => {
                console.log(error.response, 'error get hotel detail');
                handleError({
                    error,
                    dispatch,
                    failedAction: hotelFailed(error, GET_HOTEL_DETAIL),
                    type: GET_HOTEL_DETAIL,
                    onFailed: action.payload.onFailed,
                });
            })
            .then(() => done());
    },
});

export default [getHotelDetail];
