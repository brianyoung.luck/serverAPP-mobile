import { createLogic } from 'redux-logic';
import {
    GET_PROMOTION,
    getPromotion,
    getPromotionSuccess,
    GET_PROMOTION_DETAILS,
    getPromotionDetailsSuccess,
    getPromotionDetails,
} from '../actions/action.promotion';
import { IDependencies, ISuccessGetPromotion, ISuccessGetPromotionDetails } from '../types/responseApi';
import { GET_PROMOTION_API, GET_SPECIFIC_PROMOTION_API, GET_EXPERIENCE_API } from '../constants/api';
import { AxiosResponse } from 'axios';
import { handleError, IRulesFormValidation, handleFormValidation } from '../utils/handleLogic';
import { printUrl } from '../utils/formating';
import { format } from 'date-fns';
import { Alert } from 'react-native';

const getPromotionLogic = createLogic({
    type: GET_PROMOTION,
    process(
        { httpClient, getState, action }: IDependencies<ReturnType<typeof getPromotion>>,
        dispatch,
        done,
    ) {
        httpClient
            .get(GET_PROMOTION_API + action.payload.idButton.toString(), {
                headers: {
                    Accept: 'application/json',
                    Authorization: `Bearer ${getState().account.access_token}`,
                    // Authorization: "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9hcGkuc2VydnJob3RlbGNsaWVudGRlbW8udGtcL1wvYm9va2luZ3MiLCJpYXQiOjE1ODMyNDA3NTMsImV4cCI6MTU4MzQ1Mjc5OSwibmJmIjoxNTgzMjQwNzUzLCJqdGkiOiJsZ0JiNWNGT1FMWnpuOVVIIiwic3ViIjoxMTIsInBydiI6IjI4ZDBjNTc2NGQ2MTQzMjcxNGFlOTBmM2I1Yjg0NmFiZmJiOGRiNDMifQ.RejBO9P1tGl--1JNcZLMFAg4YR0qYhLrG9HLCv0GdkY",
                },
            })
            .then((response: AxiosResponse<ISuccessGetPromotion>) => {
                if (__DEV__) {
                    console.log(`${action.type}: `, response);
                    console.log('GET_SPECIFIC_PROMOTION_API Response', response);
                }

                //  dispatch(
                //     getPromotionSuccess({
                //         ...response.data.data,
                //         // logo_url: [response.data.logo_url as any, response.data.logo_url as any],
                //         promotions:response.data
                //     }),
                // );

                return response.data;
            })
            .then((response) => {
                console.log('Heloo from logic.promotion  = ', response);
                //TODO: transform logo url string to array string
                // const a ={
                //     id:0,
                //     name:'',
                //     logo_url:[],
                //     promotions:response
                // }
                dispatch(
                    getPromotionSuccess({
                        ...response.data,
                        // logo_url: [response.data.logo_url as any, response.data.logo_url as any],
                        promotions: response,
                    }),
                );

                // if (action.payload.onSuccess) {
                //     action.payload.onSuccess({
                //         ...response.data,
                //         logo_url: [response.data.logo_url as any, response.data.logo_url as any],
                //     });
                // }
            })
            .catch((error) => {
                console.log(error);
                // handleError({
                //     error,
                //     dispatch,
                //     failedAction: spaFailed(error, action.type),
                //     type: action.type,
                //     onFailed: action.payload.onFailed,
                // });
            })
            .then(() => {
                done();
                console.log(
                    'all promotion details url 22' + GET_PROMOTION_API + action.payload.idButton.toString(),
                );
            });
    },
});

const getPromotionDetailsLogic = createLogic({
    type: GET_PROMOTION_DETAILS,
    process(
        { httpClient, getState, action }: IDependencies<ReturnType<typeof getPromotionDetails>>,
        dispatch,
        done,
    ) {
        httpClient
            .get(
                GET_SPECIFIC_PROMOTION_API +
                    action.payload.idHotel.toString() +
                    '/' +
                    action.payload.idButton.toString(),
                {
                    headers: {
                        Accept: 'application/json',
                        Authorization: `Bearer ${getState().account.access_token}`,
                        // Authorization: "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9hcGkuc2VydnJob3RlbGNsaWVudGRlbW8udGtcL1wvYm9va2luZ3MiLCJpYXQiOjE1ODMyNDA3NTMsImV4cCI6MTU4MzQ1Mjc5OSwibmJmIjoxNTgzMjQwNzUzLCJqdGkiOiJsZ0JiNWNGT1FMWnpuOVVIIiwic3ViIjoxMTIsInBydiI6IjI4ZDBjNTc2NGQ2MTQzMjcxNGFlOTBmM2I1Yjg0NmFiZmJiOGRiNDMifQ.RejBO9P1tGl--1JNcZLMFAg4YR0qYhLrG9HLCv0GdkY",
                    },
                },
            )
            .then((response: AxiosResponse<ISuccessGetPromotionDetails>) => {
                if (__DEV__) {
                    console.log(`${action.type}: `, response);
                    console.log('PromotionDetails Response', response);
                }

                return response.data;
            })
            .then((response) => {
                console.log('PromotionDetails Response 111', response);

                dispatch(
                    getPromotionDetailsSuccess({
                        ...response.data,
                        // logo_url: [response.data.logo_url as any, response.data.logo_url as any],
                        PromotionDetails: response,
                    }),
                );

                // if (action.payload.onSuccess) {
                //     action.payload.onSuccess(response.data);
                // }
            })
            .catch((error) => {
                // handleError({
                //     error,
                //     dispatch,
                //     failedAction: spaFailed(error, action.type),
                //     type: action.type,
                //     onFailed: action.payload.onFailed,
                // });
                console.log('ERROR:::', error);
            })
            .then(() => {
                done();
                console.log(
                    'promotion details url 22' +
                        GET_SPECIFIC_PROMOTION_API +
                        action.payload.idHotel.toString() +
                        '/' +
                        action.payload.idButton.toString(),
                );
            });
    },
});

export default [getPromotionLogic, getPromotionDetailsLogic];
